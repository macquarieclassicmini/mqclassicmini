package TestingBuffer;

import java.awt.*;
import java.awt.event.*;

public class Main extends Frame{

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		super("testing123");
		setSize(400, 300);
		setVisible(true);
		addCloseOperation();
	}
	
	public void addCloseOperation() {
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			}
		});
	}

	public void paint(Graphics g) {
		g.setColor(Color.red);
		g.drawRect(50,50,200,200);
		
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.blue);
		g2d.drawRect(75,75,300,200);
		
		g2d.setColor(Color.BLACK);
		g2d.drawRect(10,30,200,250);
	}

}
