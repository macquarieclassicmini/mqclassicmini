package stresstest;
import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class stesstest extends PApplet {

final int ballradius = 16;
final int back = color(25,25,25,25);
class ball {
  float x;
  float y;
  float xspeed;
  float yspeed;
  int col;
  
  public ball (float x, float y) {
    this.x = x;
    this.y = y;
    xspeed = random(-10,10);
    yspeed = random(-10,10);
    col = color(random(0,255));
    }
    
  public void render() {
    fill(col);
    ellipse(x,y,ballradius*2,ballradius*2);
    }
    
  public void move() {
    if ((x + xspeed + (ballradius) >= width) || (x + xspeed - ballradius < 0)) {
        xspeed = -xspeed;
      }
    if ((y + yspeed + (ballradius) > height) || (y + yspeed - ballradius < 0)) {
        yspeed = -yspeed;
      }
    x += xspeed;
    y += yspeed;
    }
  }
  
ArrayList<ball> ballarray = new ArrayList<ball>();
  
public void setup() {
  
  ballarray.add(new ball(random(ballradius,width-ballradius),random(ballradius,height-ballradius)));
  }

public void draw() {
  background(back);
  ballarray.add(new ball(random(ballradius,width-ballradius),random(ballradius,height-ballradius)));
  for (ball b : ballarray) {
    b.move();
    b.render();
    }
  fill(0);
  text(str(frameRate),13,13);
  text(str(ballarray.size()),13,25);
  fill(255);
  text(str(frameRate),12,12);
  text(str(ballarray.size()),12,24);
  }
  
public void keyPressed() {
  ballarray.add(new ball(random(ballradius,width-ballradius),random(ballradius,height-ballradius)));
  ballarray.add(new ball(random(ballradius,width-ballradius),random(ballradius,height-ballradius)));
  ballarray.add(new ball(random(ballradius,width-ballradius),random(ballradius,height-ballradius)));
  ballarray.add(new ball(random(ballradius,width-ballradius),random(ballradius,height-ballradius)));
  }
  public void settings() {  size(500,500); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "stresstest.stesstest" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
