/*
     WHAT IS THIS?
     
 Its a tool i wrote to generate PShape code
 because i wanted to use a lot of them and
 doing all the vertex positions by hand is
 nigh on impossible.
 
     HOW DOES IT WORK?
     
 Left or right click anywhere on the image to
 add vertexes to the current shape.
 
 Shortcuts:
 
 UP - Export vertex code to console
 LEFT - swap between polygon, line and contour modes
 RIGHT - set the relative origin point (without a set origin, the minimum x and y coordinates for the shape will become the relative origin)
 DOWN - destroy the current shape
 
 */


PImage background;

ArrayList<point> shapePoints = new ArrayList<point>();
PShape currentShape;
int mode = 0;

point setOrigin;

String modeToString() {
  String a = "";
  switch(mode) {
    case 0: {a =  "normal";}break;
    case 1: {a =  "lines";}break;
    case 2: {a =  "contour";}break;
    }
  return a;
  }

void setup() {
  noSmooth();
  size(512, 348);
  background = loadImage("hammer.png");
  currentShape = createShape();
  currentShape.beginShape();
  currentShape.endShape();
  
  
}

void draw() {
  background(255);
  tint(255, 125);
  image(background, 0, 0);
  
  fill(0,125);
  
  
  shape(currentShape);
  
  if(shapePoints.size()>0) {
    point a = shapePoints.get(shapePoints.size()-1);
    stroke(255,0,0,125);
    line(a.x,a.y,mouseX,mouseY);
    }
  
  stroke(5);
  text(modeToString(),15,15);
  
  if(setOrigin!=null) {fill(255,0,0,128);ellipse(setOrigin.x,setOrigin.y,4,4);}
  
}

void mousePressed() {
  shapePoints.add(new point(mouseX,mouseY));
  updateShape();
  }

void updateShape() {
  currentShape = null;
  currentShape = createShape();
  if (mode == 1) {
    currentShape.beginShape(LINES);
    currentShape.strokeWeight(3);
    }
  else {currentShape.beginShape();}
  
  point a;
  
  for(int i = 0; i < shapePoints.size(); i++) {
    a = shapePoints.get(i);
    currentShape.vertex(a.x,a.y);
    }
  
  a = null;
  currentShape.endShape();
  
  if(mode != 1){currentShape.setStroke(false);}
  
  }

void keyPressed() {
  if (keyCode == UP) {outputCode();} //print the code
  else if (keyCode == LEFT) {mode++; if(mode>2){mode=0;}} //shift modes
  else if (keyCode == RIGHT) {setOrigin = new point(mouseX,mouseY);} //set the offset origin
  else if (keyCode == DOWN) { //clear the current shape
    shapePoints = new ArrayList<point>();
    currentShape = createShape();
    currentShape.beginShape();
    currentShape.endShape();
  
    }
  }

void outputCode() {
  float minX, minY;
  point a = shapePoints.get(0);
  
  
  if (setOrigin != null) {
    minX = setOrigin.x;
    minY = setOrigin.y;
  }
  else {
  
    
    
    minX = a.x;
    minY = a.y;
    
    for(int i = 1; i < shapePoints.size(); i++) {
      a = shapePoints.get(i);
      
      if (a.x < minX) {minX = a.x;}
      if (a.y < minY) {minY = a.y;}
      }
    println("POS: " + str(minX)+","+str(minY));      
    }
    
    
  
  
  String shapeName = "hotel3";
  
  
  switch(mode){
    case 0: {println(shapeName+".beginShape();");};break;
    case 1: {
      println(shapeName+".beginShape(LINES);");
      println(shapeName+".strokeWeight(3);");
      };break;
    case 2: {println(shapeName+".beginContour();");};break;
    }
  
  
  
  for(int i = 0; i < shapePoints.size(); i++) {
    a = shapePoints.get(i);
    println(shapeName+".vertex("+str(a.x-minX)+","+str(a.y-minY)+");");
    }
    
  switch(mode){
    case 0: 
    case 1: {println(shapeName+".endShape();");};break;
    case 2: {println(shapeName+".endContour();");};break;
    }
    
  println("/**/");
  
  }