package resizeTest;

import processing.core.*;

public class Main extends PApplet{
	
	public void draw() {
		
	}
	
	public void settings() {
		size(100, 100);
	}
	
	public Main() {
		String[] a = new String[] {"Main"};
		PApplet.runSketch(a, this);
	}
	
	public static void main(String[] Args) {
		new Main();
	}

	public void keyPressed() {
		if (key == CODED) {
			if (keyCode == UP) {
				destroyMe(this);
				new small();
			} else if (keyCode == DOWN) {
				destroyMe(this);
				new big();
			}
		}

	}
	
	public static void createMain() {
		new Main();
	}
	
	/**
	 * This when passed an PApplet will dispose of the PApplet and it's surface
	 * as well as get the gc to pick it up
	 * @param me PApplet to be destroyed
	 */
	public static void destroyMe(PApplet me) {
		me.dispose();
		me.getSurface().setVisible(false);
		me = null;
	}
}
