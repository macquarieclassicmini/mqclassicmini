package resizeTest;

import processing.core.PApplet;

public class small extends PApplet{
	
	public void settings() {
	    size(500,500);
	}
	
	int a = 1;
	public void draw() {
	  for (int i = 0; i<a; i++) {
	    ellipse(random(width), random(height), 50, 50);
	  }
	  a++;
	  //println(frameRate);
	}
	
	
	public small() {
		String[] a = new String[] {"small"};
		PApplet.runSketch(a, this);
	}
	
	public void keyPressed() {
		if (key == 'b') {
			Main.destroyMe(this);
			Main.createMain();
		}
	}
}
