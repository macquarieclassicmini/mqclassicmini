package resizeTest;

import processing.core.PApplet;

public class big extends PApplet{
	public void settings() {
		size(500,1000);
	}
	
	public big() {
		String[] a = new String[] {"big"};
		PApplet.runSketch(a, this);
	}
	
	public void draw() {} //required for keyPressed
	
	public void keyPressed() {
		if (key == 'b') {
			Main.destroyMe(this);
			Main.createMain();
		}
	}
}
