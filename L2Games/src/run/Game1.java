package run;
import processing.core.*;

public class Game1 extends PApplet{

	int manX = 60;
	int manY = 240;
	int step = 65;
	int doorCounter = 0;
	boolean doorOpen = false;
	int hammerX = 255;
	int hammerY = 0;
	int hammerDrop = 0;
	int slot0X = 60;
	int pointBlack=0;
	int pointRed = 0;
	float cloud1X = 132;
	float cloud1Y = 86;
	float cloud2X = 344;
	float cloud2Y = 69;
	float cloud3X = 466;
	float cloud3Y = 100;



	public void settings() {
		size(512, 348);
		doorCounter = (int)random(180, 480);
		hammerDrop = (int)random(60, 60);
		hammerX = (int) (slot0X + step * (random(1, 6)));
	}

	public void draw() {
		Main.comeBackToMe(key, this);
		background(158, 170, 173);
		drawBackDrop();
		drawClouds(cloud1X, cloud1Y);
		drawClouds(cloud2X, cloud2Y);
		drawClouds(cloud3X, cloud3Y);
		drawGrass(152, 258);
		drawGrass(80, 321);
		drawGrass(220, 319);
		drawGrass(494, 278);
		drawGrass(336, 261);
		drawGrass(424, 308);
		doorSystem();
		drawMan();
		hammer();
		hammerCollide();
		hammerPass();
		manHome();
		pointSystem(pointBlack/6, pointRed);
	}

	public void keyPressed() {
		/*
		if (key == 'b') {
			Main.destroyMe(this);
			Main.createMain();
			return;
		}
		*/
		makeManMove();
	}

	void drawBackDrop() {
		int sunX = 10;
		int sunY = 50;
		int balconyX = 10;
		int balconyY = 100;
		int cottageX = 400;
		int cottageY = 200;
		int floorX = 100;
		int floorY = 280;
		int knobDiameter = 5;
		int leftDoorX = 50;
		int leftDoorY = 200;

		// sky, ground
		fill(158, 217, 233, 180);
		noStroke();
		rectMode(CORNERS);
		rect(0, 0, width, height-110);
		fill(156, 206, 111, 180);
		rect(0, height-110, width, height);

		//sun
		fill(230, 200, 0);
		noStroke();
		ellipse(sunX-10, sunY-50, 150, 150);
		triangle(sunX, sunY+40, sunX+14, sunY+37, sunX+11, sunY+57);
		triangle(sunX+29, sunY+31, sunX+42, sunY+23, sunX+45, sunY+45);
		triangle(sunX+51, sunY+18, sunX+62, sunY+8, sunX+72, sunY+31);
		triangle(sunX+70, sunY-1, sunX+77, sunY-12, sunX+92, sunY+8);
		triangle(sunX+81, sunY-21, sunX+82, sunY-35, sunX+97, sunY-25);

		//balcony
		fill(154, 110, 46);
		rect(balconyX-10, balconyY+17, balconyX, balconyY+110);
		rect(balconyX, balconyY+17, balconyX+33, balconyY+31);
		rect(balconyX, balconyY+43, balconyX+33, balconyY+70);
		rect(balconyX, balconyY+78, balconyX+33, balconyY+110);

		fill(198, 169, 164);
		rect(balconyX, balconyY+25, balconyX+33, balconyY+28);
		rect(balconyX, balconyY+61, balconyX+33, balconyY+65);
		rect(balconyX, balconyY+100, balconyX+33, balconyY+104);

		//roof and walls
		strokeWeight(1);
		stroke(0);
		fill(167, 110, 10);
		rect(cottageX+30, cottageY+12, cottageX+126, cottageY+76);
		stroke(102, 51, 0);
		fill(153, 102, 51);
		triangle(cottageX+30, cottageY+12, cottageX+78, cottageY-8, cottageX+126, cottageY+12);

		stroke(0);
		strokeWeight(8);
		line(cottageX+24, cottageY+10, cottageX+78, cottageY-8);
		line(cottageX+78, cottageY-8, cottageX+112, cottageY+4);

		//floor
		strokeWeight((float) 1.5);
		stroke(174, 138, 136);
		line(floorX-54, floorY-4, floorX+6, floorY-7);
		line(floorX+13, floorY - 1, floorX+74, floorY + 6);
		line(floorX+85, floorY + 3, floorX+118, floorY-1);
		line(floorX+138, floorY+3, floorX+197, floorY+3);
		line(floorX+209, floorY+4, floorX+266, floorY+9);
		line(floorX+269, floorY+5, floorX+330, floorY-4);

		stroke(165, 154, 159);
		line(floorX-55, floorY, floorX+1, floorY-3);
		line(floorX+33, floorY+4, floorX+73, floorY+11);
		line(floorX+98, floorY+5, floorX+130, floorY+1);
		line(floorX+141, floorY+7, floorX+193, floorY+4);
		line(floorX+205, floorY+9, floorX+256, floorY+14);
		line(floorX+274, floorY+14, floorX+330, floorY-4);

		//left door
		strokeWeight(1);
		stroke(0);
		fill(180);
		rect(leftDoorX-50, leftDoorY+10, leftDoorX -4, leftDoorY+76);
		//window
		fill(0);
		rect(leftDoorX-35, leftDoorY+22, leftDoorX-19, leftDoorY+38);
		//doorknob
		ellipse(leftDoorX-9, leftDoorY + 46, knobDiameter, knobDiameter);
	}

	void circleColour (int t, int h, int red) {
		int circleSize=75;
		for (int k=0; k<255; k+=17) {
			fill(red, 0, 0, k);
			noStroke();
			ellipse(t, h, circleSize, circleSize);
			circleSize=circleSize-5;
		}
	}

	void drawClouds(float x, float y) {
		fill(230, 230, 230, 248);
		noStroke();
		ellipse(x - 23, y - 20, 40, 30);
		ellipse(x - 44, y - 2, 50, 30);
		ellipse(x, y, 54, 28);
		//cloud movement
		cloud1X = (float) ((cloud1X + 0.4) % (width + 70)); //code discovered in reference tab on
		cloud2X = (float) ((cloud2X + 0.4) % (width + 75)); // processing.org
		cloud3X = (float) ((cloud3X + 0.4) % (width + 43));
	}

	void doorSystem() {
		int knobDiameter = 5;
		int cottageX = 400;
		int cottageY = 200;
		doorCounter = doorCounter - 1; //code inspiration from
		if (doorCounter <= 0) { //Dominic Verity on discussion page
			doorOpen = !doorOpen;
			doorCounter = (int)random(180, 480);
		}
		strokeWeight(1);
		stroke(0);
		if (doorOpen == true) {
			//open door
			fill(180);
			quad(cottageX+75, cottageY+17, cottageX+102, cottageY+5, cottageX+102, cottageY+88, cottageX+75, cottageY+76);
			fill(0);
			quad(cottageX+80, cottageY+27, cottageX+95, cottageY+20, cottageX+95, cottageY+42, cottageX+80, cottageY+40);
			ellipse(cottageX+97, cottageY+53, knobDiameter-1, knobDiameter+1);
		} else { 
			//cottage window and doorknob
			fill(180);
			rect(cottageX+35, cottageY+17, cottageX+75, cottageY+76);
			fill(0);
			rect(cottageX+47, cottageY+26, cottageX+63, cottageY+42);
			ellipse(cottageX+40, cottageY+47, knobDiameter, knobDiameter);
		}
		//doorframe colour
		if (doorOpen == true) {
			fill(100);
			rect(cottageX+35, cottageY+17, cottageX+75, cottageY+76);
		}
	}
	void drawCottage() {
		//color roof = color(153, 102, 51);
		//color cottageWall=color(167, 110, 10);
		//color roofLine = color(102, 51, 0);
		//strokeWeight(1);
		//stroke(0);
		//fill(cottageWall);
		//rect(cottageX+30, cottageY+12, cottageX+126, cottageY+76);
		//stroke(roofLine);
		//fill(roof);
		//triangle(cottageX+30, cottageY+12, cottageX+78, cottageY-8, cottageX+126, cottageY+12);

		////roof
		//stroke(0);
		//strokeWeight(8);
		//line(cottageX+24, cottageY+10, cottageX+78, cottageY-8);
		//line(cottageX+78, cottageY-8, cottageX+112, cottageY+4);
	}

	void drawGrass(int x, int y) {
		fill(51, 153, 51);
		noStroke();
		triangle(x-7, y, x-3, y, x-5, y-8);
		triangle(x-4, y, x+2, y, x, y-19);
		triangle(x+1, y, x+5, y, x+3, y-8);
	}

	void hammer() {
		strokeWeight(5);
		line(hammerX, hammerY, hammerX + 20, hammerY - 20);
		strokeWeight(6);
		line(hammerX + 8, hammerY + 6, hammerX - 6, hammerY - 6);

		hammerDrop = hammerDrop - 1; //similar to door open/close code from Dominic Verity
		if (hammerDrop <= 0) {
			hammerY = hammerY + 60;
			hammerDrop = 60;
		}
	}

	void hammerPass() {
		if ((hammerY >= 480)) {
			hammerY = 0;
			hammerX = ((int)random(1, 6));
			pointBlack+=1;
			hammerX = slot0X + step * hammerX;
		} 
		//if (hammerX == 1) {
		//  hammerX = slot1X;
		//} else if (hammerX == 2) {
		//  hammerX = slot2X;
		//} else if (hammerX == 3) {
		//  hammerX = slot3X;
		//} else if (hammerX == 4) {
		//  hammerX = slot4X;
		//} else if (hammerX == 5) {
		//  hammerX = slot5X;
		//}

		// hammerX = slot0X + step * hammerX;
	}

	void hammerCollide() {
		if ((hammerY == manY)&&(manX == hammerX)) {
			manX = slot0X;
			pointRed+=1;
			hammerY = 0;
			hammerX = ((int)random(1, 6));
		}
	}

	void drawMan() {
		//head
		stroke(0);
		fill(0);
		ellipse(manX, manY, 20, 20);

		//mouth interactive with background
		if (manX >= 430) {
			fill(100);
			noStroke();
			ellipse(manX + 10, manY + 6, 13, 5);
		} else {
			fill(155, 196, 126);
			noStroke();
			ellipse(manX + 10, manY + 6, 13, 5);
		}

		//nose
		fill(0);
		ellipse(manX + 10, manY - 2, 9, 5);

		//body
		triangle(manX - 2, manY + 10, manX - 10, manY + 24, manX - 2, manY + 28);

		//legs
		stroke(0);
		line(manX - 9, manY + 24, manX - 9, manY + 34);
		line(manX - 5, manY + 24, manX - 5, manY + 34);

		//arms
		line(manX - 2, manY + 16, manX + 3, manY + 16);
		line(manX - 2, manY + 18, manX + 3, manY + 18);
	}

	void manHome() {
		if (manX==slot0X + step * 6) {
			manX=slot0X;
			pointBlack+=3;
		}
	}


	void makeManMove() {
		if (key == CODED) {
			if ((keyCode == RIGHT)&&(doorOpen == true)) {
				if (manX<slot0X * 6 + step) {
					manX = manX + step;
				}
			} else if (keyCode == RIGHT) {
				if (manX<slot0X * 5 + step) {
					manX = manX + step;
				}
			} else if (keyCode == LEFT) {
				if (manX>slot0X) {
					manX = manX - step;
				}
			}
		}
	}

	void pointSystem(int b, int r) {
		//single black
		for (int i=0; i<pointBlack%6; i++) {
			circleColour(80*i+50, 80*(b)+80*(r)+50, 0);
		}
		//black row
		for (int w=0; w<6; w++) {
			for (int v=r; v<(b+r); v++) {
				circleColour(80*w+50, 80*v+50, 0);
			}
		}
		//red row
		for (int w=0; w<=5; w++) {
			for (int v = 1; v<=r; v++) {
				circleColour(80*w+50, 80*v-30, 255);
			}
		}
		//reset
		if (b+r >=4) {
			pointBlack = 0;
			pointRed = 0;
			hammerY = 0;
			manX = slot0X;
			cloud1X = 132;
			cloud1Y = 86;
			cloud2X = 344;
			cloud2Y = 69;
			cloud3X = 466;
			cloud3Y = 100;
		}
	}

	void drawLeftDoor() {
		//int knobDiameter = 5;
		//color door = color(180);
		//int leftDoorX = 50;
		//int leftDoorY = 200;
		//strokeWeight(1);
		//stroke(0);
		//fill(door);
		//rect(leftDoorX-50, leftDoorY+10, leftDoorX -4, leftDoorY+76);
		////window
		//fill(0);
		//rect(leftDoorX-35, leftDoorY+22, leftDoorX-19, leftDoorY+38);
		////doorknob
		//ellipse(leftDoorX-9, leftDoorY + 46, knobDiameter, knobDiameter);
	}



	public Game1() {
		String[] a = new String[] {"big"};
		PApplet.runSketch(a, this);
	}
}
