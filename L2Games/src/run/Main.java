package run;

import processing.core.*;

public class Main extends PApplet{
	
	
	public void draw() {
		drawOptions();
		buttonlistener();
	}
	
	
	/** option draw configurations **/
	int numOfOpt = 2;
	int border = 10;
	String optionName = "game";
	
	/**
	 * Creates all the option boxes on the screen
	 */
	public void drawOptions() {
		int temp = border;
		int h = (height-(border*(numOfOpt+1)))/numOfOpt;
		int w = width-border*2;
		for (int i = 0; i<numOfOpt; i++) {
			this.rect(border, temp, w, h);
			fill(0); text(optionName + (i+1), border+(w/2), temp+(h/2)); fill(255);
			temp += h + border;
		}
	}

	public void buttonlistener() {
		int temp = border;
		int h = (height-(border*(numOfOpt+1)))/numOfOpt;
		for (int i = 0; i<numOfOpt; i++) {
			if (mousePressed == true && mouseY > temp) {
				if (mouseY > temp+h+border) {
					temp += h + border;
					continue;
				}
				try {
					loader.load(i);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				destroyMe(this);
				return;
			}
			temp += h + border;
		}
	}

	
	/** **/
	
	public void settings() {
		size(500, 500);
	}
	
	public Main() {
		String[] a = new String[] {"Main"};
		PApplet.runSketch(a, this);
	}
	
	public static void main(String[] Args) {
		new Main();
	}
	
	public static void createMain() {
		new Main();
	}
	
	public static void comeBackToMe(char k, PApplet prog) {
		if (k == 'b') {
			Main.destroyMe(prog);
			Main.createMain();
			return;
		}
	}
	
	/**
	 * This when passed an PApplet will dispose of the PApplet and it's surface
	 * as well as get the gc to pick it up
	 * @param me PApplet to be destroyed
	 */
	public static void destroyMe(PApplet me) {
		me.dispose();
		me.getSurface().setVisible(false);
		me = null;
	}
}
