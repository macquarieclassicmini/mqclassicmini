package run;
import java.io.*;

import javax.tools.JavaCompiler;

public class loader {
	public static void load(int game) throws Exception {
		/*
		if (game == 0) {
			//runProcess("javac D:\\please\\hello.java");
			//runProcess("java -cp D:\\please\\ hello");
			runProcess("java -cp D:\\eclipse-workspace\\L2Games\\bin\\ Main");
		}
		*/
		switch(game) {
			case 0: 
				new Game1();
				break;
			case 1: 
				new Game2();
				break;
			default:
				new Main();
		}
	}
	
	/*
	private static void printLines(String cmd, InputStream ins) throws Exception {
		String line = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(ins));
		while ((line = in.readLine()) != null) {
			System.out.println(cmd + " " + line);
		}
	}

	private static void runProcess(String command) throws Exception {
		Process pro = Runtime.getRuntime().exec(command);
		printLines(command + " stdout:", pro.getInputStream());
		printLines(command + " stderr:", pro.getErrorStream());
		pro.waitFor();
		System.out.println(command + " exitValue() " + pro.exitValue());
	}
	*/
}
