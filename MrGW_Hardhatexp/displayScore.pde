void displayScore(int blackAdd, int redAdd) {
  ellipseMode(CENTER);
  int drawnScore = 0;
  int circleNum = 0;
  blackScore = blackAdd + blackScore; //increases positive score
  redScore = redAdd + redScore; // increases negative score

  if (blackScore >= 40) {  //resets game if circles fill screen
    gameWon = true;
    gameOver();
  } else if (blackScore + redScore >= 40) {
    gameWon = false;
    gameOver();
  }
  
  //negative score
  for (int i=0; i < redScore; i++) {            //makes the circles display in rows
    drawnScore = i*8;
    circleNum = redScore;
    circleNum = circleNum - drawnScore;

    for (int j=0; j<circleNum && j<8; j++) {    //draws the circles in a row

      for (int k = 50; k > 0; k--) {            //makes a gradient circle
        fill(200, 0, 0, 70-k);
        noStroke();
        ellipse((gapX*j+40), (i*70+35), k, k);
      }
    }
  }

  //positive score, functions same as negative
  for (int i=0; i < blackScore; i++) {
    drawnScore = i*8;
    circleNum = blackScore;
    circleNum = circleNum - drawnScore;

    for (int j=0; j<circleNum && j<8; j++) {

      for (int k = 50; k > 0; k--) {
        fill(k*3, 90-k);
        noStroke();
        ellipse((gapX*j+40), (i*70+35+redScore/8*70), k, k);  //makes black circles appear below red circles
      }
    }
  }
}