void gameOver() {
  //resets all relevant variables to original state, displays appropriate game over screen
  //set counter high to tell draw loop to recall this function for an additional few seconds
  if (counter < 480) {
    gwPos = 1;
    doorOpen = false;
    counter = 600;
    toolY[0] = 9;
    toolY[1] = 1;
    toolY[2] = 11;
  }
// resetting score to 0 here prevents player scoring during game over screen
  if (counter > 480) {
    if (gameWon == false) {
      background(0);
      fill(255);
      textSize(40);
      text("GAME OVER", width/4, height/2);
      blackScore = 0;
      redScore = 0;
      return;
    } else {
      background(0);
      fill(255);
      textSize(40);
      text("YOU WIN", width/3, height/2);
      blackScore = 0;
      redScore = 0;
      return;
    }
  }
}