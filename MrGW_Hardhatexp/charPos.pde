void charPos(int gwAdd) {
  PImage gw;
  gw = loadImage("gwhelmet.gif");

  //assigns Mr G&W a position on the screen, changes based on user input
  int gwY = 280;
  int gwX = gwPos*gapX+20;
  image(gw, gwX, gwY);  

  //moves gw left and right, or not at all if he is at the bounds of the stage
  //increments score by 3 when goal is met, sets door timer low to prevent player spam scoring
  gwPos = gwPos + gwAdd;

  if (gwPos == 7) {
    if (doorOpen == true) {
      gwPos = 1;
      counter = 20;
      displayScore(3, 0);
    } else {
      gwPos = 6;
    }
  }
  if (gwPos < 1) {
    gwPos = 1;
  }
}