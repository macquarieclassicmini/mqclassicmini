void toolPos() {
  PImage[] toolType = new PImage[3];
  toolType[0] = loadImage("hammer.png");
  toolType[1] = loadImage("bucket.png");
  toolType[2] = loadImage("wrench.png");

//defines tool position once a second
//tools in array jump til off screen, then reset
  int gapY = 30;
  int[] toolX = {2, 3, 5};
  for (int i = 2; i > -1; i--) {
    image(toolType[i], toolX[i]*gapX+20, toolY[i]*gapY+20);
    if (toolCounter == 0) {
      toolY[i] = toolY[i] + 2;
    } else if (toolY[i]  >= 12) {
      toolY[i] = 1;
      displayScore(1, 0);
    }
  }

  if (toolCounter == 0) {
    toolCounter = 60;
  }

  //resets if gw and the tool are touching, adds negative score
  for (int i = 2; i > -1; i--)
    if (toolY[i] == 9 && gwPos == toolX[i]) {
      gwPos = 1;
      toolY[i] = 1;
      displayScore(0, 8);
    }
}