void backdrop() {
  PImage backdropLeft;
  PImage backdropBottom;
  PImage backdropRight;
  backdropLeft = loadImage("backdropLeft.png");
  backdropBottom = loadImage("backdropBottom.png");
  backdropRight = loadImage("backdropRight.png");
  image(backdropLeft, 0, 0);
  image(backdropRight, 400, 0);
  image(backdropBottom, 85, 327);
}