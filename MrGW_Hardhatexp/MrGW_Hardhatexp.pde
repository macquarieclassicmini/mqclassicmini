int gwPos = 1;
int blackScore = 0;
int redScore = 0;
int toolCounter = 60;
int counter = (int)random(180, 480);
int gapX = 60;
int[] toolY = {1, 5, 3};
boolean doorOpen = false;
boolean gameWon = false;

void setup() {
  size(512, 348);
}  

void draw() {
  background(255);
  backdrop();

  //decreases the counter once a frame
  counter = counter - 1;
  toolCounter = toolCounter - 1;

  //calls other functions without altering their variables
  doorState();
  charPos(0);
  toolPos(); 
  displayScore(0, 0);

//calls gameover function when counter is set high on fail state
  for (int i = counter; i > 480; i--) {
    gameOver();
  }
}

//defines the current state of G&W by user input, increases score if goal is met
void keyPressed() {
  if (key == CODED) {
    if (keyCode == RIGHT) {
      charPos(1);
    } else if (keyCode == LEFT) {
      charPos(-1);
    }
  }
}