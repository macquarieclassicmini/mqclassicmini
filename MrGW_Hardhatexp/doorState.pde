void doorState() {
  PImage doorClosed;
  PImage doorClear;
  doorClear = loadImage("doorClear.png");
  doorClosed = loadImage("doorClosed.png");

  //defines state of door and displays appropriate image
  if (counter <=0) {
    if (doorOpen == false) {
      doorOpen = true;
      counter = (int)random(180, 480);
    } else {
      doorOpen = false;
      counter = (int)random(180, 480);
    }
  }

  if (doorOpen == false) {
    image(doorClosed, 450, 225);
  } else if (doorOpen == true) {
    image(doorClear, 450, 225);
  }
}