//player info
 //x,y of player
  int x;
  int y;
 //rotation of player
  int rot = 180;

//fizzy variables
  //fizzy eyes
    int eye_w, eye_h, pupil_w, pupil_h;

  // sets the radius and diamater for the player circle
    int dia = 50;
    int radius = dia/2;

// the diamater of circles for the grid
  int gridDia = 20;
  
boolean status[] = new boolean[16];

///////////////////////////////////////////////////////////

void setup(){
  size (810, 810);
  
//start in the middle
  x = width/2; 
  y = height/2;
  
// starts the character moving
  key = 's';
}

////////////////////////////////////////////////////////////

void eyeUD (int x1, int x2, int ye1, int ye2) {
  //the eye
  fill(0,0,0);
  ellipse(x1, ye1, eye_w, eye_h);
  ellipse(x2, ye1, eye_w, eye_h);
  
  // the pupil
  fill(255,255,255);
  ellipse(x1, ye2, pupil_w, pupil_h);
  ellipse(x2, ye2, pupil_w, pupil_h);
}

////////////////////////////////////////////////////////////

void eyeSide (int x1, int x2, int ye1, int ye2) {
  //the eye
  fill(0,0,0);
  ellipse(x1, ye1, eye_w, eye_h);
  ellipse(x1, ye2, eye_w, eye_h);
  
  // the pupil
  fill(255,255,255);
  ellipse(x2, ye1, pupil_w, pupil_h);
  ellipse(x2, ye2, pupil_w, pupil_h);
}

////////////////////////////////////////////////////////////

// sets the eye width and height
void eyeWH (int eW, int eH) {
  eye_w = eW;
  eye_h = eH;
  pupil_w = eW - 10;
  pupil_h = eH - 10;
}

////////////////////////////////////////////////////////////

//set the parameters for the redline on fizzy
void redLine (int Xl1, int Xl2, int Yl1, int Yl2){
  color(255, 0, 0);
  stroke(255,0,0);
  strokeWeight(4);
  line(Xl1, Yl2, Xl2, Yl1);
}

////////////////////////////////////////////////////////////

void drawFizzy (int x, int  y, int rot) {
  //the circle
  strokeWeight(0);
  fill(255,0,0);
  ellipse(x, y, dia, dia);
  
  //rotation
    //going up
    if (rot == 0){
      eyeWH (20, 25);
      eyeUD (x + 6, x - 6, y - 6, y - 12);
      redLine (x, x, y - 20, y + 33);
    
    // going right
  } else if (rot == 90){
      eyeWH (25, 20);
      eyeSide (x + 6, x + 12, y + 6, y - 6);
      redLine (x + 20, x - 33, y, y);
        
    //going down
  } else if (rot == 180){
      eyeWH (20, 25);
      eyeUD (x - 6, x + 6, y + 6, y + 12);
      redLine (x, x, y + 20, y - 33);
    
    // going left
  } else if (rot == 270){
      eyeWH (25, 20);
      eyeSide (x - 6, x - 12, y - 6, y + 6);
      redLine (x - 20, x + 33, y, y);
  }
}

////////////////////////////////////////////////////////////

int distance(int posX, int posY, int sqrsize){
  int dis;
  dis = (int)(dist(posX + sqrsize + 20, posY + sqrsize + 20, x+dia, y+dia));
  return(dis);
}

////////////////////////////////////////////////////////////

void sqrloc (int posX[], int i, int posY [], int j, int sqrsize,boolean status[] , int k){ 
  stroke (0, 255, 0);
  if ((distance(posX[i], posY[j], sqrsize) <= sqrsize) || (status[k] == true)) { // add an or here status = true
      status[k] = true;
      fill(0, 255, 0);
      rect(posX[i], posY[j], sqrsize, sqrsize);
      fill (255);
    }
  stroke (255, 0, 0);
}

////////////////////////////////////////////////////////////

void createsqr () {
  // intiulise the int arrays
  int posX[] = new int[7];
  int posY[] = new int[7];
  
  int sqrsize = 40;
  
  // Set all the grid coordiants
  for (int i = 0; i < posX.length; i ++) {
    posX[i] = 115+90*i;
    posY[i] = 115+90*i;
  }
  
//The heart 
  // First row (row 0)
  sqrloc (posX, 1, posY, 0, sqrsize, status, 0);
  sqrloc (posX, 2, posY, 0, sqrsize, status, 1);
  sqrloc (posX, 4, posY, 0, sqrsize, status, 2);
  sqrloc (posX, 5, posY, 0, sqrsize, status, 3);
  
  //Row 1
  sqrloc (posX, 3, posY, 1, sqrsize, status, 4);
  sqrloc (posX, 0, posY, 1, sqrsize, status, 5);
  sqrloc (posX, 6, posY, 1, sqrsize, status, 6);
 
 //Row 2
  sqrloc (posX, 0, posY, 2, sqrsize, status, 7);
  sqrloc (posX, 6, posY, 2, sqrsize, status, 8);
  
  //Row 3
  sqrloc (posX, 0, posY, 3, sqrsize, status, 9);
  sqrloc (posX, 6, posY, 3, sqrsize, status, 10);
 
 //Row 4
 sqrloc (posX, 1, posY, 4, sqrsize, status, 11);
 sqrloc (posX, 5, posY, 4, sqrsize, status, 12);
 
 //Row 5
 sqrloc(posX, 2, posY, 5, sqrsize, status, 13);
 sqrloc (posX, 4, posY, 5, sqrsize, status, 14);
 
 //Bottom row (row 6)
 sqrloc (posX, 3, posY, 6, sqrsize, status, 15);
}

////////////////////////////////////////////////////////////

void draw () {
// resets the background
  background (0);
  createsqr();
  
// creates the grid
  for (int down= 90; down < height - 80; down += 90){
    for (int across= 90; across < width - 80; across += 90){
      fill(0);
      strokeWeight (4);
      stroke (0, 210, 0);
      ellipse (across, down, gridDia, gridDia);
      stroke (0);
      fill(255);
    }
  }

// creats the circle
  drawFizzy(x, y, rot);
  //ellipse (x, y, dia, dia); // debug circle
  
  //key inputs
  if (key == 'w') {
    y--;
    rot = 0;
  }
  if (key == 'd') {
    x++;
    rot = 90;
  }
  if (key == 's') {
    y++;
    rot = 180;
  }
  if (key == 'a') {
    x--;
    rot = 270;
  }
 
// bounce of walls (by force input)
  if (y+radius >= height){
   key = 'w';
  }
  if (x+radius >= width){
   key = 'a';
  }
  if (x-radius <= 0){
    key = 'd';
  }
  if (y-radius <=0) {
    key = 's';
  }
}