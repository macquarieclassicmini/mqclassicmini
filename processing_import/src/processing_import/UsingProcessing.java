package processing_import;

import processing.core.*;

public class UsingProcessing extends PApplet{
	public void settings() {
		size(500,500);
	}
	
	public void draw() {
		background(64);
		ellipse(mouseX, mouseY, 20, 20);
	}
	
	public static void main(String[] Args) {
		String[] a = new String[] {"UsingProcessing"};
		UsingProcessing happy = new UsingProcessing();
		PApplet.runSketch(a, happy);
	}
}