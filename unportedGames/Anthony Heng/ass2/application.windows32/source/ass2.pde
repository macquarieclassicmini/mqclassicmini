/*NOTE TO MARKER: This is a Prize submission, thus will variate slightly from the initial specifications as listed: //<>// //<>//
 -Fizzy swings around poles with wasd, in which his hands stick out usings wasd controls. These hands cling onto poles on contact in order to initiate swinging. Letting go of the button will 
 have fizzy move in the direction he is facing at the next 90 degree swinging interval
 -There is a green border around the game level, for both presentation and in order to create space to place GUI elements
 -drawFizzy function in fizzy class
 -Rupees are ovular and slightly decorated instead of a square. These will be joined between two poles and still make a heart shape. 
 */
int level;
float handY; 
float handX;
int enemyCount = 6;
int a = 0;
int endGameAnimBox = 950;
int timeIntv = millis();
boolean gameRunning = false;
int time = 120;
boolean drawHand = false;
int highScore = 0;
int score = 0;
// for rupeesLoc, 2nd dimension: x is for the first row, y is for the second
int[][] rupeesLoc =     {{405, 405, 450, 360, 315, 495, 270, 540, 585, 225, 180, 180, 630, 630, 225, 585, 270, 360, 450, 540, 315, 495}, 
  {270, 630, 585, 585, 540, 540, 495, 495, 450, 450, 405, 315, 405, 315, 270, 270, 225, 225, 225, 225, 180, 180}};
boolean[] rotatedRupee = {false, false, true, true, false, false, true, true, false, false, true, true, true, true, false, false, true, true, true, true, false, false};
boolean[] rupeeCollected = new boolean[rotatedRupee.length];
int rupLessCount;
int[][] blackHolesLoc = {{225, 585, 405}, {585, 585, 405}};
PVector powerUp;
boolean powerUpFound = false;
boolean handOut = false;
boolean showHelp =false;
int backgroundCoinCount = 0;
boolean gameWon = false;
boolean scoreAdded = false;
fizzy[] backgroundFizzies = new fizzy[5];
urchin[] backgroundUrchins = new urchin[30];
urchin[] enemies = new urchin[10];
fizzy player1;



void setup() {
  //Setup 810x810 screen
  size(810, 810);
  resetRupees();
  frameRate(30);
  smooth();
  player1 = new fizzy(1, 135, 135, 0, 0, 255);
  int y = 0;
  for (int i = 0; i<enemyCount; i++) {
    if (y >=2) {
      y = 0;
    }
    y++;
    enemies[i]= new urchin(player1.speed*0.75, blackHolesLoc[0][y], blackHolesLoc[1][y], int(random(1, 6)), 0);
  }
  for (int i = 0; i<backgroundFizzies.length; i++) {
    backgroundFizzies[i] = new fizzy((int (random(1, 50))), random(30, width-20), -100, 1, 0, 255);
  }
  for (int i= 0; i< backgroundUrchins.length; i++) {
    backgroundUrchins[i] = new urchin((random(1, 50)), int(random(30, width-20)), -100, int(random(1, 5)), 0);
  }
  /*The two lines below would load the saved high score. Because you a recieving my game as a pde, uncommenting them at first will cause errors.
   If you like my lovely work and want to allow the high score to be saved on application exit, you must get a game over at least once before uncommenting the two lines below*/
  //String[] s = loadStrings("highScore.txt");
  //highScore = int(s[0]);
}


void draw() {
  background(#45830C);
  if ((!gameRunning)&&(!gameWon)) {
    mainMenu();
  } else if (gameWon) {
    winGame();
  } else if (gameRunning) {
    gameControls();
    levelChanges();
    backgroundDesign();
    poles();
    hourglass();
    rupee(player1.posX, player1.posY);
    if (player1.alive) {
      timeLimit();
      enemyActions();
      playerActions();
      player1.drawFizzy(int(player1.posX), int( player1.posY), player1.rotate);
      playerColours();
    } else {
      //if player is hit, play death animation when she currently has more than 0 lives. Otherwise, end game. 
      if (player1.lives>0) {
        player1.death();
        for (int i = 0; i<enemyCount; i++) {
          enemies[i].drawUrchin(enemies[i].posX, enemies[i].posY, enemies[i].rotate);
        }
      } else if (player1.lives == 0) {
        player1.alive = false;
        enemyActions();
        gameOver();
      }
    }

    fizzyHit();
    showHelp = false;
  }
}

void keyPressed() {
  handOut = true;
  //If game over box is in the correct position, pressing any key will reset the game but record the high score. pde's alone will not be enough to save progress once user exit's program.
  if ((endGameAnimBox < (width/2)-90)) {
    resetGame();
    score = 0;
  } else if (gameWon) {
    if (millis() > timeIntv+3000) { 
      resetGame();
      score = 0;
    }
  }
}

void keyReleased() {
  player1.drawHand = false;
  if (player1.inSwinging) {
    player1.finishSwinging = true;
  } 
  handOut = false;
}

void playerActions(){
  //if player isn't swinging, move normally
   if (!player1.swinging) {
        player1.move();
        player1.keepInLine();
        if (player1.drawHand) {
          player1.hand();
        }
      } else if (player1.finishSwinging) {
        player1.finishSwing();
      } else if ((player1.swinging)&&(!player1.finishSwinging)) {
        player1.swing(player1.rotate);
      } 
}




void enemyActions() {
  for (int i = 0; i<enemyCount; i++) {
    enemies[i].ai();
    enemies[i].move();
    enemies[i].drawUrchin(enemies[i].posX, enemies[i].posY, enemies[i].rotate);
    enemies[i].keepInLine();
  }
}


void playerColours() {
  //If player hasn't moved yet, fizzy is white to indicate she is invincible. Otherwise, she is red.
  if (player1.movement != 0) {
    player1.colour = #FA0808;
  } else { 
    player1.colour = 255;
    textSize(30);
    fill(0);
  }
}

void winGame() {

  for (int i = 0; i<15; i++) {
    backgroundUrchins[i].move();
    backgroundUrchins[i].spin += int(random(3, 9));
    if (backgroundUrchins[i].posY>900) {
      backgroundUrchins[i].posY = -100;
      backgroundUrchins[i].posX = random(30, width-20);
      backgroundUrchins[i].speed = (int (random(1, 10)));
    }
    backgroundUrchins[i].drawUrchin(int(backgroundUrchins[i].posX), int(backgroundUrchins[i].posY), backgroundUrchins[i].spin);
  }
  fill(255);
  rectMode(CORNER);
  fill(#F3F71B);
  rect(280, 450, 250, 230);
  rect(200, 100, 400, 50);
  fill(0);
  //Titles
  textSize(30);
  text("You saved Kuru Country!", 220, 130);
  text("BONUS!", 350, 480);
  text("TOTAL SCORE", 300, 600);
  //Bonus point text
  int bonusP = player1.lives*10000;
  if (!scoreAdded) {
    score +=bonusP;
    timeIntv = millis();
  }
  scoreAdded = true;
  textSize(20);
  text("lives x 10000", 340, 510);
  textSize(25);
  text("=" + bonusP, 340, 550);

  text(score, 300, 650);

  if (millis() > timeIntv+3000) { 
    textSize(10);
    text("Press Any Key Restart", 350, 670);
  } 
  fizzyWinner();
}
void fizzyWinner() {
  //shoes
  rectMode(CENTER);
  fill(0);
  rect(405 - 30, 405 +40, 80/2, 80/4); 
  rect(405 + 30, 405 +40, 80/2, 80/4); 
  //Head Base
  fill(255, 0, 0);
  ellipse(405, 405, 80, 80);
  //Face
  //Outer "White Bit" of eyes
  fill(255);
  ellipse(405, (405- 10), 80/5, 80/5);
  ellipse((405 + 20), (405- 10), 80/5, 80/5);
  //Pupils 
  fill(0);
  ellipse(405, (405- 10), 80/10, 80/10);
  ellipse((405 + 20), (405- 10), 80/10, 80/10);
  //Mouth
  arc(405+10, 405+4, 30, 20, 0, PI, CHORD);
  //Arms
  noFill();  
  strokeWeight(4);
  arc((405+40), (405+14), 30, 50, PI+HALF_PI, TWO_PI);
  arc((405-31), (405+10), 50, 50, PI, PI+HALF_PI);
  strokeWeight(1);
  fill(255);
  ellipse(405+54, 405+20, 15, 15);
  ellipse(405-55, 405+13, 15, 15);
  //Hat
  fill(0);
  ellipse(405, 405-30, 100, 20);
  rectMode(CENTER);
  rect(405, 405-45, 40, 20);
  fill(245, 184, 0);
  ;
  arc(405, 405-43, 40, 30, 0, PI, CHORD);
  fill(0);
  arc(405, 405-43, 40, 15, 0, PI, CHORD);
}


void fizzyHit() {
  //if fizzy touches an urchin or black hole, she dies. 
  for (int i = 0; i<enemyCount; i++) {
    if ((dist(player1.posX, player1.posY, enemies[i].posX, enemies[i].posY)<60)&&(player1.movement!=0)) {

      player1.alive = false;
    } 
    for (int y = 0; y<3; y++) {
      if (dist(player1.posX, player1.posY, blackHolesLoc[0][y], blackHolesLoc[1][y])<30) {
        player1.alive = false;
      }
    }
  }
}

void hourglass() {
  if ((!powerUpFound)&&(dist(player1.posX, player1.posY, powerUp.x, powerUp.y)<30)) {
    fill(255);
    
    powerUpFound = true;
    time+=30;
  } else if (powerUpFound){
    fill(255);
    triangle(powerUp.x, powerUp.y, powerUp.x-20, powerUp.y-20, powerUp.x+20, powerUp.y-20);
    triangle(powerUp.x, powerUp.y, powerUp.x+20, powerUp.y+20, powerUp.x-20, powerUp.y+20);
  }
}

void gameControls() { 
  if (keyPressed) {
    if (handOut) {
      if (key == 'w')      
      { 
        if (player1.movement == 0) {
          player1.movement = 2;
        }
        if (player1.movement==3||player1.movement==4) {
          if ((player1.posX%90==0)&&(player1.posY!=135)) {
            player1.swinging = true;
          } else if ((!player1.swinging)&&(player1.posY!=135)) {
            player1.drawHand = true;
          }
        }
      } else if (key == 's') {
        if (player1.movement == 0) {
          player1.movement = 1;
        } 
        if (player1.movement==3||player1.movement==4) {
          if ((player1.posX%90==0)&&(player1.posY!=675)) {
            player1.swinging = true;
          } else if ((!player1.swinging)&&(player1.posY!=675)) {
            player1.drawHand = true;
          }
        }
      }
      if (key == 'a') {
        if (player1.movement == 0) {
          player1.movement = 3;
        } 
        if (player1.movement==1||player1.movement==2) {
          if ((player1.posY%90==0)&&(player1.posX!=135)) {
            player1.swinging = true;
          } else if ((!player1.swinging)&&(player1.posX!=135)) {
            player1.drawHand = true;
          }
        }
      } else if (key == 'd') 
      { 
        if (player1.movement == 0) {
          player1.movement = 4;
        } else if (player1.movement==1||player1.movement==2) {
          if ((player1.posY%90==0)&&(player1.posX!=675)) {
            player1.swinging = true;
          } 
          if (!player1.swinging) {
            player1.drawHand = true;
          }
        }
      }
    }
  }
}


void levelChanges() {
  //Simply determines the speed of all characters while game is running depending on the level. Would be more efficient code wise to make a 6x3 array. 
  //All speeds must be able to divide 45 without remainders, or else players and enemies alike will be able to travel around without any problems.
  if (level==1) {
    enemyCount = 3;
    for (int i = 0; i<enemyCount; i++) {
      enemies[i].regSpeed = 1;
      enemies[i].fastSpeed = 3;
      player1.speed = 1;
    }
  } else if (level==2) {
    enemyCount = 3;
    for (int i = 0; i<enemyCount; i++) {
      enemies[i].regSpeed = 1.5;
      enemies[i].fastSpeed = 3;
      player1.speed = 1.5;
    }
    //Increasing speed any further will result in fizzy becoming nearly impossible to control. Thus, levels 4 and 5 add challenge through increased enemy count.
  } else if ((level>=3)&&(level<6)) {
    enemyCount = 3+(level-3);
    player1.speed = 3;
    for (int i = 0; i<enemyCount; i++) {
      enemies[i].regSpeed = 3;
      enemies[i].fastSpeed = 9;
    }
    //Could have made the game endless, however once the game hits 6 enemies the game becomes stupidly hard. So I made an ending instead because that would be more satisfying.
  } else if (level>5) {
    gameWon = true;
    gameRunning = false;
  }
}

void resetGame() {
  gameRunning = false;
  player1.lives = 6;
  level = 1; 
  endGameAnimBox= 900;
  resetPositions();
  gameWon = false;
  if (score>highScore) {
    highScore = score;
    String saveScore = str(highScore);
    String[] scoreSaved = split(saveScore, ' ');
    saveStrings("highScore.txt", scoreSaved);
  }
  resetRupees();
  scoreAdded = false;
}
void gameOver() { 
  // death animation
  player1.a+=10;
  if (player1.sizeX >0) {
    player1.sizeX -= 1;
    player1.sizeY -= 1;
  }
  player1.drawFizzy(int(player1.posX), int(player1.posY), int(player1.a));
  //small box moving from bottom of the screen, notifying that the game is over.
  rectMode(CENTER);
  fill(255);
  rect(width/2, endGameAnimBox, 300, 100);
  fill(0);
  textSize(50);
  player1.posX = 405;
  player1.posY = 405;
  text("GAME OVER", width/2 - 140, endGameAnimBox);
  // if the Game Over box is in the middle of the screen, tell user how to restart game. Otherwise, keep moving box up. 
  if (endGameAnimBox>(width/2)-100) {
    endGameAnimBox-=  5;
  } else if ((endGameAnimBox<(width/2))) {
    textSize(15);
    text("Press Any Key Restart", width/2 - 70, endGameAnimBox+20);
  }
}
//resets player and enemy positions to the default when game starts
void resetPositions() {
  player1.movement =0;
  player1.posX = 135;
  player1.posY = 135;
  player1.swinging = false;
  player1.inSwinging = false;
  player1.finishSwinging = false;
  for (int i= 0; i<enemyCount; i++) {
    enemies[i].posX = blackHolesLoc[0][i%3];
    enemies[i].posY = blackHolesLoc[1][i%3];
    enemies[i].movement = int(random(1, 5));
  }
  //picks random place on map for hourglass. 
  int x = int(random(1, 8));
  int y = int(random(1, 8));
  powerUp = new PVector((90*x+45), (90*y)+45);
  powerUpFound = false;
}

//Makes all coins hidden. Done when new level starts, or when starting a new game.
void resetRupees() {
  for (int i=0; i < rupeeCollected.length; i++) {
    rupeeCollected[i] = false;
  }
}


//Basic background design including black holes, game field, and border
void backgroundDesign() {

  fill(0);
  rectMode(CORNER);
  rect(90, 90, 630, 630, 50);
  fill(255);
  //Black holes
  for (int i = 0; i<3; i++) {
    stroke(255);
    noFill();
    strokeWeight(2);

    for (int j = 20; j<=70; j+=10) {
      ellipse(blackHolesLoc[0][i], blackHolesLoc[1][i], j, j);
    }
  }
  strokeWeight(1);
  stroke(0);
  //GUI
  textSize(30);
  text("Time: " + time, 50, 50);
  text("Lives: " + player1.lives, 500, 50);
  text("Coins Left: " + rupLessCount, 250, 50);
  text("Level: " + level, 650, 50);
  text("Score: " + score, 350, 780);
}
void poles() {

  for (int y=180; y<720; y+=90) {
    for (int x = 180; x<720; x+= 90) {
      strokeWeight(2);
      stroke(255);
      fill(#FF5F03);
      ellipse(x, y, 10, 10);
      strokeWeight(2);
    }
  }
  strokeWeight(1);
  stroke(0);
}

void timeLimit() {
  //Time limit ticking down fizzy in each level before losing a life. 
  if (millis() > timeIntv+1000) { 
    timeIntv = millis(); 
    time= time - 1; // decrease int time by 1 every 1000 milliseconds (1 second)
  }
  //If timer reaches 0, kill fizzy then reset timer. 
  if (time<1) {
    player1.alive = false;
    time = 120;
  }
}

void rupee(float x, float y) {
  rupLessCount = 0;
  for (int  i = 0; i<rotatedRupee.length; i++) {
    if (!rupeeCollected[i]) {
      if (dist(x, y, rupeesLoc[0][i], rupeesLoc[1][i]) <30) {
        rupeeCollected[i] = true;
        score+=100;
      }
    }
    // if rupee has been collected, if it is rotated, then the rotated sprite will be drawn at its position. Otherwise, it will be drawn normally (horizontally)
    if (rupeeCollected[i]) {
      if (rotatedRupee[i]) {
        fill(#5BF00C);
        ellipse(rupeesLoc[0][i], rupeesLoc[1][i], 20, 70);
        fill(#66FC59);
        ellipse(rupeesLoc[0][i], rupeesLoc[1][i], 10, 40);
      } else {   
        fill(#5BF00C);
        ellipse(rupeesLoc[0][i], rupeesLoc[1][i], 70, 20);
        fill(#66FC59);
        ellipse(rupeesLoc[0][i], rupeesLoc[1][i], 40, 10);
      }
    } else { 
      rupLessCount++;
    }
  }
  //if all coins collected
  if (rupLessCount == 0) {
    score+=(level+time)*10;
    resetRupees();
    resetPositions();
    level++;
    player1.lives+=5;
    time = 120;
  }
}


class character {
  float speed;
  float posX;
  float posY;
  int sizeX;
  int sizeY;
  int movement;
  int rotate;
  int colour;
  character(float tempSpeed, float tempPosX, float tempPosY, int tempMovement, int tempR, int tempColour) {
    speed = tempSpeed;
    posX = tempPosX;
    posY = tempPosY;
    rotate = tempR;
    movement = tempMovement;
    colour = tempColour;
    sizeX= 70;
    sizeY = 70;
  }
  //Determines how characters move and face depending on their movement.
  void move() {
    if (movement == 1) {
      posY += speed;
      rotate = 0;
    } else if (movement == 2) {
      posY-= speed;
      rotate = 180;
    } else if (movement == 3) {
      posX -= speed;
      rotate = 90;
    } else if (movement == 4) {
      posX += speed;
      rotate = 270;
    }
    if (gameRunning) {
      wallbounce();
    }
  }
  //Bounces characters off wall. when they reach it. 
  void wallbounce() {
    if ((movement == 1)||(movement==2)) {
      if (posY<=135-(player1.speed*2))
      {
        movement =1;
      }
      if (posY>=675+(player1.speed*2)) {
        movement = 2;
      }
    } else if ((movement==3)||(movement==4)) {
      if ((posX<=135-(player1.speed*2))) {
        movement =4;
      }
      if (posX>=675+(player1.speed*2)) {
        movement = 3;
      }
    }
  } 
  //Forces all characters to stay within a set path branching path. Without is, enemies will often be unable to change directions, and players will be unable to swing.
  void keepInLine() {
    //remainderX and remainder Y determine how far off a character is from the designated path.
    float remainderX;
    float remainderY;
    if ((movement == 1)||(movement == 2)) {
      remainderX = posX%45;
      //Move back to the closest path that the characters are allowed to move in.
      if (remainderX<=22) {
        posX += remainderX;
      } else if (remainderX>=23) {
        posX-= remainderX;
      }
      if (posX%90 == 0) {
        //If any character moves onto the poles,  move them back into the designated travelling path. 
        if (posX < width/2) {
          posX +=45;
        } else if (posX>width/2) {
          posX-=45;
        }
      }

      remainderY = posY%speed;
      if (remainderY < speed/2) {
        posY -= remainderY;
      } else if (remainderY>=speed/2) {
        posY += remainderY;
      }
      posY = int(posY);
      posX = int(posX);
    } else if ((movement == 3)||(movement == 4)) {
      posY = int(posY);
      remainderY = posY%45;
      if (remainderY<=22) {
        posY += remainderY;
      } else if (remainderY>=23) {
        posY-= remainderY;
      }
      if (posY%90 == 0) {
        if (posY < height/2) {
          posY +=45;
        } else if (posY>height/2) {
          posY-=45;
        }
      }
      posX = int(posX);
      remainderX = posX%int(speed);
      if (remainderX < speed/2) {
        posX -= remainderX;
      } else if (remainderX>speed/2) {
        posX += remainderX;
      }
      posY = int(posY);
      posX = int(posX);
    }
  }
}

//Urchin Class
class urchin extends character {
  boolean player1Seen;
  float fastSpeed;
  float regSpeed;
  int lastMove;
  float lastSeenX;
  float lastSeenY;
  int handX;
  int handY;
  int spin;
  urchin(float speed, int posX, int posY, int movement, int r) {
    super(speed, posX, posY, movement, r, #2455A2);
    player1Seen = false;
  }

  //AI exclusive to urchins
  private void ai() {
    if (!player1Seen) {
      /*If the player is seen by an urchin, that urchin will chase after fizzy if she is moving (if she wasn't moving she will be white,
       meaning the player has either just started the game or recently died*/
      if ((player1.movement!=0)&&(((movement == 1)&&(player1.posX == posX)&&(player1.posY >posY))||((movement == 2)&&(player1.posX == posX)&&(player1.posY <posY))||((movement == 3)&&(player1.posY == posY)&&(player1.posX <posX))||((movement == 4)&&(player1.posY == posY)&&(player1.posX >posX))))
      { 
        lastSeenX = player1.posX; 
        lastSeenY = player1.posY;  //Coordinate points of where fizzy was last seen
        speed = fastSpeed; //speeds up the Urchins to specified speeds determened by the level the player is on (see function levelChanges).
        player1Seen = true;  
        if ((player1.movement != movement)&&((player1.posX-45)%90 ==0)&&((player1.posY-45)%90 ==0)) {

          lastMove = player1.movement; // if the player was seen between 4 poles, the urchin will remember which way they were moving. Making them seem smarter
        }
      } else if ((posX%90==45)&&(posY%90==45)) {
        movement = int(random(0, 5));  // if player isnt seen, roam around randomly, randomly choosing directions when the urchin is between 4 poles unless the player is seen.
      } else {
        speed = regSpeed; //if player isn't seen, roam around at a regular pace.
      }
    } else if (player1Seen) {
      if (dist(posX, posY, lastSeenX, lastSeenY)<speed+1) {
        if (lastMove >0) {  
          movement = lastMove; // if the urchin reaches rememebers which way fizzy was moving, the urchin will start moving in the direction they saw fizzy moving.
        }
        player1Seen = false; 
        speed = regSpeed; // return to normal status once reaching point where fizzy was seen
        score+=50; //give player points for escaping urchins to encourage risky gameplay.
      }
    }
  }



  private void drawUrchin(float x, float y, int rot) { //Urchin drawn similarly to fizzy, except that the eyes are back with white pupils to give a "creepy" and distinct look.

    pushMatrix();
    translate(x, y);
    fill(colour);
    ellipse(0, 0, sizeX, sizeY);
    rotate(radians(rot));
    //Face
    //Outer "White Bit" of eyes
    fill(0);
    ellipse(10, (10), sizeX/4, sizeY/4);
    ellipse(-10, (10), sizeX/4, sizeY/4);
    //Pupils 
    fill(255);
    ellipse(-10, (10), sizeX/10, sizeY/10);
    ellipse(10, (10), sizeX/10, sizeY/10);
    fill(#FFEA00); 
    ellipse(0, -20, sizeX/3, sizeY/2.5); // Yellow circle on back. Helps player determine more quickly which way the urchins are facing.
    popMatrix();
  }
}


class fizzy extends character {
  boolean alive = true;
  int lives = 5;
  float handX;
  float handY;
  boolean swinging = false;
  boolean inSwinging = false;
  boolean finishSwinging = false;
  float a;
  float aInc;
  int iR;
  boolean drawHand;

  fizzy(float speed, float posX, float posY, int movement, int r, int colour) {
    super(speed, posX, posY, movement, r, #FA0808);
  }
  


  void changeDirect(int m) {
    swinging = false;
    inSwinging = false;
    finishSwinging = false;
    movement = m;
    positionNormalise();
  }


  void positionNormalise() {
    //I forget how this differs from the function keepInLine, but there are a few key changes here that really, REALLY improves the feel of swinging. . 
    //This is why I should have commented this before writing it. Oh well, lesson learned. 
    float remainderX;
    float remainderY;
    if ((movement == 1)||(movement == 2)) {
      posX = int(posX);
      remainderX = posX%45;
      if (remainderX<45) {
        posX += remainderX;
      } else if (remainderX>=45) {
        posX-= remainderX;
      }
      posY = int(posY);
      remainderY = posY%90;
      if (remainderY < 45) {
        posY -= remainderY;
      } else if (remainderY>=45) {
        posY += remainderY;
      }
      if ((movement ==1)&&((a== 360)||(a==0)||(a==180))) {
        posY -=90;
      }
      if ((a==0)||(a==360)) {
        posX-= 43;
      }
    } else if ((movement == 3)||(movement == 4)) {
      posY = int(posY);
      remainderY = posY%45;
      if (remainderY<=22) {
        posY += remainderY;
      } else if (remainderY>=23) {
        posY-= remainderY;
      }
      if (a == 90) {
        posY +=45;
      }
      posX = int(posX);
      remainderX = posX%90;
      if (remainderX < 45) {
        posX -= remainderX;
      } else if (remainderX>=45) {
        posX += remainderX;
      }
      if ((movement ==4)&&((a==270)||(a==90))) {
        posX-=90;
      }
    }
  }

  //Draws fizzy. 
  private void drawFizzy(int x, int y, int rot) {
    pushMatrix();
    translate(x, y);
    fill(colour);
    ellipse(0, 0, sizeX, sizeY);
    rotate(radians(rot));
    //Face
    //Outer "White Bit" of eyes
    fill(255);
    ellipse(10, (10), sizeX/5, sizeY/5);
    ellipse(-10, (10), sizeX/5, sizeY/5);
    //Pupils 
    fill(0);
    ellipse(-10, (10), sizeX/10, sizeY/10);
    ellipse(10, (10), sizeX/10, sizeY/10);
    popMatrix();
  }

  private void swing(int initialRot) {
    initialRot = player1.rotate;

    if (!inSwinging) {
      //Before swinging, game determines which way fizzy should be swinging
      if (key=='d') {
        handX = posX+45;
        handY = posY;
        a=180;
        iR = 180;
        if (movement == 1) {
          aInc = -(player1.speed*2);
        }
        if (movement == 2) {
          aInc = (player1.speed*2);
        }
      } else if (key=='a') {
        handX = posX-45;
        handY = posY;
        a= 0;
        iR = 360;
        if (movement == 1) {
          aInc = (player1.speed*2);
        }
        if (movement == 2) {
          aInc = -(player1.speed*2);
        }
      } else if (key=='w') { 
        handX = posX;
        handY = posY- 45;
        a= 90;
        iR = 90;
        if (movement == 3) {
          aInc = (player1.speed*2);
        }
        if (movement == 4) {
          aInc = -(player1.speed*2);
        }
      } else if (key=='s') { 
        handX = posX;
        handY = posY + 45;
        a= 270;
        iR = 270;
        if (movement == 3) {
          aInc = -(player1.speed*2);
        }
        if (movement == 4) {
          aInc = (player1.speed*2);
        }
      }
      //Confirms that values have been set and thus puts fizzy into cirular motion.
      inSwinging = true;
    } else {     
      fill(#FF8C00);
      //rotates fizzy around her own hand. At this point, hand will be stuck to a pole
      posX = handX +cos(radians(a))*45;
      posY = handY +sin(radians(a))*45;
      ellipse(handX, handY, 15, 15);
      a+= aInc;

      if (a>=360) {
        a = 0;
      } else if (a<=1) {
        a = 360;
      }
      rotate = initialRot - int(-aInc);
    }
  }
  //Executes once player lets go of a key (see keyReleased())
  private void finishSwing() {
    swing(rotate);
    drawHand = false;
    if (movement == 1) {
      if ((iR == 360)||(iR == 0)) {
        if (a == 180 ) {
          changeDirect(2);
        } else if (a == 90) {
          changeDirect(3);
        } else if (a == 270) {
          changeDirect(4);
        } else if ((a == 0)||(a==360)) {
          changeDirect(1);
        }
      } else if ((iR == 180)) {
        if ((a==360)||(a==0)) {
          changeDirect(2);
        } else if (a == 90) {
          changeDirect(4);
        } else if (a == 270) {
          changeDirect(3);
        } else if (a== 180) {
          changeDirect(1);
        }
      }
    } else if (movement == 2) {
      if ((iR == 360)||(iR == 0)) {
        if ((a==360)||(a==0)) {
          changeDirect(2);
        } else if (a == 90) {
          changeDirect(4);
        } else if (a == 270) {
          changeDirect(3);
        } else if (a== 180) {
          changeDirect(1);
        }
      } else if ((iR == 180)) {
        if (a == 180 ) {
          changeDirect(2);
        } else if (a == 90) {
          changeDirect(3);
        } else if (a == 270) {
          changeDirect(4);
        } else if ((a == 0)||(a==360)) {
          changeDirect(1);
        }
      }
    } else if (movement == 4) {
      if (iR == 90) {
        if ((a==360)||(a==0)) {
          changeDirect(2);
        } else if (a == 90) {
          changeDirect(4);
        } else if (a == 270) {
          changeDirect(3);
        } else if (a== 180) {
          changeDirect(1);
        }
      } else if (iR == 270) {
        if ((a==360)||(a==0)) {
          changeDirect(1);
        } else if (a == 90) {
          changeDirect(3);
        } else if (a == 270) {
          changeDirect(4);
        } else if (a== 180) {
          changeDirect(2);
        }
      }
    } else if (movement == 3) {
      if (iR == 90) {
        if ((a==360)||(a==0)) {
          changeDirect(1);
        } else if (a == 90) {
          changeDirect(3);
        } else if (a == 270) {
          changeDirect(4);
        } else if (a== 180) {
          changeDirect(2);
        }
      } else if (iR == 270) {
        if ((a==360)||(a==0)) {
          changeDirect(2);
        } else if (a == 90) {
          changeDirect(4);
        } else if (a == 270) {
          changeDirect(3);
        } else if (a== 180) {
          changeDirect(1);
        }
      }
    }
  }
  private void hand() {
    if (key=='w') {
      fill(#FF8C00);
      ellipse(player1.posX, player1.posY-45, 15, 15);
    } else if (key == 's') {
      fill(#FF8C00);
      ellipse(player1.posX, player1.posY+45, 15, 15);
    } else if (key == 'a') {
      fill(#FF8C00);
      ellipse(player1.posX -45, player1.posY, 15, 15);
    } else if (key == 'd') {
      fill(#FF8C00);
      ellipse(player1.posX+45, player1.posY, 15, 15);
    }
  }
  //Death animation. Fizzy rotates and shrinks.
  void death() {
    a+=10;
    drawFizzy(int(player1.posX), int(player1.posY), int(a));
    sizeX -= 1;
    sizeY -= 1;
    if (sizeX <= 0) {
      lives--;
      posX = 135;
      posY = 135;
      movement = 0;
      rotate = 0;
      sizeX = 70;
      sizeY = 70;
      a = 0;
      swinging = false;
      inSwinging = false;
      finishSwinging = false;
      for (int i= 0; i<enemyCount; i++) {
        enemies[i].player1Seen = false;
      }

      if (lives>0) {
        alive = true;
      }
    }
  }
}


//MAIN MENU: kept seperate from most of the code as we can treat it as a subprogram 
void mainMenu() {
  //background of Main Menu. Features multiple fizzies spinning at random speeds, with Urchins (3 times the amount as fizzies) also move down but don't spin
  background(#45830C);
  for (int i = 0; i<backgroundFizzies.length; i++) {
    backgroundFizzies[i].move();   
    backgroundFizzies[i].a += backgroundFizzies[i].aInc;
    if (backgroundFizzies[i].posY>900) {
      backgroundFizzies[i].aInc = random(-10, 20);
      backgroundFizzies[i].posY = -100;
      backgroundFizzies[i].posX = random(30, width-20);
      backgroundFizzies[i].speed = (int (random(1, 10)));
    }
    backgroundFizzies[i].drawFizzy(int(backgroundFizzies[i].posX), int(backgroundFizzies[i].posY), int(backgroundFizzies[i].a));
  }
  for (int i = 0; i<backgroundUrchins.length; i++) {
    backgroundUrchins[i].move();
    if (backgroundUrchins[i].posY>900) {
      backgroundUrchins[i].posY = -100;
      backgroundUrchins[i].posX = random(30, width-20);
      backgroundUrchins[i].speed = int(random(1, 10));
    }
    backgroundUrchins[i].drawUrchin(int(backgroundUrchins[i].posX), int(backgroundUrchins[i].posY), 0);
  }
  //MENU INTERFACE
  fill(#F7020B);
  ellipse(width/2-10, height/2-120, 700, 200);
  fill(255);
  ellipse(width/2-10, height/2-120, 500, 100);
  //play button
  rect(290, 420, 200, 50);
  //How2Play button
  rect(290, 520, 200, 50);
  //High Score thing;
  ellipse(width/2, height-100, 500, 100);
  textSize(60);
  fill(0);
  text("Kuru Country", 200, height/2-100);
  rectMode(CORNER);
  textSize(30);
  text("Play!", 360, 450);
  text("How to Play", 305, 550);
  text("High Score:" + " " + highScore, width/2 - 200, height-100);

  if (mousePressed) {
    if (showHelp ==false) {
      if ((mouseX>290)&&(mouseX<490)&&(mouseY>420)&&(mouseY<470)) {
        gameRunning = true;
        showHelp = false;
        level = 1;
        time = 120;
        enemyCount = 3;
        resetPositions();
      } else if ((mouseX>290)&&(mouseX<490)&&(mouseY>520)&&(mouseY<620)) {
        showHelp = true;
      }
    }
  }
  if (showHelp == true) {
    helpScreen();
  }
}
void helpScreen() {
  //Bounding box for screen contents
  fill(255);
  rect(50, 50, 700, 750);
  //Button to go back to menu
  fill(#F3F71B);
  rect(180, 660, 400, 80);
  fill(0);
  textSize(30);
  //titles
  text("Objective", 80, 90);
  text("Controls", 80, 200);
  text("Scoring Points", 80, 430);
  textSize(15);
  //Objective
  text("Guide Fizzy, the famous red treasure hunter, and help her find all the green rupees hidden", 80, 120);
  text("throughout each level. Becareful of the evil space pirates known as the Blue Urchins,", 80, 140);
  text("who want to use the rupees to take over Kuru Country!", 80, 160);
  //Controls
  text("At start-up, s to move down, d to move right", 80, 220);
  text("When moving left and right:", 80, 240);
  text("Hold d for Fizzy to stick hand out above.", 80, 260);
  text("Hold w for fizzy to stick hand out below.", 80, 280);
  text("When moving up and down:", 80, 310);
  text("Hold a for Fizzy to stick hand out to the left.", 80, 330);
  text("Hold d for Fizzy to stick hand out to the right.", 80, 350);
  text("When Fizzy's hand touches a pole, she will hold on and swing around it until you let go", 80, 380);
  //Scoring
  text("Collecting a coin: 100 points", 80, 460);
  text("Finishing a level: 10 points for every second left on the timer", 80, 480);
  text("Escaping a Urchin chasing Fizzy: 50 points", 80, 500);
  text("Find the hidden hourglass for extra time!", 80, 520);
  
  textSize(40);
  text("Back to Main Menu", 200, 710);
  if (mousePressed) {
    if ((mouseX>180)&&(mouseX<580)&&(mouseY>660)&&(mouseY<740)) {
      showHelp = false;
    }
  }
}