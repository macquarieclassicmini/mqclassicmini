hardhat V2 contains the necessary levelling system for the assignment and my final function at line 631 (levellingSystem2) contains all the code to achieve the effect needed except certain triggers.

hardhat uses my levelling system which acts on the speed and frequency of the falling objects.
everytime the character reaches the door the play goes up one level.
int difficulty triggers different weapons to fall at different times by producing a random number between 0 and the value of difficulty. 1000 is deducted from difficulty every second time the character passes through the door until difficulty reaches 5000. At 5000 the weapons drop the most frequently, often more than one at the same time.
int speed changes the rate the objects fall at at a constant rate every time the character reaches the door. The objects originally fall one spot every second and their speed caps at five spots every second. 
weapons fall the fastest after level 48 and fall the most frequently after level 70, meaning after level 70 the game stops getting harder.
there are three hearts in the top right corner to indicate lives.
once the lives run out the score is calculated by multiplying the amount of objects that reached the ground by the level the player was one when they lost their final life. the player is returned to the main menu and their score is displayed.
