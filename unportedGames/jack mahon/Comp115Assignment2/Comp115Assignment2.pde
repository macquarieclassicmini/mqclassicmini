

void setup () { //I think it's mostly self explanatory but I've annoted here and there
  size (512, 348); //recquired size
}

boolean startGame1 = false;

boolean startGame2 = false;

boolean levelUp = false;

int life = 3;

int level = 1;

int score = 0;
int score2 = 0;

float counter = random(180, 480); //creates the 3-8 second r (60fps-default)

int avatarX = 64; //for my man to move, I realised too late that y wasn't necessary but I left it just incase I want to play with this after the assignment is finished
int avatarY = 298;

int brickY = 0; //for my objects to fall
int knifeY = 0;
int hammerY = 0;
int bowlingBallY = 0;
int weightY = 0;

boolean brick = false; // triggers for my objects (or weapons) to fall
boolean knife = false;
boolean hammer = false;
boolean bowlingBall = false;
boolean weight = false;

int brickFallRate = 0; // this was to help them "jump" like in the original game
int knifeFallRate = 0;
int hammerFallRate = 0;
int bowlingBallFallRate = 0;
int weightFallRate = 0;

int speed = 60;

boolean door = false; // door starts closed

float dropper = random (0, 5000); //decides which weapons to drop

int heartAmount = 3;

float difficulty = 40000; //initial difficulty (the higher the easier)

int wholeBlackLine = 0;
int wholeRedLine = 0;

int cloud1x = 0;
float cloud2x = 0.0;

void draw () {
  backgroundScene (); 

  if (startGame1 == false && startGame2 == false) {  //menu screen
    men ();
    mrGW ();
  }
  if (startGame1 == true) {  //actual game

    commonGameFunctionality ();
    game1Functionality ();
  }

  if (startGame2 == true) {

    commonGameFunctionality ();
    game2Functionality ();
  }
}

void commonGameFunctionality () {
  door1 (); //thought it looked good just as a white portal thing

  doorTrigger ();//activating and deactivating my objective door
  if (door == true) {
    door2Open ();
  }

  if (door == false) {
    door2Closed ();
  } 

  mrGW (); // avatar

  //weapons
  if (difficulty > 6000) {
    if ((dropper>0) && (dropper<1200)) {//used thousands as it left more room to edit if I needed to
      brick = true;
    }
    if ((dropper>1000) && (dropper<2200)) {
      knife = true;
    }
    if ((dropper>2000) && (dropper<3200)) {
      hammer = true;
    }
    if ((dropper>3000) && (dropper<4200)) {
      bowlingBall = true;
    }
    if ((dropper>4000) && (dropper<5000)) {
      weight = true;
    }
  }


  if (brick == true) { //creates and drops weapons
    weapon1();
  }
  if (knife == true) {
    weapon2();
  }
  if (hammer == true) {
    weapon3();
  }
  if (bowlingBall == true) {
    weapon4();
  }
  if (weight == true) {
    weapon5();
  }

  if ((avatarX==128)&&(brickY == 240)) { // kills MrGW when he's stepped in the wrong spot
    death();
  }
  if ((avatarX==192)&&(knifeY == 240)) {
    death();
  }
  if ((avatarX==256)&&(hammerY == 240)) {
    death();
  }
  if ((avatarX==320)&&(bowlingBallY == 240)) {
    death();
  }
  if ((avatarX==384)&&(weightY == 240)) {
    death();
  }
}

void game1Functionality () {
  if (avatarX == 448) {  //interactions when MrGW reaches the objective
    avatarX = 64;
    level = level + 1;
    levelUp = true;
  }

  levelDisplay ();// level 

  for (int i = 0; i < heartAmount; i ++) {
    int HeartXPos = 480 - i * 30; 
    heart (HeartXPos);
  }

  if (difficulty < 6000) {
    if ((dropper>0) && (dropper<1600)) {//this is to make the game harder the higher you go
      brick = true;
    }
    if ((dropper>1000) && (dropper<2600)) { 
      knife = true;
    }
    if ((dropper>2000) && (dropper<3600)) {
      hammer = true;
    }
    if ((dropper>3000) && (dropper<4600)) {
      bowlingBall = true;
    }
    if ((dropper>4000) && (dropper<5000)) {
      weight = true;
    }
    if ((dropper>2000) && (dropper<2400)) {
      brick = true;
    }
    if ((dropper>3000) && (dropper<3400)) {
      knife = true;
    }
    if ((dropper>4000) && (dropper<4400)) {
      hammer = true;
    }
    if ((dropper>0) && (dropper<400)) {
      bowlingBall = true;
    }
    if ((dropper>1000) && (dropper<1400)) {
      weight = true;
    }
  }


  if (levelUp == true) {
    if (level % 2 == 0 && difficulty > 5999) {
      difficulty = difficulty - 1000; //makes the objects fall more often the high level you go
    }
    if (speed > 12) {
      speed = speed - 1;
    }
    levelUp = false;
  }
  if (life == 0) { //ends game
    score = score * level;
    startGame1 = false;
  }
}

void game2Functionality () {
  if (avatarX == 448) {  //interactions when MrGW reaches the objective
    avatarX = 64;
    score2 = score2 + 3;
  }
  levellingSystem2 (86, 86, 84);
}

void mrGW () {
  fill (0);
  noStroke();
  ellipse (avatarX, avatarY, 20, 20); // head
  ellipse (avatarX+10, avatarY, 10, 10); // nose
  rect (avatarX-5, avatarY+7, 10, 25, 80); // body
  stroke (1);
  strokeWeight (3);
  line (avatarX-5, avatarY+12, avatarX-8, avatarY+27); // left arm
  line (avatarX+4, avatarY+12, avatarX+7, avatarY+27); // right arm
  strokeWeight (5);
  line (avatarX-3, avatarY+30, avatarX-4, avatarY+45); //left leg
  line (avatarX+2, avatarY+30, avatarX+3, avatarY+45); // right leg
  noStroke ();
  ellipse (avatarX-8, avatarY+27, 6, 6); // left hand
  ellipse (avatarX+8, avatarY+27, 6, 6); // right hand
  ellipse (avatarX-5, avatarY+48, 8, 4); // left foot
  ellipse (avatarX+5, avatarY+48, 8, 4); // right foot
  fill (255, 255, 0);
  rect (avatarX-12, avatarY-10, 24, 5, 50, 50, 0, 0); // hat rim
  rect (avatarX-8, avatarY-15, 16, 10, 50); //hat top
}

void keyPressed () {  //keys make him go left and right
  if ((keyCode == LEFT) && (avatarX>64) && (startGame1 == true || startGame2 == true)) {
    avatarX = avatarX - 64;
  } 
  if ((door==true) && (keyCode == RIGHT)&& (avatarX<444) && (startGame1 == true || startGame2 == true)) {
    avatarX = avatarX +64;
  }
  if ((door==false) && (keyCode == RIGHT)&& (avatarX<384) && (startGame1 == true || startGame2 == true)) {
    avatarX = avatarX +64;
  }
}

void backgroundScene () {
  fill (105, 225, 229); //new background
  rect (-1, -1, 514, 350);

  fill (3, 165, 6); // bushes

  noStroke();

  ellipse (130, 326, 80, 80);
  ellipse (230, 326, 80, 300);
  ellipse (360, 326, 60, 230);
  ellipse (180, 326, 100, 80);
  ellipse (300, 316, 80, 80);

  strokeWeight (1);
  stroke(1);
  fill (209, 64, 2); // wall
  rect (0, 324, 512, 24);
  rect (0, 322, 512, 4);

  fill (150);
  rect (-1, 261, 131, 90);
  rect (30, 201, 10, 60);
  rect (90, 201, 10, 60);
  fill (140);
  rect (-1, 260, 141, 10);
  fill (255);
  rect (10, 180, 110, 60, 20);
  fill(18, 140, 2);
  rect (15, 185, 100, 50, 15);
  fill (0);
  textSize (20);
  text ("train", 44, 205);
  text ("station", 33, 225);
  textSize (40);
  text("1", 6, 320) ; //train station 1
  fill (150);
  rect (382, 261, 131, 90);
  rect (472, 201, 10, 60);
  rect (412, 201, 10, 60);
  fill (140);
  rect (372, 260, 141, 10);
  fill (255);
  rect (392, 180, 110, 60, 20);
  fill(18, 140, 2);
  rect (397, 185, 100, 50, 15);
  fill (0);
  textSize (20);
  text ("train", 426, 205);
  text ("station", 415, 225);
  textSize (40);
  text("2", 390, 320) ; //train station 2  
  if (cloud1x >= 512) {
    cloud1x = 0;
  }
  if (cloud2x >= 512) {
    cloud2x = 0.0;
  }
  noStroke ();
  fill (255);
  ellipse (cloud1x + 60, 60, 50, 20);
  ellipse (cloud1x + 110, 50, 80, 40);
  ellipse (cloud1x - 452, 60, 50, 20);
  ellipse (cloud1x - 402, 50, 80, 40);
  cloud1x = cloud1x + 1;
  ellipse (cloud2x + 460, 120, 60, 30);
  ellipse (cloud2x + 410, 130, 80, 40);
  ellipse (cloud2x + 470, 140, 90, 30);
  ellipse (cloud2x - 52, 120, 60, 30);
  ellipse (cloud2x - 102, 130, 80, 40);
  ellipse (cloud2x - 42, 140, 90, 30);
  cloud2x = cloud2x + 0.5;
  door1 ();
  door2Closed ();
}

void door1() { //starting door
  fill (255);
  rect (41, 278, 50, 70);
}

void door2Open () { //open door on the right
  fill (255);
  rect (424, 278, 50, 70);
  fill (140, 88, 16);
  quad (474, 278, 494, 260, 494, 348, 474, 348);
}

void door2Closed () {  //closed door on the right
  fill (140, 88, 16);
  rect (424, 278, 50, 70);
  stroke (1);
  strokeWeight (1);
  rect (427, 281, 20, 30);
  rect (427, 315, 20, 30);
  rect (451, 281, 20, 30);
  rect (451, 315, 20, 30);
  fill (255, 255, 0);
  ellipse (430, 313, 5, 5);
}

void levelDisplay () { //display of level, top left
  fill (0);
  textSize (25);
  text ("level " + level, 10, 30);
}

void doorTrigger () { //opens and closes door according to the counter
  counter = counter - 1;
  if (counter <= 0) {
    door = !door;
    counter = random(180, 480);
  }
}

void weapon1 () { //a brick
  brickFallRate = brickFallRate + 1;

  strokeWeight(1);
  stroke(1);
  fill (242, 101, 7);
  quad (128, brickY, 148, brickY+10, 128, brickY+32, 108, brickY+20);
  quad (108, brickY+21, 128, brickY+33, 128, brickY+46, 108, brickY+33);
  quad (149, brickY+11, 149, brickY+23, 129, brickY+46, 129, brickY+33);
  fill (0);
  line (128, brickY+32, 108, brickY+20);
  line (148, brickY+10, 128, brickY+32);
  line (128, brickY+32, 128, brickY+45);
  if (brickFallRate >= speed) {
    brickY = brickY + 60;
    brickFallRate = 0;
    dropper = random (1, difficulty);
  }

  if ((brickY>240)&&(dropper>5000)) {
    dropper = random (5000);
  }
  if (brickY>240) {
    brickY = 0;
    brickFallRate = 0;
    brick = false;
    if (startGame1 == true) {
      score = score + 1;
    }
    if (startGame2 == true) {
      score2 ++;
    }
  }
}
void weapon2 () { // a knife
  knifeFallRate = knifeFallRate + 1;
  strokeWeight(1);
  stroke(1);
  fill (200);
  quad (187, knifeY, 197, knifeY, 197, knifeY+30, 187, knifeY+45);
  fill (0);
  rect (182, knifeY+12, 20, 5);
  rect (187, knifeY, 10, 12);
  if (knifeFallRate >= speed) {
    knifeFallRate = 0;
    knifeY = knifeY + 60;
    dropper = random (1, difficulty);
  }

  if ((knifeY>240)&&(dropper>5000)) {
    dropper = random (5000);
  }
  if (knifeY>240) {
    knifeY = 0;
    knifeFallRate = 0;
    if (startGame1 == true) {
      score = score + 1;
    }
    knife = false;
    if (startGame2 == true) {
      score2 ++;
    }
  }
}

void weapon3 () { //a hammer
  hammerFallRate = hammerFallRate + 1;
  strokeWeight(1);
  stroke(1);
  fill (160, 104, 8);
  rect (253, hammerY, 6, 45);
  fill (0);
  rect (243, hammerY+30, 26, 10);
  if (hammerFallRate >= speed) {
    hammerFallRate = 0;
    hammerY = hammerY + 60;
    dropper = random (1, difficulty);
  }

  if ((hammerY>240)&&(dropper>5000)) {
    dropper = random (5000);
  }
  if (hammerY>240) {
    hammerY = 0;
    if (startGame1 == true) {
      score = score + 1;
    }
    hammerFallRate = 0;
    hammer = false;
    if (startGame2 == true) {
      score2 ++;
    }
  }
}

void weapon4 () { //a bowling ball
  strokeWeight(1);
  stroke(1);
  bowlingBallFallRate = bowlingBallFallRate + 1;
  fill (99, 1, 63);
  ellipse (320, bowlingBallY+22, 45, 45);
  fill (0);
  ellipse (322, bowlingBallY+11, 5, 5);
  ellipse (332, bowlingBallY + 13, 5, 5);
  ellipse (325, bowlingBallY + 25, 5, 5);
  if (bowlingBallFallRate >= speed) {
    bowlingBallFallRate = 0;
    bowlingBallY = bowlingBallY + 60;
    dropper = random (1, difficulty);
  }
  if ((bowlingBallY>240)&&(dropper>5000)) {
    dropper = random (5000);
  }
  if (bowlingBallY>240) {
    bowlingBallY = 0;
    if (startGame1 == true) {
      score = score + 1;
    }
    bowlingBallFallRate = 0;
    bowlingBall = false;
    if (startGame2 == true) {
      score2 ++;
    }
  }
}

void weapon5 () { //an old timey cartoon weight
  weightFallRate = weightFallRate + 1;
  strokeWeight(1);
  stroke(1);
  fill (0);
  ellipse (384, weightY+9, 18, 18);
  quad (369, weightY+14, 399, weightY+14, 404, weightY+45, 364, weightY+45);
  fill (105, 225, 229);
  ellipse (384, weightY+9, 10, 10);
  fill (100);
  textSize (10);
  text ("100", 373.5, weightY +33);
  if (weightFallRate >= speed) {
    weightFallRate = 0;
    weightY = weightY + 60;
    dropper = random (1, difficulty);
  }
  if ((weightY>240)&&(dropper>5000)) {
    dropper = random (5000);
  }
  if (weightY>240) {
    weightY = 0;
    weightFallRate = 0;
    if (startGame1 == true) {
      score = score + 1;
    }
    weight = false;
    if (startGame2 == true) {
      score2 ++;
    }
  }
}
void men() {
  fill (0);
  rect (44, 50, 200, 80);
  fill (255, 255, 0);
  textSize (40);
  text ("Hard Hat", 57, 104);
  textSize (30);
  fill (0);
  text ("score " + score, 190, 319); //displays score from  last game
  fill (255, 255, 0);
  textSize (40);
  if ((mouseX>44)&&(mouseX<244)&&(mouseY>50)&&(mouseY<130)) { //highlights menu (style choice)
    fill (255, 255, 0);
    rect (44, 50, 200, 80);
    fill (0);
    text ("Hard Hat", 57, 104);
  }
  // second button
  fill (0);
  rect (268, 50, 200, 80);
  fill (255, 255, 0);
  textSize (40);
  text ("Hard Hat", 281, 94);
  textSize (30);
  text ("V2", 351, 124);
  if ((mouseX>268)&&(mouseX<468)&&(mouseY>50)&&(mouseY<130)) { //highlights menu (style choice)
    fill (255, 255, 0);
    rect (268, 50, 200, 80);
    fill (0);
    textSize (40);
    text ("Hard Hat", 281, 94);
    textSize (30);
    text ("V2", 351, 124);
  }
}


void mousePressed () {
  if ((mouseX>268)&&(mouseX<468)&&(mouseY>50)&&(mouseY<130) && (startGame2 == false && startGame1 == false)) {
    startGame2 = true;
    brick = false;
    knife = false;
    hammer = false;
    bowlingBall = false;
    weight = false;
    brickY = 0;
    knifeY = 0;
    hammerY = 0;
    bowlingBallY = 0;
    weightY = 0;
    dropper = random (0, 5000);
    counter = random(180, 480);
    speed = 60;
    difficulty = 15000;
  }
  if ((mouseX>44)&&(mouseX<244)&&(mouseY>50)&&(mouseY<130) && (startGame2 == false && startGame1 == false)) {
    life = 3;
    startGame1 = true; //starts game again and resets values
    brick = false;
    knife = false;
    hammer = false;
    bowlingBall = false;
    weight = false;

    level = 1;

    brickY = 0;
    knifeY = 0;
    hammerY = 0;
    bowlingBallY = 0;
    weightY = 0;

    speed = 60;

    dropper = random (0, 5000);
    counter = random(180, 480);

    difficulty = 40000;

    score = 0;

    heartAmount = 3;
  }
}

void death () { // resets MrGW and takes a life from him
  avatarX = 64;
  if (startGame1 == true) {
    life = life - 1;
    heartAmount = heartAmount - 1;
  }
  if (startGame2 == true) {
    wholeRedLine ++;
  }
}
void heart (int heartx) {
  noStroke();
  fill (255, 0, 0);
  rect (heartx, 10, 6, 3);
  rect (heartx+12, 10, 6, 3);
  rect (heartx-3, 13, 24, 6);
  rect (heartx, 19, 18, 3);
  rect (heartx+3, 21, 12, 3);
  rect (heartx+6, 24, 6, 3);
}

void levellingSystem2 (int xloc, int yloc, int size) { // assignment 2 levelling system
  if (score2>6) {
    score2 = score2 - 6;
    wholeBlackLine ++;
  }

  for (int k = 1; k < 16; k ++) { // gradient loop
    for (int v = 0; v < 6; v ++) { //draws whole red line
      for (int m = 0; m <= wholeRedLine; m ++) { // draws it when hit on head and makes sure to not draw it over black circles
        fill (255, 0, 0, 10*k);
        noStroke ();
        ellipse (xloc * v + 42, (m*86) - 43, size-4*k, size-4*k);
      }
    }
    for (int t = 0; t < score2; t ++) { //left to right
      fill (0, 10*k);
      noStroke ();
      ellipse (xloc*t +42, yloc*wholeBlackLine +43 + (wholeRedLine*86), size-4*k, size-4*k);
    }
    for (int u = 0; u < 6; u++) {
      if (wholeBlackLine >= 1) {
        ellipse (xloc*u +42, yloc*(wholeBlackLine-1) +43 + (wholeRedLine*86), size-4*k, size-4*k);
      }
    }
    for (int u = 0; u < 6; u++) {
      if (wholeBlackLine >= 2) {
        ellipse (xloc*u +42, yloc*(wholeBlackLine-2) +43+ (wholeRedLine*86), size-4*k, size-4*k);
      }
    }
    for (int u = 0; u < 6; u++) {
      if (wholeBlackLine >= 3) {
        ellipse (xloc*u +42, yloc*(wholeBlackLine-3) +43+ (wholeRedLine*86), size-4*k, size-4*k);
      }
    }
  }
  if (wholeBlackLine+wholeRedLine >= 4) {
    startGame2 = false;
    wholeBlackLine = 0;
    score2 = 0;
    wholeRedLine = 0;
    avatarX = 64;
  }
}