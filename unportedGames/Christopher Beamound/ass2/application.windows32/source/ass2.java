import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class ass2 extends PApplet {

/* This was a lot of fun. So much more to do, many more ideas, but this will be enough. The game uses a state system for each of the scenes/rooms, so each scene
 has its own setup and update function. The rest of the functions are supportive, either drawing, collision detection or for utlity use.
 
 Since this code is a little complicated, feel free to email me at christopher.beamond@students.mq.edu.au after marking is complete if there are any questions.
 */

boolean debug = false;
//size of each cell in the grid
final int cellSize = 90;
final int halfCellSize = 45;
final int cellsWide = 9;
final int cellsHigh = 9;
final int cellCount = 9 * 9; //cellsWide * cellsHigh

//fizzy varibles
int fizzyX;
int fizzyY;
final int fizzySize = 50;
final int fizzyHalfSize = 25;
float fizzyDefaultSpeed = 2.5f;
float fizzyMaxSpeed = 4.8f;
float fizzySpeed;
int fizzyRotation;

//These are constants to replace fizzy rotation magic values
final int fizzyRotationUp = 0;
final int fizzyRotationRight = 90;
final int fizzyRotationDown = 180;
final int fizzyRotationLeft = 270;

//rupee varibles
int rupeeCount;
final int rupeeSize = 40;
final int rupeeRotationTimeMS = 1400;
final int rupeeColour = 0xff00FF00;
final int rupeeDebugColour = 0xff00FF00;
final int rupeeMinSpawnChance = 10;
final int rupeeMaxSpawnChance = 60;
int rupeesLocated;

//black hole varibles
final int minBlackHoleRandomCount = 2;
final int maxBlackHoleRandomCount = 6;
final int blackHoleRadius = 25;

//rupee UI icon varibles
final int rupeeIconX = 10;
final int rupeeIconY = 15;
final int rupeeIconSize = 20;

// The default grid of rupees to find. Feel free to change this!!
// !! IMPORTANT !! Grid *must* be cellsWide x cellsHigh 
final int cellEmpty = 0;
final int cellRupee = 1;
final int cellRupeeFound = 2;
final int cellFizzy = 3;
final int cellBlackHole = 4;
final int[] defaultRupeeGrid = 
  {
  0, 0, 0, 0, 3, 0, 0, 0, 0, 
  0, 0, 1, 1, 0, 1, 1, 0, 0, 
  0, 1, 0, 0, 1, 0, 0, 1, 0, 
  0, 1, 0, 0, 0, 0, 0, 1, 0, 
  0, 1, 0, 4, 4, 4, 0, 1, 0, 
  0, 0, 1, 0, 0, 0, 1, 0, 0, 
  0, 0, 0, 1, 0, 1, 0, 0, 0, 
  0, 4, 0, 0, 1, 0, 0, 4, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0
};

//Use a 1D array instead of a 2D. This makes it much easier to traverese through the array
//at the cost of slightly more advanced math (most of which is handled in functions near the bottom) 
int[] rupeeGrid;

//we can control the gameplay flow using gameState
int gameState;
final int gameStateIntro = 0;
final int gameStatePlaying = 1;
final int gameStateWon = 2;
final int gameStateNoRupees = 3;
final int gameStateEditor = 4;
final int gameStateLost = 5;

boolean editorShowHelp = false;

//make sure that fizzy doesn't start moving until there is player input
boolean gameStarted = false;

public void setup()
{
  

  //start the game by going to the intro state
  stateIntro();
}

public void draw()
{
  //clear previous frame
  background(0);

  switch(gameState)
  {
  case gameStateIntro: 
    updateIntro(); 
    break;
  case gameStatePlaying: 
    updatePlaying(); 
    break;
  case gameStateEditor: 
    updateEditor(); 
    break;
  case gameStateWon: 
    updateWin(); 
    break;
  case gameStateNoRupees: 
    updateNoRupees(); 
    break;
  case gameStateLost: 
    updateLost(); 
    break;
  }

  //If in debug mode, alert the user by diplaying Debug Enabled in bottom left of the screen
  if (debug == true)
  {
    fill(255);
    textSize(12);
    textAlign(LEFT, BOTTOM);
    text("Debug Enabled", 10, height - 10);
  }
}

public void stateIntro()
{
  gameState = gameStateIntro;
  setRupeeGridToDefault();
}

public void statePlaying()
{
  gameState = gameStatePlaying;
  gameStarted = false;
  fizzySpeed = fizzyDefaultSpeed;
  setupObjects();
  setupFizzy();
}

public void stateEditor()
{
  gameState = gameStateEditor;
  setupObjects();
  setupFizzy();
}

public void stateWin()
{
  gameState = gameStateWon;

  //move fizzy to below the game win text. Since fizzy will not move when the
  //game is won, this is all we need to do
  fizzyRotation = fizzyRotationDown;
  fizzyX = width/2;
  fizzyY = height/2 + height/6;
}

public void stateNoRupees()
{
  gameState = gameStateNoRupees;
}

public void stateLost()
{
  gameState = gameStateLost;
}

public void updateIntro()
{
  //draw game intro text
  if (gameState == gameStateIntro)
  {
    fill(255, 0, 0);
    textSize(24);
    textAlign(CENTER, CENTER);
    text("Help Fizzy needs to find her missing rupees!\nUse W,A,S,D to move fizzy and find her rupees\n\nPress Spacebar to Play!\nPress E to enter the editor at any time", width/2, height/2);
    return;
  }
}

public void updatePlaying()
{
  //no rupees exist! Nice work trying to break the game ;)
  if (rupeeCount == 0)
  {
    stateNoRupees();
    return;
  }

  //All rupees found!! Show win screen
  if (rupeesLocated == rupeeCount)
  {
    stateWin();
    return;
  }

  //enable movement adn collisions only if the game has started
  if (gameStarted == true)
  {
    //move fizzy
    if (fizzyRotation == fizzyRotationUp)
      fizzyY -= fizzySpeed;  
    else if (fizzyRotation == fizzyRotationLeft)
      fizzyX -= fizzySpeed;
    else if (fizzyRotation == fizzyRotationDown)
      fizzyY += fizzySpeed;
    else if (fizzyRotation == fizzyRotationRight)
      fizzyX += fizzySpeed;

    //check for collisions
    collisionFizzyObjectCheck();
    collisionFizzyWallCheck();
  }

  //draw everything
  drawPoles();
  drawRupees();
  drawBlackHoles();
  drawFizzy(fizzyX, fizzyY, fizzyRotation);
  drawRupeeCount(rupeeCount - rupeesLocated);
}


public void updateEditor()
{
  if (editorShowHelp == true)
  {
    //draw editor help text and do nothing else
    fill(255, 0, 0);
    textSize(24);
    textAlign(CENTER, CENTER);
    text("Left click to place a rupee\nRight click to remove a rupee\nPress B to place a black hole\nPress F to place Fizzy\nPress Q for a randomised map (instantly plays)\nPress E to begin playing\nPress R to reset the grid to default\n\nPress H to enable/disable help", width / 2, height / 2);
    return;
  }

  //place a rupee when mouse left is pressed
  if (mousePressed && mouseButton == LEFT)
  {
    int rupeeID = getCellPosition1D(mouseX, mouseY);
    rupeeGrid[rupeeID] = cellRupee;
    setupObjects();
    setupFizzy();
  }

  //remove a rupee when mouse left is pressed
  if (mousePressed && mouseButton == RIGHT)
  { 
    int rupeeID = getCellPosition1D(mouseX, mouseY);
    rupeeGrid[rupeeID] = cellEmpty;
    setupObjects();
    setupFizzy();
  }

  drawEditorSelectionBox();
  drawPoles();
  drawRupees();
  drawBlackHoles();
  drawFizzy(fizzyX, fizzyY, fizzyRotation);
  drawRupeeCount(rupeeCount);

  //draw show help text
  fill(255);
  textSize(12);
  textAlign(CENTER, BOTTOM);
  text("Press H to show help text", width / 2, height - 10);
}

public void updateWin()
{
  drawRupees();
  drawFizzy(fizzyX, fizzyY, fizzyRotation);

  //draw game win text
  fill(255, 0, 0);
  textSize(24);
  textAlign(CENTER, CENTER);
  text("Fizzy found her rupees!\nPress R to restart\nPress E to enter the editor", width/2, height/2);
}

public void updateNoRupees()
{
  drawFizzy(fizzyX, fizzyY, fizzyRotation);

  //draw game no rupees text
  fill(255, 0, 0);
  textSize(24);
  textAlign(CENTER, CENTER);
  text("Fizzy has no rupees to find! :( \nNice work trying to break the game ;)\nPress E to enter the editor", width/2, height/2);
}

public void updateLost()
{
  //draw black hole as background
  drawBlackHole(width / 2, height / 2, width / 2);

  //draw game lost text
  fill(255, 0, 0);
  textSize(24);
  textAlign(CENTER, CENTER);
  text("Oh no!\nFizzy is lost in space!\nPress R to restart\nPress E to enter the editor", width/2, height/2);
}

//I would have loved to put this code into each update function, but it's impossible to have
//keypress down and keypress up events without tracking them yourself. Such an input manager
//is beyond what I would like to implement. Takes a lot of lines.
public void keyPressed()
{
  if (gameState == gameStateIntro)
  {
    //start game when a movement key is pressed
    if (key == ' ')
      statePlaying();

    if (key == 'e')
      stateEditor();

    return;
  }

  //change fizzy's direction when a movement key is pressed
  if (gameState == gameStatePlaying)
  {
    if (key == 'w'|| key == 'a' || key == 's' || key == 'd')
      gameStarted = true;

    if (key == 'w')
      fizzyRotation = fizzyRotationUp;
    if (key == 'a')
      fizzyRotation = fizzyRotationLeft;
    if (key == 's')
      fizzyRotation = fizzyRotationDown;
    if (key == 'd')
      fizzyRotation = fizzyRotationRight;

    if (key == 'e')
      stateEditor();

    return;
  }

  if (gameState == gameStateEditor)
  {
    //enable/disable showing help text
    if (key == 'h')
      editorShowHelp = !editorShowHelp;  

    //with the help screen open, there are no other keys to press than H
    if (editorShowHelp == true)
      return;

    //reset the rupee grid to default heart shape when r is pressed
    if (key == 'r')
    {
      setRupeeGridToDefault();
      setupObjects();
      setupFizzy();
    }

    //generate a random grid and play straight away!
    if (key == 'q')
    {
      fillGridRandomly();
      statePlaying();
    }

    //start playing with this grid
    if (key == 'e')
      statePlaying();

    //place fizzy where the mouse is
    if (key == 'f')
    {
      //remove any and all old fizzies
      for (int i = 0; i < cellsHigh * cellsWide; i++)
        if (rupeeGrid[i] == cellFizzy)
          rupeeGrid[i] = cellEmpty;

      //place new fizzy and update editor
      rupeeGrid[getCellPosition1D(mouseX, mouseY)] = cellFizzy;
      setupObjects();
      setupFizzy();
    }

    //place black hole where the mouse is
    if (key == 'b')
    {
      //don't do anything if fizzy is here
      int cellID = getCellPosition1D(mouseX, mouseY);
      if (rupeeGrid[cellID] == cellFizzy)
        return;

      //place black hole
      rupeeGrid[cellID] = cellBlackHole;
      setupObjects();
      setupFizzy();
    }

    return;
  }

  if (gameState == gameStateWon)
  {
    if (key == 'r')
      statePlaying();
    if (key == 'e')
      stateEditor();
    return;
  }

  if (gameState == gameStateNoRupees)
  {
    if (key == 'e')
      stateEditor();
    return;
  }

  if (gameState == gameStateLost)
  {
    if (key == 'r')
      statePlaying();
    if (key == 'e')
      stateEditor();
    return;
  }
}


public void setupFizzy()
{
  //face downwards initally
  fizzyRotation = fizzyRotationDown;

  //find where fizzy is on the grid and move her to that location
  for (int i = 0; i < cellCount; i++)
  {
    if (rupeeGrid[i] == cellFizzy)
    {
      fizzyX = getCellXCenter(i);
      fizzyY = getCellYCenter(i);
      return;
    }
  }

  //if fizzy was not found on the grid (likely an error), place her top centre
  fizzyX = width / 2;
  fizzyY = halfCellSize;
}

public void drawFizzy(int x, int y, int rot)
{
  //simply draw fizzy based on her direction
  if (rot == fizzyRotationUp)
  {
    ellipseMode(CENTER);
    noStroke();
    fill(255, 0, 0);
    ellipse(x, y, fizzySize, fizzySize);
    fill(0);
    ellipse(x, y - fizzySize / 7, fizzySize / 1.75f, fizzySize / 1.75f);
    fill(255);
    ellipse(x - fizzySize / 8, y - fizzySize / 4, fizzySize / 5, fizzySize / 3);
    ellipse(x + fizzySize / 8, y - fizzySize / 4, fizzySize / 5, fizzySize / 3);
    fill(255, 0, 0);
    rectMode(CENTER);
    rect(x, y + fizzySize / 5, fizzySize / 10, fizzySize * 1.3f);
    return;
  } else if (rot == fizzyRotationLeft)
  {
    ellipseMode(CENTER);
    noStroke();
    fill(255, 0, 0);
    ellipse(x, y, fizzySize, fizzySize);
    fill(0);
    ellipse(x - fizzySize / 7, y, fizzySize / 1.75f, fizzySize / 1.75f);
    fill(255);
    ellipse(x - fizzySize / 4, y - fizzySize / 8, fizzySize / 3, fizzySize / 5);
    ellipse(x - fizzySize / 4, y + fizzySize / 8, fizzySize / 3, fizzySize / 5);
    fill(255, 0, 0);
    rectMode(CENTER);
    rect(x + fizzySize / 5, y, fizzySize * 1.3f, fizzySize / 10);
    return;
  } else if (rot == fizzyRotationRight)
  {
    ellipseMode(CENTER);
    noStroke();
    fill(255, 0, 0);
    ellipse(x, y, fizzySize, fizzySize);
    fill(0);
    ellipse(x + fizzySize / 7, y, fizzySize / 1.75f, fizzySize / 1.75f);
    fill(255);
    ellipse(x + fizzySize / 4, y - fizzySize / 8, fizzySize / 3, fizzySize / 5);
    ellipse(x + fizzySize / 4, y + fizzySize / 8, fizzySize / 3, fizzySize / 5);
    fill(255, 0, 0);
    rectMode(CENTER);
    rect(x - fizzySize / 5, y, fizzySize * 1.3f, fizzySize / 10);
    return;
  }

  //assume fizzy is looking down if rot is wrong or not set
  ellipseMode(CENTER);
  noStroke();
  fill(255, 0, 0);
  ellipse(x, y, fizzySize, fizzySize);
  fill(0);
  ellipse(x, y + fizzySize / 7, fizzySize / 1.75f, fizzySize / 1.75f);
  fill(255);
  ellipse(x - fizzySize / 8, y + fizzySize / 4, fizzySize / 5, fizzySize / 3);
  ellipse(x + fizzySize / 8, y + fizzySize / 4, fizzySize / 5, fizzySize / 3);
  fill(255, 0, 0);
  rectMode(CENTER);
  rect(x, y - fizzySize / 5, fizzySize / 10, fizzySize * 1.3f);
}

public void setupObjects()
{
  //since we have a grid that may be different each time, we must build our
  //rupee arrays from the rupee grid

  rupeesLocated = 0;
  rupeeCount = 0;

  //find number of rupees in grid
  for (int i = 0; i < cellCount; i++)
  {
    //set any found rupees to unfound
    if (rupeeGrid[i] == cellRupeeFound)
      rupeeGrid[i] = cellRupee;

    if (rupeeGrid[i] == cellRupee)
      rupeeCount++;
  }
}

public void drawRupees()
{
  for (int i = 0; i < cellCount; i++)
  {
    //draw any found rupees
    if (rupeeGrid[i] == cellRupeeFound)
      drawRupee(getCellXCenter(i), getCellYCenter(i), rupeeSize);

    //draw hidden rupees if not playing (most likely editor)
    if (rupeeGrid[i] == cellRupee && gameState != gameStatePlaying)
      drawRupee(getCellXCenter(i), getCellYCenter(i), rupeeSize);

    //draw hidden rupees during playing IF debug mode is enabled    
    if (gameState == gameStatePlaying && debug == true)
      drawRupee(getCellXCenter(i), getCellYCenter(i), rupeeSize, rupeeDebugColour);
  }
}

public void drawRupee(int x, int y, float size)
{
  drawRupee(x, y, size, rupeeColour);
}
public void drawRupee(int x, int y, float size, int colour)
{
  drawRupee(x, y, size, colour, 0xffC8C8C8);
}
public void drawRupee(int x, int y, float size, int colour, int strokeColour)
{
  strokeWeight(1);
  stroke(strokeColour);
  fill(colour);

  //construct and advance an incremental timer based on how many milliseconds
  //have passed since start of game
  float rotationTimer = millis() % rupeeRotationTimeMS;

  //turn the timer into a percentage-based figure
  float animationPercent = rotationTimer / rupeeRotationTimeMS;

  //draw a square (a rupee) rotating on the Y axis
  quad(
    x, y - size / 2, 
    x - size / 2 + animationPercent * size, y, 
    x, y + size / 2, 
    x + size / 2 - animationPercent * size, y
    );
}

public void drawRupeeCount(int count)
{
  drawRupee(rupeeIconX, rupeeIconY, rupeeIconSize, rupeeColour, 0xff646464);

  //draw number of found rupees next to rotating rupee in top left corner
  fill(rupeeColour);
  textAlign(LEFT, CENTER);
  textSize(16);
  text("x" + count, rupeeIconX + rupeeIconSize / 2, rupeeIconY);
}

public void drawPoles()
{
  noStroke();
  fill(255, 255, 0);

  for (int i = 1; i < 9; i++)
  {
    for (int j = 1; j < 9; j++)
    {
      ellipse(i * cellSize, j * cellSize, 10, 10);
    }
  }
}

public void drawBlackHoles()
{
  for (int i = 0; i < cellCount; i++)
  {
    if (rupeeGrid[i] == cellBlackHole)
      drawBlackHole(getCellXCenter(i), getCellYCenter(i));
  }
}

public void drawBlackHole(int x, int y)
{
  drawBlackHole(x, y, blackHoleRadius);
}
public void drawBlackHole(int x, int y, int size)
{
  fill(0xff781875);
  noStroke();

  //construct and advance an incremental timer based on how many milliseconds
  //have passed since start of game
  float rotationTimer = millis() % 1250;

  //turn the timer into a percentage-based figure
  float animationPercent = rotationTimer / 1250;

  //finally, we'll expand the animation percentage into a rotation amount for 0-360
  float rotationAmount = animationPercent * 360.0f;

  ellipseMode(RADIUS);
  ellipse(x, y, size, size);

  stroke(0xffa427a0);
  strokeWeight(4);
  noFill();

  //now that we draw a swirling motion, here is where it gets fun!!
  //https://stackoverflow.com/questions/839899/how-do-i-calculate-a-point-on-a-circle-s-circumference

  float arcX = 0;
  float arcY = 0;

  arcX = size * cos(radians(rotationAmount)) + x;
  arcY = size * sin(radians(rotationAmount)) + y;
  arc(arcX, arcY, size, size, HALF_PI + QUARTER_PI + radians(rotationAmount), PI + radians(rotationAmount));

  arcX = size * cos(radians(rotationAmount + 90)) + x;
  arcY = size * sin(radians(rotationAmount + 90)) + y;
  arc(arcX, arcY, size, size, PI + QUARTER_PI + radians(rotationAmount), PI + HALF_PI + radians(rotationAmount));


  arcX = size * cos(radians(rotationAmount + 180)) + x;
  arcY = size * sin(radians(rotationAmount + 180)) + y;
  arc(arcX, arcY, size, size, PI + HALF_PI + QUARTER_PI + radians(rotationAmount), PI + PI + radians(rotationAmount));

  arcX = size * cos(radians(rotationAmount + 270)) + x;
  arcY = size * sin(radians(rotationAmount + 270)) + y;
  arc(arcX, arcY, size, size, QUARTER_PI + radians(rotationAmount), HALF_PI + radians(rotationAmount));
}

public void drawEditorSelectionBox()
{
  //convert mouse cordinates into usable cordinates
  int cellID = getCellPosition1D(mouseX, mouseY);
  int rectX = getCellXPosition(cellID);
  int rectY = getCellYPosition(cellID);

  fill(rupeeColour, 30);
  stroke(rupeeColour);
  strokeWeight(5);
  rectMode(CORNER);
  rect(rectX, rectY, cellSize, cellSize);
}

public void collisionFizzyObjectCheck()
{
  if (gameState != gameStatePlaying)
    return;

  for (int i = 0; i < cellCount; i++)
  {
    //nothing here in this cell
    if (rupeeGrid[i] == cellEmpty)
      continue;

    //fizzy has no interaction with found rupees. skip to next object
    if (rupeeGrid[i] == cellRupeeFound)
      continue;

    //there must be something here! Is fizzy too far to touch it? If so, continue to next object
    if (dist(fizzyX, fizzyY, getCellXCenter(i), getCellYCenter(i)) > fizzySize - fizzySize / 4)
      continue;

    //Fizzy has touched this object. Lets filter and work out what it is!

    //Good! Fizzy has found a rupee! Collect rupee, speed up and skip any further collision checking
    if (rupeeGrid[i] == cellRupee)
    {
      rupeesLocated += 1;
      rupeeGrid[i] = cellRupeeFound;

      //make fizzy speed up as she finds more rupees. The rate will be percentage based of the entire number of rupees
      //so speed is always the same at start and end of the game, regardless of how many rupees collected
      fizzySpeed = fizzyDefaultSpeed + (float)rupeesLocated / (float)rupeeCount * (fizzyMaxSpeed - fizzyDefaultSpeed); 
      break;
    }

    //oh no! Rupee has ran into a black hole. She is now lost! Skip any further collision checking
    if (rupeeGrid[i] == cellBlackHole)
    {
      stateLost();
      break;
    }
  }

  //Win game when all rupees are found
  if (rupeesLocated == rupeeCount)
    stateWin();
}

public void collisionFizzyWallCheck()
{
  //bounce against wall if contacting
  if (fizzyX - fizzyHalfSize <= 0)
    fizzyRotation = fizzyRotationRight;
  if (fizzyX + fizzyHalfSize >= width)
    fizzyRotation = fizzyRotationLeft;
  if (fizzyY - fizzyHalfSize <= 0)
    fizzyRotation = fizzyRotationDown;
  if (fizzyY + fizzyHalfSize >= height)
    fizzyRotation = fizzyRotationUp;
}

public void setRupeeGridToDefault()
{
  if (rupeeGrid == null)
    rupeeGrid = new int[cellCount]; 
  arrayCopy(defaultRupeeGrid, rupeeGrid);
}

//returns the X position of a cell
public int getCellXPosition(int cellID)
{
  return cellID % cellsWide * cellSize;
}

//returns the Y position of a cell
public int getCellYPosition(int cellID)
{
  return cellID / cellsWide * cellSize;
}

//returns the center X position of a cell
public int getCellXCenter(int cellID)
{
  return getCellXPosition(cellID) + halfCellSize;
}

//returns the center Y position of a cell
public int getCellYCenter(int cellID)
{
  return getCellYPosition(cellID) + halfCellSize;
}

//Converts position X and Y into a cell slot for a 1-dimensional array
public int getCellPosition1D(int x, int y)
{
  //first, find out how many cell rows there are to get to the position.
  //Next, multiply that by number of cells in a row. This will get the number to be the correct row.
  //Finally, add the number of columns in the row in order to get the final ID 
  return (y / cellSize) * cellsWide + (x / cellSize);
}

//fill the grid with random rupess. Afterall, the rupees are missing!
//I so wish I could do some 10k to 1m times random unit testing on this function!!
public void fillGridRandomly()
{
  for (int i = 0; i < cellCount; i++)
  {
    //clear any existing items
    rupeeGrid[i] = cellEmpty;
  }


  //make sure that there is always a chance to spawn a rupee at each cell
  float rupeeSpawnChance = random(rupeeMinSpawnChance, rupeeMaxSpawnChance);
  for (int i = 0; i < cellCount; i++)
  {
    //spawn a rupee if spawn chance is inside the random number
    if (random(0, 100) < rupeeSpawnChance)
      rupeeGrid[i] = cellRupee;
  }


  //randomly spawn black holes
  int blackHoleCount = (int)random(minBlackHoleRandomCount, maxBlackHoleRandomCount + 1);
  int blackHolesPlaced = 0;
  int[] blackHoles = new int[blackHoleCount];

  for (int i = 0; i < blackHoleCount; i++)
  {
    //we will provide 20 chances for each black hole to spawn spread far enough apart
    for (int j = 0; j < 20; j++)
    {
      int blackHoleCellID = (int)random(0, cellCount);

      //the first one has nothing to check against, so just place and continue
      if (i == 0)
      {
        rupeeGrid[blackHoleCellID] = cellBlackHole;
        blackHoles[blackHolesPlaced] = blackHoleCellID;
        blackHolesPlaced++;
        break;
      }

      boolean positionAcceptable = true;

      //we want to check that there is at least two spare cells between each black hole for fizzy to get through
      //otherwise, we can easily get a rupee stuck between two black holes and make the game unwinnable
      for (int k = 0; k < blackHolesPlaced; k++)
      {
        //we're too close to another black hole. we'll have to find another position
        if (dist(getCellXCenter(blackHoleCellID), getCellYCenter(blackHoleCellID), getCellXCenter(blackHoles[k]), getCellYCenter(blackHoles[k])) < cellSize * 2)
        {
          positionAcceptable = false;
          break;
        }
      }

      //this is a bad position. continue
      if (positionAcceptable == false)
        continue;

      //place this black hole and don't attempt to reposition it. move onto the next black hole to place
      rupeeGrid[blackHoleCellID] = cellBlackHole;
      blackHoles[blackHolesPlaced] = blackHoleCellID;
      blackHolesPlaced++;
      break;
    }
  }

  //although unlikely, it's possible to spawn a map with no rupees.
  //make sure that there is at least 1 rupee!!
  boolean rupeesExist = false;
  for (int i = 0; i < cellCount; i++)
  {
    if (rupeeGrid[i] != cellRupee)
      continue;

    rupeesExist = true;
    break;
  }

  if (rupeesExist == false)
  {
    //if we get here, then by pure (bad?) luck we have not spawned a single rupee
    //spawn several just in case positioning fizzy overwrites one

    //Since we are guessing a few ranges based on the usual 9x9 grid, we'll ensure that we cannot go  out of bounds using min
    //we use random ranges to ensure that by incredible luck, we don't somehow spawn all the rupees in the exact same cell that fizzy spawn
    //we'll also use min(cellcount) to ensure that 
    rupeeGrid[min((int)random(0, 20), cellCount)] = cellRupee;
    rupeeGrid[min((int)random(20, 40), cellCount)] = cellRupee;
    rupeeGrid[min((int)random(40, 60), cellCount)] = cellRupee;
    rupeeGrid[min((int)random(60, 80), cellCount)] = cellRupee;
    rupeeGrid[min((int)random(80, 100), cellCount)] = cellRupee;

    //let's throw a few more around randomly
    rupeeGrid[(int)random(cellCount)] = cellRupee;
    rupeeGrid[(int)random(cellCount)] = cellRupee;
    rupeeGrid[(int)random(cellCount)] = cellRupee;
  }

  //place fizzy at a random position along the edge
  //we'll use min to ensure that array access does not go out of bounds
  //I believe these are correct, but without extensive automated testing....
  switch((int)random(0, 4))
  {
  case 0: 
    {
      //north wall
      rupeeGrid[min(cellCount, (int)random(cellsWide))] = cellFizzy;
      break;
    }

  case 1: 
    {
      //east wall. Max gets around an error. Math is hard!
      rupeeGrid[min(cellCount, max(cellsWide * (int)random(cellsHigh) - 1, 0))] = cellFizzy;
    }

  case 2: 
    {
      //south wall
      rupeeGrid[min(cellCount, (int)random(cellCount - cellsWide, cellCount))] = cellFizzy;
      break;
    }

  case 3: 
    {
      //west wall
      rupeeGrid[min(cellCount, cellsWide * (int)random(cellsHigh - cellsWide))] = cellFizzy;
      break;
    }
  }
}
  public void settings() {  size(810, 810); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "ass2" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
