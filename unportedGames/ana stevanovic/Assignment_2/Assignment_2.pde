int manX = 60;
int manY = 240;
int step = 65;
int doorCounter = 0;
boolean doorOpen = false;
int hammerX = 255;
int hammerY = 0;
int hammerDrop = 0;
int slot0X = 60;
//int slot1X = 125;
//int slot2X = 190;
//int slot3X = 255;
//int slot4X = 320;
//int slot5X = 385;
//int slot6X = 450;
int pointBlack=0;
int pointRed = 0;
//color floorPink = color(165, 154, 159);
//color floorPurple = color(174, 138, 136);
//int floorX = 100;
//int floorY = 280;
//color sky = color(158, 217, 233, 180);
//color ground = color(156, 206, 111, 180);
//int sunX = 10;
//int sunY = 50;*/
//color balconyBrown = color(154, 110, 46);
//color balconyPink = color(198, 169, 164);
//int balconyX = 10;
//int balconyY = 100;
//int leftDoorX = 50;
//int leftDoorY = 200;
//int knobDiameter = 5;
//int cottageX = 400;
//int cottageY = 200;
//color roof = color(153, 102, 51);
//color cottageWall=color(167, 110, 10);
//color roofLine = color(102, 51, 0);
//color door = color(180);
//color openDoor = color (100);
//color grass = color(51,153,51);
//color clouds = color(230,230,230,248);
float cloud1X = 132;
float cloud1Y = 86;
float cloud2X = 344;
float cloud2Y = 69;
float cloud3X = 466;
float cloud3Y = 100;



void setup() {
  size(512, 348);
  doorCounter = (int)random(180, 480);
  hammerDrop = (int)random(60, 60);
  hammerX = slot0X + step * int(random(1, 6));
}

void draw() {
  background(158, 170, 173);
  drawBackDrop();
  drawClouds(cloud1X, cloud1Y);
  drawClouds(cloud2X, cloud2Y);
  drawClouds(cloud3X, cloud3Y);
  //drawLeftDoor();
  //drawFloor();
  //drawCottage();
  drawGrass(152, 258);
  drawGrass(80, 321);
  drawGrass(220, 319);
  drawGrass(494, 278);
  drawGrass(336, 261);
  drawGrass(424, 308);
  doorSystem();
  drawMan();
  hammer();
  hammerCollide();
  hammerPass();
  manHome();
  pointSystem(pointBlack/6, pointRed);
}

void keyPressed() {
  makeManMove();
}