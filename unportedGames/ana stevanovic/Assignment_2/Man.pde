void drawMan() {
  //head
  stroke(0);
  fill(0);
  ellipse(manX, manY, 20, 20);

  //mouth interactive with background
  if (manX >= 430) {
    fill(100);
    noStroke();
    ellipse(manX + 10, manY + 6, 13, 5);
  } else {
    fill(155, 196, 126);
    noStroke();
    ellipse(manX + 10, manY + 6, 13, 5);
  }

  //nose
  fill(0);
  ellipse(manX + 10, manY - 2, 9, 5);

  //body
  triangle(manX - 2, manY + 10, manX - 10, manY + 24, manX - 2, manY + 28);

  //legs
  stroke(0);
  line(manX - 9, manY + 24, manX - 9, manY + 34);
  line(manX - 5, manY + 24, manX - 5, manY + 34);

  //arms
  line(manX - 2, manY + 16, manX + 3, manY + 16);
  line(manX - 2, manY + 18, manX + 3, manY + 18);
}
