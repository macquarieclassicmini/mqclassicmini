void doorSystem() {
  int knobDiameter = 5;
  color openDoor = color (100);
  color door = color(180);
  int cottageX = 400;
  int cottageY = 200;
  doorCounter = doorCounter - 1; //code inspiration from
  if (doorCounter <= 0) { //Dominic Verity on discussion page
    doorOpen = !doorOpen;
    doorCounter = (int)random(180, 480);
  }
  strokeWeight(1);
  stroke(0);
  if (doorOpen == true) {
    //open door
    fill(door);
    quad(cottageX+75, cottageY+17, cottageX+102, cottageY+5, cottageX+102, cottageY+88, cottageX+75, cottageY+76);
    fill(0);
    quad(cottageX+80, cottageY+27, cottageX+95, cottageY+20, cottageX+95, cottageY+42, cottageX+80, cottageY+40);
    ellipse(cottageX+97, cottageY+53, knobDiameter-1, knobDiameter+1);
  } else { 
    //cottage window and doorknob
    fill(door);
    rect(cottageX+35, cottageY+17, cottageX+75, cottageY+76);
    fill(0);
    rect(cottageX+47, cottageY+26, cottageX+63, cottageY+42);
    ellipse(cottageX+40, cottageY+47, knobDiameter, knobDiameter);
  }
  //doorframe colour
  if (doorOpen == true) {
    fill(openDoor);
    rect(cottageX+35, cottageY+17, cottageX+75, cottageY+76);
  }
}