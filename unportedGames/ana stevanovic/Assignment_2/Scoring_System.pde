void pointSystem(int b, int r) {
  //single black
  for (int i=0; i<pointBlack%6; i++) {
    circleColour(80*i+50, 80*(b)+80*(r)+50, 0);
  }
  //black row
  for (int w=0; w<6; w++) {
    for (int v=r; v<(b+r); v++) {
      circleColour(80*w+50, 80*v+50, 0);
    }
  }
  //red row
  for (int w=0; w<=5; w++) {
    for (int v = 1; v<=r; v++) {
      circleColour(80*w+50, 80*v-30, 255);
    }
  }
  //reset
  if (b+r >=4) {
    pointBlack = 0;
    pointRed = 0;
    hammerY = 0;
    manX = slot0X;
    cloud1X = 132;
    cloud1Y = 86;
    cloud2X = 344;
    cloud2Y = 69;
    cloud3X = 466;
    cloud3Y = 100;
  }
}
