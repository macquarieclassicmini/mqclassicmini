void circleColour (int t, int h, int red) {
  int circleSize=75;
  for (int k=0; k<255; k+=17) {
    fill(red, 0, 0, k);
    noStroke();
    ellipse(t, h, circleSize, circleSize);
    circleSize=circleSize-5;
  }
}