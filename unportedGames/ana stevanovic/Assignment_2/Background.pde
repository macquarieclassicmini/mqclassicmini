void drawBackDrop() {
  color sky = color(158, 217, 233, 180);
  color ground = color(156, 206, 111, 180);
  color balconyBrown = color(154, 110, 46);
  color balconyPink = color(198, 169, 164);
  int sunX = 10;
  int sunY = 50;
  int balconyX = 10;
  int balconyY = 100;
  int cottageX = 400;
  int cottageY = 200;
  color roof = color(153, 102, 51);
  color cottageWall=color(167, 110, 10);
  color roofLine = color(102, 51, 0);
  color floorPink = color(165, 154, 159);
  color floorPurple = color(174, 138, 136);
  int floorX = 100;
  int floorY = 280;
  int knobDiameter = 5;
  color door = color(180);
  int leftDoorX = 50;
  int leftDoorY = 200;

  // sky, ground
  fill(sky);
  noStroke();
  rectMode(CORNERS);
  rect(0, 0, width, height-110);
  fill(ground);
  rect(0, height-110, width, height);

  //sun
  fill(230, 200, 0);
  noStroke();
  ellipse(sunX-10, sunY-50, 150, 150);
  triangle(sunX, sunY+40, sunX+14, sunY+37, sunX+11, sunY+57);
  triangle(sunX+29, sunY+31, sunX+42, sunY+23, sunX+45, sunY+45);
  triangle(sunX+51, sunY+18, sunX+62, sunY+8, sunX+72, sunY+31);
  triangle(sunX+70, sunY-1, sunX+77, sunY-12, sunX+92, sunY+8);
  triangle(sunX+81, sunY-21, sunX+82, sunY-35, sunX+97, sunY-25);

  //balcony
  fill(balconyBrown);
  rect(balconyX-10, balconyY+17, balconyX, balconyY+110);
  rect(balconyX, balconyY+17, balconyX+33, balconyY+31);
  rect(balconyX, balconyY+43, balconyX+33, balconyY+70);
  rect(balconyX, balconyY+78, balconyX+33, balconyY+110);

  fill(balconyPink);
  rect(balconyX, balconyY+25, balconyX+33, balconyY+28);
  rect(balconyX, balconyY+61, balconyX+33, balconyY+65);
  rect(balconyX, balconyY+100, balconyX+33, balconyY+104);

  //roof and walls
  strokeWeight(1);
  stroke(0);
  fill(cottageWall);
  rect(cottageX+30, cottageY+12, cottageX+126, cottageY+76);
  stroke(roofLine);
  fill(roof);
  triangle(cottageX+30, cottageY+12, cottageX+78, cottageY-8, cottageX+126, cottageY+12);

  stroke(0);
  strokeWeight(8);
  line(cottageX+24, cottageY+10, cottageX+78, cottageY-8);
  line(cottageX+78, cottageY-8, cottageX+112, cottageY+4);

  //floor
  strokeWeight(1.5);
  stroke(floorPurple);
  line(floorX-54, floorY-4, floorX+6, floorY-7);
  line(floorX+13, floorY - 1, floorX+74, floorY + 6);
  line(floorX+85, floorY + 3, floorX+118, floorY-1);
  line(floorX+138, floorY+3, floorX+197, floorY+3);
  line(floorX+209, floorY+4, floorX+266, floorY+9);
  line(floorX+269, floorY+5, floorX+330, floorY-4);

  stroke(floorPink);
  line(floorX-55, floorY, floorX+1, floorY-3);
  line(floorX+33, floorY+4, floorX+73, floorY+11);
  line(floorX+98, floorY+5, floorX+130, floorY+1);
  line(floorX+141, floorY+7, floorX+193, floorY+4);
  line(floorX+205, floorY+9, floorX+256, floorY+14);
  line(floorX+274, floorY+14, floorX+330, floorY-4);

  //left door
  strokeWeight(1);
  stroke(0);
  fill(door);
  rect(leftDoorX-50, leftDoorY+10, leftDoorX -4, leftDoorY+76);
  //window
  fill(0);
  rect(leftDoorX-35, leftDoorY+22, leftDoorX-19, leftDoorY+38);
  //doorknob
  ellipse(leftDoorX-9, leftDoorY + 46, knobDiameter, knobDiameter);
}