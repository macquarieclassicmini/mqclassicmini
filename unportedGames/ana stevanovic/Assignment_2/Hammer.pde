void hammer() {
  strokeWeight(5);
  line(hammerX, hammerY, hammerX + 20, hammerY - 20);
  strokeWeight(6);
  line(hammerX + 8, hammerY + 6, hammerX - 6, hammerY - 6);

  hammerDrop = hammerDrop - 1; //similar to door open/close code from Dominic Verity
  if (hammerDrop <= 0) {
    hammerY = hammerY + 60;
    hammerDrop = 60;
  }
}