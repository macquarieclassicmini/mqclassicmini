void drawClouds(float x, float y) {
  color clouds = color(230, 230, 230, 248);
  fill(clouds);
  noStroke();
  ellipse(x - 23, y - 20, 40, 30);
  ellipse(x - 44, y - 2, 50, 30);
  ellipse(x, y, 54, 28);
  //cloud movement
  cloud1X = (cloud1X + 0.4) % (width + 70); //code discovered in reference tab on
  cloud2X = (cloud2X + 0.4) % (width + 75); // processing.org
  cloud3X = (cloud3X + 0.4) % (width + 43);
}