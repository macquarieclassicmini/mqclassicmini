void drawGrass(int x, int y) {
  color grass = color(51, 153, 51);
  fill(grass);
  noStroke();
  triangle(x-7, y, x-3, y, x-5, y-8);
  triangle(x-4, y, x+2, y, x, y-19);
  triangle(x+1, y, x+5, y, x+3, y-8);
}