- COMP115 Assignment 2
- "Hardhat 2"
- Author: Brendan McSweeney
- Date: 27/05/2018
- Original Game, Concept and Character �1981 Nintendo

========
Controls
========

Left Arrow Key: 	Move Left
Right Arrow Key: 	Move Right
Up/Down Arrow Key: 	Change Menu Selection
Enter/Return Key:	Pause, Confirm Menu Selection, Restart after Game Over

==========
Game Modes
==========

Common rules:
	Use the arrow keys to move MrGW left and right. Reach the door without getting hit by any of the falling tools. Being under a tool as it disappears counts as a hit. You will not be able to enter the door if it is not open, so wait a moment, staying aware of the tools.

Game A:
	Reaching the door earns one point. Getting hit by a tool loses a life. Lose three lives and it's Game Over. Try to get as many points as possible before losing all your lives. *This is effective the classic gameplay from the original game and Assignment 1.
Game B:
	Each tool that reaches the ground without hitting you causes 1 circle appear on the screen. Entering the door makes 3 circles appear. Getting hit by a tool causes a full row of red circles to appear, each giving a 1 second penalty to your time. Try to fill up the screen in as little time as possible. *This is the new gameplay as outlined for Assignment 2.

=====================
Customisable settings
=====================

To demonstrate the thorough elimination of magic numbers, a collection of variables has been initialised at the start of the code. Their variables can be changed freely to alter the experience of the game and overall difficulty. Their functionality is described by their respective comments in the code.