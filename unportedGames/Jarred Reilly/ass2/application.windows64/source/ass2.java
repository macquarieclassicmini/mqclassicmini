import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class ass2 extends PApplet {

/******************
      SETUP
*******************/
public void setup(){
  
  frameRate(30);
}


/******************
  GLOBAL VARIABLES
*******************/
int angle = 0;
String lastRotation = "right";
int fizzySpeed = 1;

int xPos = constrain(45, 0, width);
int yPos = constrain(90, 0, height);

boolean rotate = false;
boolean stillRotating = false;
boolean move = true;

int time = 14520;
int flash = 0;
int fizzyFlash = 0;

String gameState = "menu";
int totalScore = 0;
int highScore = 0;
boolean result = false;
boolean deathByBlackHole = false;
boolean scoreCalculated = false;

int[][] rupees = { {225, 90, 0, 0}, {315, 90, 0, 0}, {495, 90, 0, 0}, {585, 90, 0, 0}, {180, 135, 1, 0}, {360, 135, 1, 0}, {450, 135, 1, 0}, {630, 135, 1, 0}, {135, 180, 0, 0}, {405, 180, 0, 0}, {675, 180, 0, 0}, {90, 225, 1, 0}, {720, 225, 1, 0}, {90, 315, 1, 0}, {720, 315, 1, 0}, {90, 405, 1, 0}, {720, 405, 1, 0}, {135, 450, 0, 0}, {675, 450, 0, 0}, {180, 495, 1, 0}, {630, 495, 1, 0}, {225, 540, 0, 0}, {585, 540, 0, 0}, {270, 585, 1, 0}, {540, 585, 1, 0}, {315, 630, 0, 0}, {495, 630, 0, 0}, {360, 675, 1, 0}, {450, 675, 1, 0}, {405, 720, 0, 0}};
int[][] blackHoles = { {225, 225}, {585, 405}, {135, 585}, {675, 675} };

/***************
      DRAW
****************/
public void draw(){
  background(0);
  if (gameState.equals("menu")) {
    menu();
  }
  if (gameState.equals("instructions")) {
    instructions();
  }
  if (gameState.equals("play")) {
    detectPoles();
    detectPosition();
    detectTime();
    detectWalls();
    drawRupees();
    drawPoles();
    drawBlackHoles();
    drawFizzy(xPos, yPos, angle);
  }
  if (gameState.equals("score")) {
    detectWalls();
    drawRupees();
    drawPoles();
    drawBlackHoles();
    if (!deathByBlackHole) {
      drawFizzy(xPos, yPos, angle);
    }
    scoreScreen(result);
  }
}


/***************
  MENU SECTION
****************/
public void menu(){
  textAlign(CENTER, CENTER);
  textSize(70);
  fill(78, 150, 241);
  text("KURU KURU", width/2, 100);
  textSize(83);
  text("COUNTRY", width/2, 170);
  textSize(30);
  text("A GAME BY JARRED REILLY", width/2, 700);
  fill(255);
  text("PLAY", width/2, 325);
  text("INSTRUCTIONS", width/2, 425);
  text("HIGH SCORE: " + highScore, width/2, 525);
  noFill();
  stroke(78, 150, 241);
  strokeWeight(5);
  rectMode(CENTER);
  rect(width/2, 148, 500, 180);
  
  // PLAY button
  if (mouseX > width/2 - 100 && mouseX < width/2 + 100 && mouseY > 275 && mouseY < 375) {
    textSize(40);
    fill(0);
    noStroke();
    rect(width/2, 325, 100, 50);
    fill(78, 150, 241);
    text("PLAY", width/2, 325);
    if (mousePressed == true) {
      restart();
      gameState = "play";
    }
  }
  
  // INSTRUCTIONS button
  if (mouseX > width/2 - 100 && mouseX < width/2 + 100 && mouseY > 375 && mouseY < 475) {
    textSize(40);
    fill(0);
    noStroke();
    rect(width/2, 425, 250, 50);
    fill(78, 150, 241);
    text("INSTRUCTIONS", width/2, 425);
    if (mousePressed == true) {
      gameState = "instructions";
    }
  }
}


/**********************
  INSTRUCTIONS SECTION
**********************/
public void instructions() {
  textAlign(CENTER, CENTER);
  textSize(60);
  fill(78, 150, 241);
  text("INSTRUCTIONS", width/2, 50);
  textAlign(LEFT);
  textSize(30);
  fill(255);
  text("1. Grab poles to move around", 50, 150);
  text("2. Hold 'A' to extend your left hand", 50, 200);
  text("3. Hold 'D' to extend your right hand", 50, 250);  
  text("4. Collect the hidden rupees between the poles", 50, 300);
  text("5. Avoid the deadly black holes!", 50, 350);
  text("6. Collect all the rupees before time runs out!", 50, 400);
  textAlign(CENTER, CENTER);
  textSize(40);
  fill(255);
  text("MENU", width/2, 700);
  
  // A Rupee
  int rupeeX = 160;
  int rupeeY = 500;
  rectMode(CENTER);
  noStroke();
  fill(255, 188, 71);
  rect(rupeeX, rupeeY, 20, 40);
  triangle(rupeeX - 10, rupeeY - 20, rupeeX, rupeeY - 30, rupeeX + 10, rupeeY - 20);
  triangle(rupeeX - 10, rupeeY + 20, rupeeX, rupeeY + 30, rupeeX + 10, rupeeY + 20);
  fill(255, 142, 39);
  rect(rupeeX, rupeeY, 13, 30);
  triangle(rupeeX - 7, rupeeY - 15, rupeeX, rupeeY - 22, rupeeX + 7, rupeeY - 15);
  triangle(rupeeX -7, rupeeY + 15, rupeeX, rupeeY + 22, rupeeX + 7, rupeeY + 15);
  fill(255);
  textAlign(CENTER, CENTER);
  textSize(25);
  text("Rupee", rupeeX, rupeeY + 75);
          
  
  // A Black Hole
  fill(255);
  int blackHoleX = 320;
  int blackHoleY = 500;
  if (flash <= 10) {
    ellipse(blackHoleX, blackHoleY, 50, 50);
  } else if (flash > 10 && flash <= 20) {
    ellipse(blackHoleX, blackHoleY, 51, 51);
  } else if (flash > 20 && flash <= 30) {
    ellipse(blackHoleX, blackHoleY, 52, 52);
  } else if (flash > 30 && flash <= 40) {
    ellipse(blackHoleX, blackHoleY, 51, 51);
  } else {
    flash = 0;
  }
  fill(0);
  ellipse(blackHoleX, blackHoleY, 45, 45);
  flash++;
  fill(255);
  text("Black hole", blackHoleX, blackHoleY + 75);
  
  // A Fizzy
  int fizzyX = 480;
  int fizzyY = 500;
  // Draw Fizzy's legs
  if (fizzyFlash < 20) {
    stroke(218, 165, 15);
    strokeWeight(5);
    line(fizzyX - 18, fizzyY + 10, fizzyX - 22, fizzyY + 20);
    stroke(255, 0, 0);
    line(fizzyX - 20, fizzyY, fizzyX - 28, fizzyY - 3);
    noStroke();
  } else {
    stroke(218, 165, 15);
    strokeWeight(5);
    line(fizzyX - 18, fizzyY - 10, fizzyX - 22, fizzyY - 20);
    stroke(255, 0, 0);
    line(fizzyX - 20, fizzyY, fizzyX - 28, fizzyY + 3);
    noStroke();
  }
  fizzyFlash++;
  if (fizzyFlash >= 40) {
    fizzyFlash = 0;
  }
  // Draw Fizzy's body
  fill(255, 0, 0);
  ellipse(fizzyX, fizzyY, 45, 45);
  fill(0);
  ellipse(fizzyX + 4, fizzyY, 20, 30);
  fill(255);
  rect(fizzyX + 8, fizzyY, 11, 25);
  fill(0);
  ellipse(fizzyX + 8, fizzyY - 4, 5, 5);
  ellipse(fizzyX + 8, fizzyY + 4, 5, 5);
  fill(255, 0, 0);
  rect(fizzyX, fizzyY, 30, 5);
  
  fill(255);
  text("Fizzy", fizzyX, fizzyY + 75);
  
  // A Pole
  int poleX = 640;
  int poleY = 500;
  noStroke();
  fill(255);
  ellipse(poleX, poleY, 12, 12);
  fill(180, 10, 0);
  ellipse(poleX, poleY, 7, 7);
  fill(255);
  text("Pole", poleX, poleY + 75);
  
  // MENU button
  if (mouseX > width/2 - 100 && mouseX < width/2 + 100 && mouseY > 650 && mouseY < 750) {
    textSize(50);
    fill(0);
    noStroke();
    rectMode(CENTER);
    rect(width/2, 700, 200, 50);
    fill(78, 150, 241);
    text("MENU", width/2, 700);
    if (mousePressed == true) {
      gameState = "menu";
    }
  }
}


/***************
  SCORE SECTION
****************/
public void scoreScreen(boolean victory) {
  String scoreString;
  String resultString;
  fizzySpeed = 0;
  if (victory) {
    scoreString = "CURRENT SCORE: ";
    resultString = "YOU WIN";
  } else {
    scoreString = "FINAL SCORE: ";
    resultString = "GAME OVER";
  }
  if (!scoreCalculated) {
    totalScore += calculateScore();
    scoreCalculated = true;
  }
  if (totalScore > highScore){
    highScore = totalScore;
  }
  textSize(40);
  textAlign(CENTER, CENTER);
  fill(78, 150, 241);
  text(scoreString + totalScore, width/2, height/2 - 95);
  text(resultString, width/2, height/2 - 5);
  fill(255);
  textSize(30);
  text("PLAY AGAIN", width/2, height/2 + 86);
  text("MENU", width/2, height/2 + 176);
  
  // PLAY AGAIN button
  if (mouseX > width/2 - 100 && mouseX < width/2 + 100 && mouseY > height/2 + 46 && mouseY < height/2 + 126) {
    textSize(35);
    fill(0);
    noStroke();
    rectMode(CENTER);
    rect(width/2, height/2 + 86, 200, 50);
    fill(78, 150, 241);
    text("PLAY AGAIN", width/2, height/2 + 86);
    if (mousePressed == true) {
        if (!victory) {
          totalScore = 0;
        }
      restart();
      gameState = "play";
    }
  }
  
  // MENU button
  if (mouseX > width/2 - 100 && mouseX < width/2 + 100 && mouseY > height/2 + 136 && mouseY < height/2 + 206) {
    textSize(35);
    fill(0);
    noStroke();
    rectMode(CENTER);
    rect(width/2, height/2 + 176, 200, 50);
    fill(78, 150, 241);
    text("MENU", width/2, height/2 + 176);
    if (mousePressed == true) {
        if (!victory) {
          totalScore = 0;
        }
      gameState = "menu";
    }
  }
}

// Calculates final score
public int calculateScore() {
  int levelScore = 0;
  for (int i = 0; i < rupees.length; i++) {
    if (rupees[i][3] == 1) {
      levelScore++;
    }
  }
  return levelScore;
}

// Restart the game
public void restart() {
  result = false;
  time = 14520;
  xPos = 45;
  yPos = 90;
  angle = 0;
  deathByBlackHole = false;
  scoreCalculated = false;
  lastRotation = "right";
  fizzySpeed = 1*5;
  for (int i = 0; i < rupees.length; i++) {
    rupees[i][3] = 0;
  }
  gameState = "play";
}


/***************
  PLAY SECTION
****************/
// Handles drawing rupees and rupee collision with Fizzy
public void drawRupees() {
  int rupeeCounter = 0;
  for (int i = 0; i < rupees.length; i++) {
    int rupeeX = rupees[i][0];
    int rupeeY = rupees[i][1];
    int rupeeRotation = rupees[i][2];
    int rupeeVisible = rupees[i][3];

      // Detect rupee collision when last spin around a pole was to the right
      if (lastRotation.equals("right")){
        if (angle == 0) {
          if (xPos + 5 >= rupeeX && xPos - 5 <= rupeeX && yPos - 90 < rupeeY && yPos > rupeeY) {
            rupees[i][3] = 1;
          }
        } else if (angle == 180){
          if (xPos + 5 >= rupeeX && xPos - 5 <= rupeeX && yPos < rupeeY && yPos + 90 > rupeeY) {
            rupees[i][3] = 1;
          }
        } else if (angle == 90) {
          if (yPos + 5 >= rupeeY && yPos - 5 <= rupeeY && xPos < rupeeX && xPos + 90 > rupeeX) {
            rupees[i][3] = 1;
          }
        } else if (angle == 270) {
          if (yPos + 5 >= rupeeY && yPos - 5 <= rupeeY && xPos > rupeeX && xPos - 90 < rupeeX) {
            rupees[i][3] = 1;
          }
        }
        // Detect rupee collision when last spin around a pole was to the left
      } else {
        if (angle == 0) {
          if (xPos + 5 >= rupeeX && xPos - 5 <= rupeeX && yPos + 90 > rupeeY && yPos < rupeeY) {
            rupees[i][3] = 1;
          }
        } else if (angle == 180){
          if (xPos + 5 >= rupeeX && xPos - 5 <= rupeeX && yPos > rupeeY && yPos - 90 < rupeeY) {
            rupees[i][3] = 1;
          }
        } else if (angle == 90) {
          if (yPos + 5 >= rupeeY && yPos - 5 <= rupeeY && xPos > rupeeX && xPos - 90 < rupeeX) {
            rupees[i][3] = 1;
          }
        } else if (angle == 270) {
          if (yPos + 5 >= rupeeY && yPos - 5 <= rupeeY && xPos < rupeeX && xPos + 90 > rupeeX) {
            rupees[i][3] = 1;
          }
        }
      }

      // Draw the rupees on the screen
      if (rupeeVisible == 1){
          noStroke();
          rectMode(CENTER);
          // Draw horizontal rupees
        if (rupeeRotation == 0) {
          fill(255, 188, 71);
          rect(rupeeX, rupeeY, 40, 20);
          triangle(rupeeX - 20, rupeeY - 10, rupeeX - 30, rupeeY, rupeeX - 20, rupeeY + 10);
          triangle(rupeeX + 20, rupeeY - 10, rupeeX + 30, rupeeY, rupeeX + 20, rupeeY + 10);
          fill(255, 142, 39);
          rect(rupeeX, rupeeY, 30, 13);
          triangle(rupeeX - 15, rupeeY - 7, rupeeX - 22, rupeeY, rupeeX - 15, rupeeY + 7);
          triangle(rupeeX + 15, rupeeY - 7, rupeeX + 22, rupeeY, rupeeX + 15, rupeeY + 7);
          // Draw vertical rupees
        } else {
          fill(255, 188, 71);
          rect(rupeeX, rupeeY, 20, 40);
          triangle(rupeeX - 10, rupeeY - 20, rupeeX, rupeeY - 30, rupeeX + 10, rupeeY - 20);
          triangle(rupeeX - 10, rupeeY + 20, rupeeX, rupeeY + 30, rupeeX + 10, rupeeY + 20);
          fill(255, 142, 39);
          rect(rupeeX, rupeeY, 13, 30);
          triangle(rupeeX - 7, rupeeY - 15, rupeeX, rupeeY - 22, rupeeX + 7, rupeeY - 15);
          triangle(rupeeX -7, rupeeY + 15, rupeeX, rupeeY + 22, rupeeX + 7, rupeeY + 15);
        }
        rupeeCounter++;
      }
      // Victory condition
      if (rupeeCounter >= rupees.length) {
        result = true;
        gameState = "score";
      }
  }
}

// Controls time limit
public void detectTime(){
  textSize(20);
  textAlign(CENTER, CENTER);
  fill(255);
  text("Time: " + time/120, 755, 45);
  if (time <= 59) {
    result = false;
    gameState = "score";
  }
  time--;
}

// Draw the poles on the screen
public void drawPoles() {
  int poleX = 90;
  int poleY = 90;
  for (int i = 0; i < 8; i++) {
    poleX = 90;
    for (int j = 0; j < 8; j++) {
      noStroke();
      fill(255);
      ellipse(poleX, poleY, 12, 12);
      fill(180, 10, 0);
      ellipse(poleX, poleY, 7, 7);
      poleX += 90;
    }
    poleY += 90;
  }
}

// Handles wall collision
public void detectWalls() {
  // If last spin around a pole was to the right
  if (lastRotation.equals("right")) {
    switch(angle){
      case 0:
        if (xPos >= width - 20) {
          angle = 180;
          yPos -= 90;
        }
        break;
      case 90:
        if (yPos >= height - 20) {
          angle = 270;
          xPos += 90;
        }
        break;
      case 180:
        if (xPos <= 20) {
          angle = 0;
          yPos += 90;
        }
        break;
      case 270:
        if (yPos <= 20) {
          angle = 90;
          xPos -= 90;
        }
        break;
    }
    // If last spin around a pole was to the left
  } else {
    switch(angle){
      case 0:
        if (xPos >= width - 20) {
          angle = 180;
          yPos += 90;
        }
        break;
      case 90:
        if (yPos >= height - 20) {
          angle = 270;
          xPos -= 90;
        }
        break;
      case 180:
        if (xPos <= 20) {
          angle = 0;
          yPos -= 90;
        }
        break;
      case 270:
        if (yPos <= 20) {
          angle = 90;
          xPos += 90;
        }
        break;
    }
  }
}

// Handles drawing Fizzy and rotating Fizzy around a poll
public void drawFizzy(int x, int y, int rot) {
  noStroke();
  rectMode(CENTER);
  pushMatrix();
  translate(x, y);
  rotate(radians(rot));
  fill(255, 0 , 0);
  // If Fizzy last turned right
  if (lastRotation.equals("right")) {
    // Fizzy sticks out right arm
    if (keyPressed && key == 'd') {
      stroke(255);
      strokeWeight(5);
      line(0, -7, 0, -45);
      stroke(255, 0, 0);
      line(-5, -7, 5, -7);
      line(-5, -7, -5, 5);
      line(-5, 7, 5, 7);
      noStroke();
      // Fizzy sticks out left arm if not spinning
    } else if (keyPressed && key == 'a' && !rotate && !stillRotating) {
      stroke(255);
      strokeWeight(5);
      line(0, -83, 0, -45);
      stroke(255, 0, 0);
      line(-5, -97, 5, -97);
      line(-5, -97, -5, -85);
      line(-5, -83, 5, -83);
      noStroke();
      // Fizzy sticks out right arm if still spinning
    } else if (rotate || stillRotating) {
      stroke(255);
      strokeWeight(5);
      line(0, -7, 0, -45);
      stroke(255, 0, 0);
      line(-5, -7, 5, -7);
      line(-5, -7, -5, 5);
      line(-5, 7, 5, 7);
      noStroke();
    }
    // Draw Fizzy's legs
    if (fizzyFlash < 20) {
      stroke(218, 165, 15);
      strokeWeight(5);
      line(-18, -35, -22, -25);
      stroke(255, 0, 0);
      line(-20, -45, -28, -48);
      noStroke();
    } else {
      stroke(218, 165, 15);
      strokeWeight(5);
      line(-18, -55, -22, -65);
      stroke(255, 0, 0);
      line(-20, -45, -28, -42);
      noStroke();
    }
    // Draw Fizzy's body
    fill(255, 0, 0);
    ellipse(0, -45, 45, 45);
    fill(0);
    ellipse(4, -45, 20, 30);
    fill(255);
    rect(8, -45, 11, 25);
    fill(0);
    ellipse(8, -49, 5, 5);
    ellipse(8, -41, 5, 5);
    fill(255, 0, 0);
    rect(0, -45, 30, 5);
    // If Fizzy last turned left
  } else {
    // Fizzy sticks out left arm
    if (keyPressed && key == 'a') {
      stroke(255);
      strokeWeight(5);
      line(0, 7, 0, 45);
      stroke(255, 0, 0);
      line(-5, -7, 5, -7);
      line(-5, -7, -5, 5);
      line(-5, 7, 5, 7);
      noStroke();
      // Fizzy sticks out right arm if not spinning
    } else if (keyPressed && key == 'd' && !rotate && !stillRotating) {
      stroke(255);
      strokeWeight(5);
      line(0, 83, 0, 45);
      stroke(255, 0, 0);
      line(-5, 83, 5, 83);
      line(-5, 83, -5, 95);
      line(-5, 95, 5, 95);
      noStroke();
      // Fizzy sticks out left arm if spinning
    } else if (rotate || stillRotating) {
      stroke(255);
      strokeWeight(5);
      line(0, 7, 0, 45);
      stroke(255, 0, 0);
      line(-5, -7, 5, -7);
      line(-5, -7, -5, 5);
      line(-5, 7, 5, 7);
      noStroke();
    }
    // Draw Fizzy's legs
    if (fizzyFlash < 20) {
      stroke(218, 165, 15);
      strokeWeight(5);
      line(-18, 35, -22, 25);
      stroke(255, 0, 0);
      line(-20, 45, -28, 48);
      noStroke();
    } else {
      stroke(218, 165, 15);
      strokeWeight(5);
      line(-18, 55, -22, 65);
      stroke(255, 0, 0);
      line(-20, 45, -28, 42);
      noStroke();
    }
    // Draw Fizzy's body
    ellipse(0, 45, 45, 45);
    fill(0);
    ellipse(4, 45, 20, 30);
    fill(255);
    rect(8, 45, 11, 25);
    fill(0);
    ellipse(8, 49, 5, 5);
    ellipse(8, 41, 5, 5);
    fill(255, 0, 0);
    rect(0, 45, 30, 5);
  }
  popMatrix();
  
  // Controls Fizzy spinning around polls
  if (rotate) {
    if (lastRotation.equals("right")) {
      angle += fizzySpeed;
    } else {
      angle -= fizzySpeed;
    }
    move = false;
  } else {
    // If Fizzy is not facing a right angle, Fizzy keeps spinning.
      if (rot != 0 && rot % 90 != 0) {
        if (lastRotation.equals("right")) {
          angle += fizzySpeed;
        } else {
          angle -= fizzySpeed;
        }
        move = false;
        stillRotating = true;
      } else {
        move = true;
        stillRotating = false;
      }
  }
  
  // The angle of Fizzy loops back to 0 after hitting 360
  if (rot >= 360){
    angle = 0;
  } else if (rot < 0) {
    angle = 359;
  }
  
  // Controls the movement of Fizzy when not spinning
  if (move){
    switch(rot){
      case 0:
        xPos += fizzySpeed;
        break;
      case 90:
        yPos += fizzySpeed;
        break;
      case 180:
        xPos -= fizzySpeed;
        break;
      case 270:
        yPos -= fizzySpeed;
        break;
    }
  }
  fizzyFlash++;
  if (fizzyFlash > 40) {
    fizzyFlash = 0;
  }
}

// Keeps Fizzy in-between the poles
public void detectPosition() {
  if (angle == 90 || angle == 270) {
    if (xPos % 90 >= 1 && xPos % 90 < 10 ) {
      xPos -= xPos % 90;
    } else if (xPos % 90 <= 89 && xPos % 90 > 80) {
      xPos += 90 - xPos % 90;
    }
  } else {
    if (yPos % 90 >= 1 && yPos % 90 < 10 ){
      yPos -= yPos % 90;
    } else if (yPos % 90 <= 89 && yPos % 90 > 80) {
      yPos += 90 - yPos % 90;
    }
  }
}

// Makes Fizzy rotate if the hand is extended and hits a pole
public void detectPoles() {
  // If right hand is extended
  if (keyPressed && key == 'd' || key == 'D') {
    // If a poll is collided with
    if ((xPos % 90 >= 89 || xPos % 90 <= 1) && (yPos % 90 >= 89 || yPos % 90 <= 1)) {
      // If direction has changed
      if (lastRotation.equals("left")) {
        // If Fizzy is not currently spinning around a poll
        if (!rotate && !stillRotating) {           
          switch(angle){
            case 0:
              yPos += 90;
              break;
            case 90:
              xPos -= 90;
              break;
            case 180:
              yPos -= 90;
              break;
            case 270:
              xPos += 90;
              break;
          }
          lastRotation = "right";
        }
      }
      rotate = true;
    }
    // If left hand is extended
  } else if (keyPressed && key == 'a' || key == 'A') {
    // If hand hits a poll
    if ((xPos % 90 >= 89 || xPos % 90 <= 1) && (yPos % 90 >= 89 || yPos % 90 <= 1)) {
      // If direction has changed
      if (lastRotation.equals("right")) {
        // If Fizzy is not currently spinning around a poll
        if (!rotate && !stillRotating) {
          switch(angle){
            case 0:
              yPos -= 90;
              break;
            case 90:
              xPos += 90;
              break;
            case 180:
              yPos += 90;
              break;
            case 270:
              xPos -= 90;
              break;
            }
          lastRotation = "left";
        }
      }
      rotate = true;
    }
  } else {
    rotate = false;
  }
}

public void drawBlackHoles() {
  flash++;
  for (int i = 0; i < blackHoles.length; i++) {
    fill(255);
    int blackHoleX = blackHoles[i][0];
    int blackHoleY = blackHoles[i][1];
    if (flash <= 10) {
      ellipse(blackHoleX, blackHoleY, 50, 50);
    } else if (flash > 10 && flash <= 20) {
      ellipse(blackHoleX, blackHoleY, 51, 51);
    } else if (flash > 20 && flash <= 30) {
      ellipse(blackHoleX, blackHoleY, 52, 52);
    } else if (flash > 30 && flash <= 40) {
      ellipse(blackHoleX, blackHoleY, 51, 51);
    } else {
      flash = 0;
    }
    fill(0);
    ellipse(blackHoleX, blackHoleY, 45, 45);
    // Detect collision with Fizzy
      if (lastRotation.equals("right")) {
        if (angle == 0) {
          if (xPos + 48 >= blackHoleX && xPos - 48 <= blackHoleX && yPos > blackHoleY && yPos - 90 < blackHoleY) {
            endGame();
          }
        } else if (angle == 180) {
          if (xPos + 48 >= blackHoleX && xPos - 48 <= blackHoleX && yPos < blackHoleY && yPos + 90 > blackHoleY) {
            endGame();
          }
        } else if (angle == 90) {
          if (yPos + 48 >= blackHoleY && yPos - 48 <= blackHoleY && xPos < blackHoleX && xPos + 90 > blackHoleX) {
            endGame();
          }
        } else if (angle == 270) {
          if (yPos + 48 >= blackHoleY && yPos - 48 <= blackHoleY && xPos > blackHoleX && xPos - 90 < blackHoleX) {
            endGame();
          }
        }
      } else {
      if (angle == 0) {
        if (xPos + 48 >= blackHoleX && xPos - 48 <= blackHoleX && yPos < blackHoleY && yPos + 90 > blackHoleY) {
          endGame();
        }
      } else if (angle == 180) {
        if (xPos + 48 >= blackHoleX && xPos - 48 <= blackHoleX && yPos > blackHoleY && yPos - 90 < blackHoleY) {
          endGame();
        }
      } else if (angle == 90) {
        if (yPos + 48 >= blackHoleY && yPos - 48 <= blackHoleY && xPos > blackHoleX && xPos - 90 < blackHoleX) {
          endGame();
        }
      } else if (angle == 270) {
        if (yPos + 48 >= blackHoleY && yPos - 48 <= blackHoleY && xPos < blackHoleX && xPos + 90 > blackHoleX) {
          endGame();
        }
      }   
    }
  }
}

public void endGame() {
  result = false;
  deathByBlackHole = true;
  gameState = "score";
}

// Fizzy will stop spinning if key is released
public void keyReleased() {
    rotate = false;
}
  public void settings() {  size(810, 810); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "ass2" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
