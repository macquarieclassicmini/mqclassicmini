class TimeLord {
	private int doorClose;
	private int doorOpen = -1;
	private int doorOpenMeta;
	private int levelStart;
	private int totalFrames;

	int getDoorOpen() { return doorOpen; }
	int getDoorOpenMeta() { return doorOpenMeta; }
	int getLevelStart() { return levelStart; }
	int getTotalFrames() { return totalFrames; }

	void recvLevelUp() {
		doorClose = -1;
		doorOpenMeta = 100;
		levelStart = 40;
	}
	void recvGameOver() { levelStart = 1; }

	void tickAll(boolean isOnScreen) {
		totalFrames++;
		if (levelStart >= 0) levelStart--;
		if (doorClose >= 0) {
			doorClose--;
			if (doorClose < 0) {
				door.close();
				doorOpenMeta = 1;
			}
		}
		if (isOnScreen && doorOpenMeta >= 0) {
			doorOpenMeta--;
			if (doorOpenMeta == 0) doorOpen = 30 * floor(3 + random(6));
		}
		if (doorOpen >= 0) {
			doorOpen--;
			if (doorOpen < 1) {
				door.open();
				doorClose = 30 * floor(3 + random(6));
			}
		}
		mrGnW.checkNewScore();
	}
}
