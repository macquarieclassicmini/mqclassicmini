/**
 * There's really no reason to encapsulate this bool except that `door.isOpen()` looks nicer than a global `isDoorOpen`.
 */
class DoorMaster {
	private boolean isOpen;
	boolean isOpen() { return isOpen; }
	void close() { isOpen = false; }
	void open() { isOpen = true; }
}
