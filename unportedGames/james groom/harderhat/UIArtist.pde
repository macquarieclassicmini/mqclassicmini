class UIArtist {
	static final int CLOUD_MARGIN_LEFT = 130;
	static final int CLOUD_MARGIN_TOP = 30;
	static final int CLOUD_RADIUS = 40;
	static final int CLOUD_ROWS = 5;
	static final float CLOUD_ROW_LENGTH = 6; // float to trigger float division
	static final int CLOUD_SPREAD = 50;

	void drawCommon() {
		fill(0);
		text("LIFE", 8, 192);
		text(mrGnW.getHP(), 80, 224);
		text("PTS", 412, 128);
//		text(tools.numDodged, 412, 160);
		text(newLifeLoosed * (int) CLOUD_ROW_LENGTH + newPoints, 412, 160);
	}

	void drawIngame() {
		drawCommon();
		stroke(0, 0, 50);
		for (int x = 4; x < width; x += 16) line(x, 256, x + 8, 256);
		// left door
		stroke(0);
		if (mrGnW.isOnScreen()) {
			line(48, 276, 48, 324);
			fill(0);
			line(48, 300, 52, 300);
			noStroke();
			ellipse(53, 300, 4, 6);
		} else {
			fill(-1);
			rect(48, 276, 32, 48);
			fill(0);
			noStroke();
			ellipse(74, 300, 6, 6);
		}
		// right door
		stroke(0);
		if (door.isOpen()) {
			fill(-1);
			rect(432, 276, 32, 48);
			fill(0);
			noStroke();
			ellipse(438, 300, 6, 6);
		} else {
			line(464, 276, 464, 324);
			fill(0);
			line(464, 300, 460, 300);
			noStroke();
			ellipse(460, 300, 4, 6);
		}
		// stormclouds? blood clouds? 3-sphere projections?
		for (int vBreak = newLifeLoosed + floor((newPoints - 1) / CLOUD_ROW_LENGTH), y = 0; y < vBreak + 1; y++)
			for (int hBreak = floor(y < vBreak ? CLOUD_ROW_LENGTH : ((newPoints - 1) % CLOUD_ROW_LENGTH + 1)), x = 0; x < hBreak; x++)
				gradEllipseOfBrightness(y < newLifeLoosed ? 100 : 0,
					CLOUD_MARGIN_LEFT + CLOUD_SPREAD * x, CLOUD_MARGIN_TOP + CLOUD_SPREAD * y, CLOUD_RADIUS);
		// UI proper
		fill(0);
		text("LVL", 32, 96);
		text(currentLevel < LEVEL_COUNT ? " " + currentLevel : currentLevel + "!", 56, 128);
		if (timer.getDoorOpenMeta() <= 0) {
			text("OPEN", 412, 224);
			text("->" + (door.isOpen() ? "GO" : framesToString(timer.getDoorOpen())), 412, 256);
		}
	}

	void drawEndGame() {
		drawCommon();
		text(currentLevel > LEVEL_COUNT ? "A WINNER" : "A  LOSER", 160, 96);
		text("IS   YOU", 160, 128);
		text("TIME", 412, 32);
		text(framesToString(timer.totalFrames), 412, 64);
	}
}
