class ToolGovernor {
	static final int COLS = 5;
	static final int COL_LENGTH = 5;
	final int[] MAX_PER_LEVEL = new int[]{0, 2, 4, 6, 8};
	final int[] CHANCE_PER_LEVEL = new int[]{0, 3, 5, 7, 9};

	int[][] toolPosData = new int[][]{new int[5], new int[5], new int[5], new int[5], new int[5]};
	int numDodged;
	int toolsOnscreen;

	void tickInCol(int col) {
		// switch on each position in column `col`, from bottom
		for (int i = COL_LENGTH - 1; i >= 0; i--) switch (toolPosData[col][i]) {
			// even = fading in, odd = fading out (thus a tool cycles through 0 -> 2 -> 4 -> 3 -> 1 -> -1 -> 0)
			case 2: toolPosData[col][i] += 2; break;
			case 4: toolPosData[col][i] -= 1; break;
			case 3: case 1: toolPosData[col][i] -= 2; break;
			case -1:
				toolPosData[col][i] -= 2;
				toolPosData[col][i] = 0;
				if (i < COL_LENGTH - 1) {
					toolPosData[col][i + 1] = 2;
				} else {
					toolsOnscreen--;
					if (mrGnW.willDodge(col) && mrGnW.isOnScreen()) {
						numDodged++;
						newPoints++;
					}
				}
		}

		// randomly introduce another tool if: 1) the level hasn't just started; 2) there aren't too many tools; and 3) the first slot in this column (and the previous, if applicable and not on the last level) are empty
		if (timer.getLevelStart() < 1 && toolsOnscreen < MAX_PER_LEVEL[currentLevel] &&
				toolPosData[col][0] == 0 && (col == 0 || currentLevel == LEVEL_COUNT || toolPosData[col - 1][0] == 0) &&
				random(20) < CHANCE_PER_LEVEL[currentLevel]) { // 1d20, weighted for difficulty
			toolPosData[col][0] = 2;
			toolsOnscreen++;
		}
	}

	void drawAll() {
		for (int col = 0; col < COLS; col++) for (int pos = 0; pos < COL_LENGTH; pos++)
			if (toolPosData[col][pos] > 0) {
				// Opacity: `tools[col][pos]` => {`-1..0` => 0 (not drawn), `1..2` => 95, `3..4` => 255}
				int x = 128 + 64 * col;
				int y = 32 + 48 * pos;
				int type = col % 3;
				fill(90, 50, toolPosData[col][pos] > 2 ? 20 : 50);
				// Mr. Game & Watch was hard enough, not drawing tools
				if (type == 0) ellipse(x, y, 32, 32);
				else if (type == 1) diamond(x, y, 32, 32);
				else dwardIsocTriangle(x, y, 32, 32);
			}
	}
}
