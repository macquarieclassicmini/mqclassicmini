class MrGameAndWatch {
	private static final int INVINCIBILITY_DURATION = 20;
	private final int[][] NOROT_SEGMENTS = new int[][]{
		new int[]{1, 10, 12, 14}, new int[]{-3, 19, 5, 9}, new int[]{-5, 23, 12, 4},
		new int[]{3, -8, 22, 21}, new int[]{0, 6, 4, 14}, new int[]{6, 16, 7, 4}
	};
	private final int[][] ROT_SEGMENTS = new int[][]{
		new int[]{8, 1, 3, 5}, new int[]{11, -1, 5, 3}, new int[]{14, -1, 3, 3},
		new int[]{15, 1, 3, 3}, new int[]{15, -3, 3, 4}, new int[]{-2, 5, 5, 3},
		new int[]{-4, 8, 3, 5}, new int[]{-4, 11, 3, 3}, new int[]{-2, 12, 3, 3},
		new int[]{-6, 12, 4, 3}, new int[]{19, 10, 7, 4}, new int[]{22, 8, 4, 10},
		new int[]{-11, -10, 10, 4}
	};

	private int hp = 1;
	private int invulnTimer;
	private int pos;

	void addHP() { hp++; }
	int getHP() { return hp; }

	void tickInvulnTimer() { if (invulnTimer >= 0) invulnTimer--; }
	int getInvulnFrames() { return invulnTimer; }

	boolean isOnScreen() { return pos > -1; }

	int getPos() { return pos; }
	void setPos(int i) {
		pos = i;
	}

	boolean willDodge(int col) {
		if (invulnTimer >= 0 || col != pos) return true;
		// else hit
		hp--;
		newLifeLoosed++;
		invulnTimer = INVINCIBILITY_DURATION;
		pos = 0;
		if (hp < 1) triggerGameOver();
		return false;
	}

	void checkNewScore() {
		if (newLifeLoosed + (newPoints / UIArtist.CLOUD_ROW_LENGTH) > UIArtist.CLOUD_ROWS) triggerGameOver();
	}

	void drawSelf() {
		// Pixel-perfection in matching vector graphics is very hard
		int fillColour = color(invulnTimer < 0 ? #000000 : (invulnTimer % 6 < 3 ? #5F5F5F : #BFBFBF));
		translate(128 + 64 * pos, 300);
		if (pos < ToolGovernor.COLS) {
			if (pos == -1 || pos % 2 == 0) {
				if (pos == -1) translate(24, 0);
				scale(-1, 1);
			}
			fill(fillColour);
			for (int[] tuple : NOROT_SEGMENTS) ellipse(tuple[0], tuple[1], tuple[2], tuple[3]);
			rotate(TWO_PI / 11);
			fill(-1);
			ellipse(-5, -3, 6, 7);
			ellipse(-8, -4, 12, 5);
			fill(fillColour);
			for (int[] tuple : ROT_SEGMENTS) ellipse(tuple[0], tuple[1], tuple[2], tuple[3]);
			rotate(TWO_PI / 14);
			ellipse(-16, -10, 10, 4);
		}
		resetMatrix();
	}
}
