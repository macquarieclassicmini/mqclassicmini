void diamond(float x, float y, float w, float h) {
	float hw = w / 2;
	float hh = h / 2;
	beginShape();
	vertex(x + hw, y);
	vertex(x, y + hh);
	vertex(x - hw, y);
	vertex(x, y - hh);
	endShape(CLOSE);
}
void dwardIsocTriangle(float x, float y, float w, float h) {
	float hw = w / 2;
	float hh = h / 2;
	triangle(x + hw, y - hh, x - hw, y - hh, x, y + hh);
}
void gradEllipseOfBrightness(int br, float x, float y, float r) {
	for (float i = r; i > 0; i--) {
		fill(0, 100, br, 10);
		ellipse(x, y, i, i);
	}
}

String framesToString(int f) { return ceil(f / 30f) + "s"; }

final int LEVEL_COUNT = 3; // level 4 was too hard, even with a difficulty reduction
int currentLevel;

DoorMaster door = new DoorMaster();
MrGameAndWatch mrGnW = new MrGameAndWatch();
TimeLord timer = new TimeLord();
ToolGovernor tools = new ToolGovernor();
UIArtist ui = new UIArtist();

int newPoints = -3; // incr. by 3 at start
int newLifeLoosed; // I assume the "loosing" of life refers to Mr. Game & Watch's blood being drawn and then poured over the screen

void triggerLevelUp() {
	currentLevel++;
	newPoints += 3;
	timer.recvLevelUp();
	door.close();
	mrGnW.setPos(-1);
	for (int col = 0; col < ToolGovernor.COLS; col++) for (int pos = 0; pos < ToolGovernor.COL_LENGTH; pos++) tools.toolPosData[col][pos] = 0;
	if (currentLevel <= LEVEL_COUNT) {
		mrGnW.addHP();
		tools.toolPosData[currentLevel][0] = 4;
		tools.toolPosData[(currentLevel + 2) % ToolGovernor.COLS][0] = 4;
		tools.toolsOnscreen = 2;
	}
}
void triggerGameOver() {
	currentLevel = -1;
	timer.recvGameOver();
}

void setup() {
	colorMode(HSB, 360, 100, 100, 100);
	frameRate(30);
	size(512, 348);
	strokeWeight(2);
	textFont(loadFont("PressStart2P-Regular-48.vlw"));
	textSize(24);

	triggerLevelUp();
	mrGnW.setPos(0);
}

void draw() {
	background(-1);
	if (currentLevel > 0 && currentLevel <= LEVEL_COUNT) {
		timer.tickAll(mrGnW.isOnScreen());
		mrGnW.tickInvulnTimer();
		tools.tickInCol(frameCount % 5);
		tools.drawAll();
		mrGnW.drawSelf();
		ui.drawIngame();
	} else {
		ui.drawEndGame();
	}
}

void keyPressed() {
	switch (keyCode) {
		case 37: case 65: case 74: // [LARR]; [A]; [J]
			mrGnW.setPos(max(mrGnW.isOnScreen() ? 1 : 0, mrGnW.getPos()) - 1);
			break;
		case 39: case 68: case 76: // [RARR]; [D]; [L]
			mrGnW.setPos(min(mrGnW.getPos(), ToolGovernor.COLS - (door.isOpen() ? 1 : 2)) + 1);
			if (mrGnW.getPos() == ToolGovernor.COLS) triggerLevelUp();
	}
}
