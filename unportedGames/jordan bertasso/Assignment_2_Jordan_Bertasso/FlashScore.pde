class FlashScore {
  PVector acc;
  PVector vel;
  PVector pos;
  int xOffset;
  int yOffset;

  float maxAcc;
  float minAcc;

  float alpha = 255;
  float flashTime = 0.5;  //In seconds

  color colour;           //Colour of the flying number to be randomised

  int intToFlash;         //The next integer that will be flashed
  boolean flashNow;       //If true, then intToFlash will be flashed

  FlashScore(float _minAcc, float _maxAcc) {
    acc = new PVector(0, 0);
    vel = new PVector(2, -5);
    pos = new PVector(0, 0);
    
    xOffset = ts.x + 40;
    yOffset = ts.y;

    minAcc = _minAcc;
    maxAcc = _maxAcc;

    colour = color(random(255), random(255), random(255));    //Generates a random colour for the flashing integer
  }

  void flash() {
    if (flashNow == true) {
      vel.add(acc);
      pos.add(vel);

      fill(colour, alpha);
      textSize(30);
      text(intToFlash, xOffset + pos.x, yOffset + pos.y);

      alpha -= 255/(frameRate*flashTime);
    }

    if (alpha <= 0) {
      flashNow = false;
      init();
    }
  }

  void flashInt (int _intToFlash) {
    init();
    flashNow = true;
    intToFlash = _intToFlash;
  }

  void init() {
    alpha = 255;
    colour = color(random(255), random(255), random(255));          //Generates a random colour for the flashing integer
    acc = new PVector(random(1, maxAcc), random(minAcc, maxAcc));   //Ensures that the number always travels to the right
    vel = new PVector(2, -5);
    pos = new PVector(0, 0);
  }
}
