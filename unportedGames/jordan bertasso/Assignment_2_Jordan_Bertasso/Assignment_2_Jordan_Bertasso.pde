// This code is based upon ideas and code by Matthew Roberts. //<>// //<>//
// It’s original source / text may be found at http://ilearn.mq.edu.au/mod/resource/view.php?id=4591562 / the COMP115 iLearn.

int gameMode;
float blackScore = 0;
float redScore = 0;
int maxCircles = 6*4;  // 4 rows and 6 columns

MrGW gw;             
RightDoor rd;        
ModePicker mp;       //The start screen text and arrow picker
TextScore ts;        //Score for endless mode
FlashScore flasher;  //The object that will flash the score added

Hammer[] hammers;    //The array that will be used to hold the tools
Dust[] dusts;        //The array that will be used to hold the dust particles

PImage wario;

void setup() {
  size(512, 348);
  frameRate(30);
  
  gameMode = 0;

  gw = new MrGW(width/8);
  rd = new RightDoor();
  mp = new ModePicker();
  ts = new TextScore();
  flasher = new FlashScore(0, 5);    //minimum and maximum acceleration of the flying numbers - for variation
  
  hammers = new Hammer[5];  //5 hammers
  dusts = new Dust[4];      //4 Dust partickes
  
  //Populate the array with hammers at a random starting velocity
  for (int i = 0; i < hammers.length; i++) {
    hammers[i] = new Hammer(2*width/8 + i*width/8, 0, random(1, 3), 60);
  }
    
  //Populate the dusts array with dust particles
  for (int i = 0; i < dusts.length; i++) {
    dusts[i] = new Dust();
  }
  
  wario = loadImage("Wario.png");
  wario.resize(0, 50);
}

void draw() {
  chooseMode(gameMode);
}

void keyPressed() {
  //Only allow Mr GW to move if not on the start screen
  if (gameMode != 0) {
    gw.move();
  }

  //Bring the game back to the start screen
  if (key == 'k') {
    gameMode = 0;
    init();        //Re-initialises the game so that previous scores and positions are not saved
  }
}
