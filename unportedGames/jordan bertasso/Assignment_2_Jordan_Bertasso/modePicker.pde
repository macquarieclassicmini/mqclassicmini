class ModePicker {

  int CIRCLES_POINTER_OFFSET;
  int ENDLESS_CIRCLES_OFFSET;
  int pointerOffset;

  ModePicker() {
    CIRCLES_POINTER_OFFSET = -32;
    ENDLESS_CIRCLES_OFFSET = 10;
    pointerOffset = CIRCLES_POINTER_OFFSET;
  }

  void display() {
    displayBackground();

    //Displays the modes to choose from
    fill(0);
    textSize(32);
    textAlign(CENTER);
    text("Circles", width/2, height/2 - 20);
    text("Endless", width/2, height/2 + 20);
    
    textSize(16);
    text("Press 'k' to return to the main menu", width/2, height-10);

    //Displays the pointer next to the highlighted mode
    fill(0);
    triangle(190, height/2 + pointerOffset, 170, height/2 - 10 + pointerOffset, 170, height/2 + 10 + pointerOffset);
  }

  void move() {
    //To move the pointer
    if (gameMode == 0) {
      if (key == CODED) {
        if (keyCode == DOWN && pointerOffset == CIRCLES_POINTER_OFFSET) {
          pointerOffset = ENDLESS_CIRCLES_OFFSET;
        } else if (keyCode == UP && pointerOffset == ENDLESS_CIRCLES_OFFSET) {
          pointerOffset = CIRCLES_POINTER_OFFSET;
        }
      }
    }
  }

  void select() {
    if (key == ENTER && pointerOffset == CIRCLES_POINTER_OFFSET) {
      gameMode = 1;
    } else if (key == ENTER && pointerOffset == ENDLESS_CIRCLES_OFFSET) {
      gameMode = 2;
    }
  }
}
