class Dust {

  PVector gWFoot;
  PVector acc;
  PVector vel;
  PVector pos;
  
  int gwBottom = 280;
  int radius = 3;

  Dust() {
    init();
  }

  void display() {
    //Draw the dust particle
    ellipseMode(CENTER);
    fill(#585757);
    noStroke();
    ellipse(pos.x, pos.y, radius, radius);

    vel.add(acc);
    pos.add(vel);

    //Resets the dust if it goes off the screen
    if (pos.y > gWFoot.y + 10 || pos.x < gWFoot.x - 40) {
      init();
    }
  }

  void init() {
    gWFoot = new PVector(gw.mrGWPos, gwBottom + 10);
    acc = new PVector(0, 2.5);
    vel = new PVector(random(-9, -1), random(-9, -4));
    pos = new PVector(gWFoot.x + random(-20, 0), gWFoot.y);
  }
}
