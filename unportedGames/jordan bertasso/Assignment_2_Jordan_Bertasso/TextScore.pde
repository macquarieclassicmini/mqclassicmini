class TextScore {

  int x = 40;
  int y = 40;

  void display() {
    fill(0);
    textSize(16);
    text("Score: " + (int) blackScore, x, y);    //Displays the current score in the top right (endless mode)
  }
}
