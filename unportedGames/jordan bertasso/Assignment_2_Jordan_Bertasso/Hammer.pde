class Hammer { //<>// //<>//

  float x;
  float y;
  float vel;
  float yPending;
  int jump;
  int GWTop = 240;  
  int GWBottom = 300;

  Hammer(float _x, float _y, float _vel, int _jump) {
    x = _x;
    y = _y;
    vel = _vel;
    jump = _jump;
  }

  void fall() {
    //Draw the hammer
    fill(0);
    stroke(0);
    strokeWeight(6);
    line(x, y+10, x+20, y-12);
    strokeWeight(4);
    line(x-3, y+13, x+1, y+11);
    strokeWeight(8);
    line(x-13, y+5, x+2, y+20);

    //Animate the hammer
    yPending = yPending + vel;
    if (yPending > jump) {
      y = y + yPending;
      yPending = 0;
    }

    //Move the hammer to the top of the screen if it reaches the bottom
    if (y >= height) {
      y = 0;
      blackScore+=1;  //Adds one black circle
      flasher.flashInt(+1);
    }

    //What to do if a hammer hits mrGW
    if (y > GWTop && y < GWBottom && gw.mrGWPos == x) {
      gw.mrGWPos = width/8;
      if (gameMode == 1) {
        blackScore += 6;  //Adds 6 black circles to move them down a row
        redScore += 1;    //Adds 1 row of red circles
      } else if (gameMode == 2) {
        if (blackScore >= 10) {
          blackScore -= 10;
          flasher.flashInt(-10);
        } else {
          blackScore = 0;
          flasher.flashInt(-10);
        }
      }
    }
  }
}
