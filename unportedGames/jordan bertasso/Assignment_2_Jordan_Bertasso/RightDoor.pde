class RightDoor {

  long timeAt;    // The time at which the door was last opened or closed
  long waitTime;  // The time to wait (in total) until the door opens next
  boolean open = true;

  int openDoorRight = 499;  int openWindowX = 476; int openWindowY = 225;
  int openHandleX = 487;    int openHandleY = 255;
  int topLineY = 206;       int bottomLineY = 314;

  int closedDoorWindowX = 432;  int closedDoorWindowY = 225;
  int closedDoorHandleX = 425;  int closedDoorHandleY = 255;


  RightDoor() {
    waitTime = int(random(3000, 8000));
  }

  void display() {
    if (open) {
      line(openDoorRight - 20, topLineY, openDoorRight, topLineY - 7);           //top line
      line(openDoorRight, topLineY - 7, openDoorRight, bottomLineY);             //right line
      line(openDoorRight, bottomLineY, openDoorRight - 21, bottomLineY - 12);    //bottom line
      fill(0);
      noStroke();
      //Draw the window in the open door
      triangle(openWindowX, openWindowY, openWindowX + 17, openWindowY - 6, openWindowX + 17, openWindowY + 18);
      triangle(openWindowX, openWindowY, openWindowX + 17, openWindowY + 18, openWindowX, openWindowY + 15);

      ellipse(openHandleX, openHandleY, openHandleX + 6, openHandleY + 8);
    } else {
      fill(0);
      noStroke();
      rect(closedDoorWindowX, closedDoorWindowY, closedDoorWindowX + 21, closedDoorWindowY + 18, 2);
      ellipse(closedDoorHandleX, closedDoorHandleY, closedDoorHandleX + 6, closedDoorHandleY + 8);
    }
  }

  void checkState() {
    if (millis() > timeAt + waitTime) {    // calculation will tell me if current time (millis) has gone past the last time the door opened plus waitTime
      open = !open;
      timeAt = millis();                   // reset last opening time to now
      waitTime = int(random(3000, 8000));  // choose new waiting time
    }
  }
}
