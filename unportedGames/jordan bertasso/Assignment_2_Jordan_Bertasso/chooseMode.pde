void chooseMode(int gameMode) {

  if (gameMode == 0) {

    mp.display();
    mp.move();
    mp.select();
  } else if (gameMode == 1) {

    displayBackground();
    rd.display();
    rd.checkState();  //Switches the state of the door depending on the timer
    gw.display();

    //Animates all the hammers in the array
    for (int i = 0; i < hammers.length; i++) {
      hammers[i].fall();
    }

    //Animates all the dust particles in the array
    for (int i = 0; i < dusts.length; i++) {
      dusts[i].display();
    }

    displayScore(0, 0);   //Draws the current score in circles - 0 paremeters because no score is being added
  } else if (gameMode == 2) {

    displayBackground();
    ts.display();
    rd.display();
    rd.checkState();     //Switches the state of the door depending on the timer
    gw.display();

    //Animates all the hammers in the array
    for (int i = 0; i < hammers.length; i++) {
      hammers[i].fall();
    }

    //Animates all the dust particles in the array
    for (int i = 0; i < dusts.length; i++) {
      dusts[i].display();
    }


    flasher.flash();    //Flashes the score just added if flashNow is true
  }
}
