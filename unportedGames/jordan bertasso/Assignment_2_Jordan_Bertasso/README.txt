# COMP115 - Assignment 2 #


## New Features ##

- Added second game mode (accessible with ‘k’)
- Added ballistic dust particles for a Mr GW running effect
- Added colourful Score Up/Score Down effect in endless mode - Numbers float away from score and
  their alpha goes to zero
- Refactored code into objects excluding the score up/score down function for circles
- Added Wario


### Author  & Acknowledgements###

- Made by Jordan Bertasso - 45391351
- Acknowledgements go to Matthew Roberts for providing the base reference code for a complete assignment 1