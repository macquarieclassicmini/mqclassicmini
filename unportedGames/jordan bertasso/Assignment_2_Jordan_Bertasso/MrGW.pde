class MrGW {

  int startingX;
  int slotSize;
  int rightDoorX;
  int mrGWPos;
  
  MrGW(int _startingX) {
    startingX = _startingX;
    mrGWPos = _startingX;
    slotSize = width/8;
    rightDoorX = 7*width/8;
  }
  
  void display() {
    // draw MrGW
    noStroke();
    fill(0);
    ellipse(mrGWPos-13, 232, mrGWPos+17, 263);
    fill(159, 172, 173);
    ellipse(mrGWPos+3, 250, mrGWPos+17, 260);
    fill(0);
    triangle(mrGWPos+2, 258, mrGWPos-11, 278, mrGWPos+1, 285);
  }

  void move() {
    if (keyCode == RIGHT && (mrGWPos < rightDoorX) && rd.open) {             //Mr GW is moving right and the door is open
      mrGWPos = mrGWPos + slotSize;
    } else if (keyCode == RIGHT && (mrGWPos < rightDoorX - slotSize)) {      //Mr GW is moving right not including last slot
      mrGWPos = mrGWPos + slotSize;
    } else if (keyCode == LEFT && (mrGWPos > startingX)) {                   //Mr GW is moving left.
      mrGWPos = mrGWPos - slotSize;
    }

    if (mrGWPos == rightDoorX) {    // resets Mr GW to the left if he goes through the door
      mrGWPos = startingX;          // and adds 3 to the score.
      blackScore += 3;
      flasher.flashInt(+3);         //Flashes the score just added (3), only in endless mode
    }
  }
}
