void init() {
  gw.mrGWPos = width/8;
  for (int i = 0; i < hammers.length; i++) {
    hammers[i] = new Hammer(2*width/8 + i*width/8, 0, random(1, 3), 60);
  }

  rd.open = true;
  rd.waitTime = int(random(3000, 8000));
  blackScore = 0;
  redScore = 0;
}
