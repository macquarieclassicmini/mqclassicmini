void displayBackground() {
  
  //Variables to draw the balcony and the entrance door
  int mainBalconyX = 0;      int mainBalconyY = 65;
  int orangeInsertX = 4;     int orangeInsertY = 74;
  int entranceX = 0;         int entranceY = 205;
  int entranceWindowX = 12;  int entranceWindowY = 221;
  int entranceHandleX = 32;  int entranceHandleY = 251;

  background(159, 172, 173);
  rectMode(CORNERS);
  ellipseMode(CORNERS);
  noStroke();

  //Draw the balcony  
  fill(145, 98, 41);       // main orange bit
  rect(mainBalconyX, mainBalconyY, mainBalconyX + 44, mainBalconyY + 140);
  fill(193, 158, 153);     // light orange inserts
  rect(orangeInsertX, orangeInsertY, orangeInsertX + 40, orangeInsertY + 5);
  rect(orangeInsertX, orangeInsertY + 56, orangeInsertX + 40, orangeInsertY + 61);
  rect(orangeInsertX, orangeInsertY + 117, orangeInsertX + 40, orangeInsertY + 122);
  fill(159, 172, 173);     // grey inserts
  rect(orangeInsertX, mainBalconyY + 20, orangeInsertX + 40, mainBalconyY + 35);
  rect(orangeInsertX, mainBalconyY + 79, orangeInsertX + 40, mainBalconyY + 95);

  //Draw entrance
  noFill();
  strokeWeight(2);
  stroke(0);
  rect(entranceX, entranceY, entranceX + 44, entranceY + 85);
  fill(0);
  rect(entranceWindowX, entranceWindowY, entranceWindowX + 17, entranceWindowY + 15, 1);
  ellipse(entranceHandleX, entranceHandleY, entranceHandleX + 6, entranceHandleY + 7);

  //Variables for the lines positions on the ground
  int l1X = 39;     int l1Y = 295;
  int l2X = 131;    int l2Y = 293;
  int l3X = 193;    int l3Y = 295;
  int l4X = 269;    int l4Y = 292;
  int l5X = 327;    int l5Y = 294;
  int l6X = 244;    int l6Y = 303;

  // draw ground
  strokeWeight(3);
  stroke(180, 123, 119);
  line(l1X, l1Y, l1X + 42, l1Y - 1);
  line(l2X, l2Y, l2X + 40, l2Y - 5);
  line(l3X, l3Y, l3X + 7, l3Y - 6);
  line(l4X, l4Y, l4X + 28, l4Y - 2);
  line(l5X, l5Y, l5X + 73, l5Y + 4);
  line(l6X, l6Y, l6X-10, l6Y - 5);
  stroke(143, 112, 117);

  //Variables for the trees positions
  int ltX = 443;  int ltY = 104;
  int mtX = 474;  int mtY = 102;
  int rtX = 506;  int rtY = 82;

  // draw trees
  noStroke();
  fill(86, 131, 35);
  //left tree
  triangle(ltX, ltY, ltX - 10, ltY + 77, ltX + 13, ltY + 74);
  triangle(ltX, ltY, ltX - 12, ltY + 21, ltX + 12, ltY + 20);
  triangle(ltX - 15, ltY + 31, ltX + 14, ltY + 31, ltX, ltY - 12);
  triangle(ltX - 26, ltY + 86, ltX, ltY + 11, ltX + 25, ltY + 81);

  //middle tree
  triangle(mtX, mtY, mtX - 9, mtY + 31, mtX + 6, mtY + 34);
  triangle(mtX, mtY + 6, mtX - 15, mtY + 24, mtX + 11, mtY + 24);
  triangle(mtX, mtY + 18, mtX - 16, mtY + 36, mtX + 12, mtY + 36);
  triangle(mtX, mtY + 18, mtX - 17, mtY + 47, mtX + 19, mtY + 46);

  // right tree
  triangle(rtX, rtY, rtX - 37, rtY + 88, rtX + 36, rtY + 88);

  int leftRoofX = 402;     int leftRoofY = 206;
  int rightRoofX = 490;    int rightRoofY = 169;
  int leftWallX = 411;     int leftWallY = 213;
  int closedDoorX = 419;   int closedDoorY = 210; 
  
  // draw house
  strokeWeight(20);
  stroke(0);
  line(leftRoofX, leftRoofY, leftRoofX + 88, leftRoofY - 37);
  line(rightRoofX, rightRoofY, rightRoofX + 22, rightRoofY + 10);
  strokeWeight(2);
  noFill();
  line(leftWallX, leftWallY, leftWallX, leftWallY + 92);
  rect(closedDoorX, closedDoorY, closedDoorX + 49, closedDoorY + 85);

  //Puts Wario on top of the shed and rotates him so that he is lying flat against the roof
  pushMatrix();
  translate(403, 146);
  rotate(-QUARTER_PI/2);
  image(wario, -5, 5);
  popMatrix();
}
