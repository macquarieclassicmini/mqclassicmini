void displayScore(float tempScoreChange, float tempScoreRedChange) {

  blackScore += tempScoreChange;
  redScore += tempScoreRedChange;

  int NUMBER_NICE_CIRCLES = 15;
  int CIRCLES_PER_ROW = 6;
  int CIRCLE_SIZE_INCREMENT = 5;
  int ROW_GAP = 80;
  int COLUMN_GAP = width/CIRCLES_PER_ROW;
  int RED_CIRCLE_SIZE = 71;
  int rowCount = ceil(blackScore/CIRCLES_PER_ROW);
  
  //Clears the circles if they are filling the screen
  if (blackScore >= maxCircles){
    blackScore = 0;
    redScore = 0;
  }

  //Draws the correct amount of black circles according to blackScore
  //The third loop is to draw multiple circles for "Nicer circles"
  for (int i = 0; i < rowCount; i++) {
    for (int j = 0; j < blackScore - i*CIRCLES_PER_ROW; j++) {
      for (int k = 0; k < NUMBER_NICE_CIRCLES; k++) {
        noStroke();
        ellipseMode(CENTER);
        fill(0, 0, 0, 50);    //Alpha for nice circles
        ellipse(43 +j*COLUMN_GAP, 50 + i*ROW_GAP, CIRCLE_SIZE_INCREMENT*k, CIRCLE_SIZE_INCREMENT*k);
      }
    }
  }

  //Draws the correct amount of red circles according to redScore
  for (int i = 0; i < redScore; i++) {
    for (int j = 0; j < CIRCLES_PER_ROW; j++) {
      fill(255, 0, 0);
      noStroke();
      ellipseMode(CENTER);
      ellipse(43 +j*COLUMN_GAP, 50 + i*ROW_GAP, RED_CIRCLE_SIZE, RED_CIRCLE_SIZE);
    }
  }
}
