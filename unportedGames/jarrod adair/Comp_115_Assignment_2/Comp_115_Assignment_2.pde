int[] PPS = {40, 104, 168, 232, 296, 360, 422};    // Player Positions
int[] ToolPS = {50,100,150,200,200};               // Tool Positions
int PP, ToolQueue, M, Secs, Score, DScore, PScore = 0;
boolean PDead, SetupImagesComplete = false;
boolean CanPress, DoorOpen = true;
PImage Background, PlayerImg1, PlayerImg2, PlayerImg3, PlayerImg4, PlayerImg5, PlayerImg6, PlayerImg7, PlayerDead, Tool1, Tool2, Tool3, Tool4, Tool5, DoorImgClosed, DoorImgOpen;
Tool[] ToolList = new Tool[15];                    // 30 = max amount of tools
int Time = millis();
int rand = (int)random(3, 9);
int gameMode = 0;    //0 = menu; 1 = normal; 2 = fire;

FireMode firemode = new FireMode();

void setup () {
  frameRate(60);
  size(512, 348);    //Width, Height
  background(200);
}

void SetupImages() {
  if (gameMode != 0) {
    Background = loadImage("Images/Background.jpg");
    PlayerImg1 = loadImage("Images/PlayerImg1.png");
    PlayerImg2 = loadImage("Images/PlayerImg2.png");
    PlayerImg3 = loadImage("Images/PlayerImg3.png");
    PlayerImg4 = loadImage("Images/PlayerImg4.png");
    PlayerImg5 = loadImage("Images/PlayerImg5.png");
    PlayerImg6 = loadImage("Images/PlayerImg6.png");
    PlayerImg7 = loadImage("Images/PlayerImg7.png");
    PlayerDead = loadImage("Images/PlayerDead.png");
    Tool1 = loadImage("Images/Tool1.png");
    Tool2 = loadImage("Images/Tool2.png");
    Tool3 = loadImage("Images/Tool3.png");
    Tool4 = loadImage("Images/Tool4.png");
    Tool5 = loadImage("Images/Tool5.png");
    DoorImgClosed = loadImage("Images/DoorImgClosed.png");
    DoorImgOpen = loadImage("Images/DoorImgOpen.png");
    buffer1 = createGraphics(w, h);
    buffer2 = createGraphics(w, h);
    cooling = createImage(w, h, RGB);
    SetupImagesComplete = true;
  }
}

void draw() { //<>//
  if (gameMode == 0) {
    textSize(15);
    fill(0);
    rect(width / 2 - 225, height / 2 - 50, 200, 100);
    fill(255);
    text("GameMode 1: Original", width / 2 - 205, height / 2);
    fill(0);
    rect(width / 2 + 0, height / 2 - 50, 200, 100);
    fill(255);
    text("GameMode 2: Fire", width / 2 + 28, height / 2);
    
    if (overRect(width / 2 - 200, height / 2, 150, 50)) {
     gameMode = 1; 
    }
    
    if (overRect(width / 2 + 50, height / 2, 150, 50)) {
     gameMode = 2; 
    }
  }
  
  if (gameMode != 0) {
    if (SetupImagesComplete != true)
      SetupImages();
      
    if (millis() > Time + 1000) {
     Secs++; 
     Time = millis();
     
      if (ToolQueue > 0) {
        if (ToolQueue > 5) {
          int queue = 5; 
          CreateTools(queue);
          ToolQueue -= queue;
        } else {
          int queue = ToolQueue; 
          CreateTools(queue);
          ToolQueue -= queue;
        }
      }
      
      int WaveFreq = 3;
      if (ToolQueue < ToolList.length - WaveFreq) {
        ToolQueue += (int)random(1, WaveFreq + 1); 
      }   
    } 
    image(Background, 0, 0); 
    ToolStuff();
    if (gameMode == 1)
      ScoringSystem();
    
    if (gameMode == 2) {
      firemode.drawfire();
    }
    
    PlayerMovement();
    Door();
    Win();
    Die();
    
    if (gameMode == 1) {    
      if (Score + (DScore * 8) > 40) {
        Win();
        Reset();
      }
    }
    
    if (gameMode == 2) {
      if (flameScore <= 5) {
       Win();
       Reset();
      }
    }
  }
}

void CreateTools (int Amount) {
  if (Amount <= ToolList.length) {
    int[] LanesAvaliable = {1,2,3,4,5};
    int StartingIndex = 0;
    
    for (int Shuffle = 0; Shuffle < LanesAvaliable.length; Shuffle++) {
      int swap = (int)random(0, LanesAvaliable.length - Shuffle) + Shuffle;
      int temp = LanesAvaliable[swap];
      LanesAvaliable[swap] = LanesAvaliable[Shuffle];
      LanesAvaliable[Shuffle] = temp;
    }
    
    for (int i = 0; i < ToolList.length; i++) {
      if (i % 5 == 0) {
        if (ToolList[i] == null) {
          StartingIndex = i;
          break;
        }
      }
      else {
        continue;
      }
    }
    
    if (StartingIndex <= ToolList.length - 5) {
      for (int amount = StartingIndex; amount < Amount + StartingIndex; amount++) {
        if (ToolList[amount] == null) {
          for (int LanesIndex = 0; LanesIndex < LanesAvaliable.length; LanesIndex++) {
            if (LanesAvaliable[LanesIndex] != 9) {
              ToolList[amount] = new Tool(LanesAvaliable[LanesIndex]);
              LanesAvaliable[LanesIndex] = 9;
              break;
            }
          }
        }
      }
    }
  }
}

void ToolStuff () {
    for (int tools = 0; tools < ToolList.length; tools++) {
    if (ToolList[tools] != null) {     
      ToolList[tools].ToolMove();         
      ToolList[tools].ToolDisplay();
      ToolList[tools].CollisionDetections();     
      if (ToolList[tools] != null) {    
        ToolList[tools].Remove(tools);
      }
    }
  }
}

void PlayerMovement () {
  if (PDead == false) { 
    
    if (PP == 0) {
      image(PlayerImg1, PPS[PP], 225);
    }
    
    if (PP == 1) {
      image(PlayerImg2, PPS[PP], 225);
    }
    
    if (PP == 2) {
      image(PlayerImg3, PPS[PP], 225);
    }
    
    if (PP == 3) {
      image(PlayerImg4, PPS[PP], 225); 
    }
    
    if (PP == 4) {
      image(PlayerImg5, PPS[PP], 225);
    }
    
    if (PP == 5) {
      image(PlayerImg6, PPS[PP], 225);
    }
    
    if (PP == 6) {
    image(PlayerImg7, PPS[PP], 225);
    }
    
  } else {
    image(PlayerDead, PPS[PP] - 20, 250);
  }
}

//  this is the function to rule all them all
void ScoringSystem () {
  //int i for rows
  //int j for columns
    for (int i = 0; i < DScore; i++) { //<>//
      for (int j = 0; j < 8; j++) {
        noStroke();
        int colour = 255;
        int a = 0;
        for (int s = 50; s > 0; s-= 3) {
          fill(colour, 0, 0, a);
          ellipse(50 + j * 60 , 30 + i * 60, s, s);   
          //colour-= 17;
          a+=17;
        }
      }
    }
    int count = 0;
    for (int i = DScore; i < 5; i++) {
      for (int j = 0; j < 8; j++) {
          if (count < Score) {
            noStroke();
            int colour = 0;
            int a = 0;
            for (int s = 50; s > 0; s-= 3) {
              fill(colour, a);
              ellipse(50 + j * 60 , 30 + i * 60, s, s);   
              //colour-= 17;
              a+=17;
            }    
          }
        count++;
      } 
    }
}

class Tool {
  int Lane;
  int yPos = 0;
  int PSecs = 0;

  Tool(int lane) {
    Lane = lane; 
  }
  
  void ToolMove () {
    if (PDead == false) {
      if (Secs == PSecs + 1 && yPos < 4) {
        yPos++;
      }
    }
  }
  
  void ToolDisplay () {
      if (Lane == 1) { 
        image(Tool1, PPS[Lane], ToolPS[yPos]);
      }
      
      if (Lane == 2) { 
        image(Tool2, PPS[Lane], ToolPS[yPos]);
      }
      
      if (Lane == 3) { 
        image(Tool3, PPS[Lane], ToolPS[yPos]);
      }
      
      if (Lane == 4) { 
        image(Tool4, PPS[Lane], ToolPS[yPos]);
      }
      
      if (Lane == 5) { 
        image(Tool5, PPS[Lane], ToolPS[yPos]);
      }
    PSecs = Secs; 
  }
  
  void CollisionDetections () {
    if (yPos == 4 && Lane == PP) {
      PDead = true;
      DScore++;
      if (flameScore >= 40)
        flameScore -= 50;
      DPSecs = Secs;
    }
  }

  void Remove (int index) {
   if (yPos == 4) { 
     PScore = Score;
     Score++;
     if (flameScore >= 5)
       flameScore -= 5;
     
     yPos = 0;
     ToolList[index] = null;
   }
  }
}


void keyPressed() {  
  if (PDead == false) {
    if (CanPress == true) {
      if (PP < 6) {
        if (PP == 5) {
          if (DoorOpen == true) {
            if (keyCode == 39) {
               PP++;
               WPSecs = Secs;
               CanPress = false;
            }             
          }
        } else {
          if (keyCode == 39) {
             PP++;
             CanPress = false;
          } 
        }
      }  
      if (PP > 0) {
        if (keyCode == 37) {
           PP--;
           CanPress = false;
        }
      } 
    }
  }
}

void keyReleased() {
  if (keyCode == 39 || keyCode == 37) {
    CanPress = true;
  }
}

int DRPSecs = 0;
void Door () {
  if (Secs >= DRPSecs + rand) {
    if (DoorOpen) {
      DoorOpen = false;
      DRPSecs = Secs;
      rand = (int)random(3, 9);
      return;
    }
    
    if (!DoorOpen) {
     DoorOpen = true; 
     DRPSecs = Secs;
     rand = (int)random(3, 9);
     return;
    }
  }
  
  if (!DoorOpen) {
    image(DoorImgClosed, 424, 230);
  }
  
  if (DoorOpen) {
    image(DoorImgOpen, 470, 220); 
  }
}

int WPSecs = 0;
void Win () {
  if (PP == 6) {
    CanPress = false;
    if (Secs >= WPSecs + 0.5) {
    print(" You Win! ");
    Score+=3;
    if (flameScore < 300)
      flameScore += 50;
    //println(Score);
    PP = 0; 
    DoorOpen = false;
    CanPress = true;
    }
  }
}

int DPSecs = 0;
void Die () {
  if (PDead == true) {
    if (Secs >= DPSecs + 3) {
      PP = 0;
      PDead = false;  
      DoorOpen = true;
      
      if (Score > 0) {
        Score = PScore;  
      }
      print (" Died! ");
    }
  }
}

void Reset() {
  for (int i = 0; i < ToolList.length; i++) {
    ToolList[i] = null;
  }
  Score = 0;
  DScore = 0;
  flameScore = 300;
  print(" Reset! ");
}