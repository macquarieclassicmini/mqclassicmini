boolean overRect(int x, int y, int width, int height)  {
  if (mouseX >= x && mouseX <= x+width && 
      mouseY >= y && mouseY <= y+height && mousePressed) {
    return true;
  } else {
    return false;
  }
}

//fire algorithm
//https://web.archive.org/web/20160418004150/http://freespace.virgin.net/hugo.elias/models/m_fire.htm

PGraphics buffer1;
PGraphics buffer2;
PImage cooling;
int w = 512;
int h = 348;
float ystart = 0.0;
float flameScore = 300;    //higher value = smaller flame; lower value = bigger flame;

class FireMode {
void cool() {
  cooling.loadPixels();
  float xoff = 0.0; // Start xoff at 0
  float increment = 0.01;
  for (int x = 0; x < w; x++) {
    xoff += increment; 
    float yoff = ystart;
    for (int y = 0; y < h; y++) {
      yoff += increment; 

      float coolingYOffset = map(y, 0, 348, h, 0);
      float n = noise(xoff, yoff);
      float bright = pow(n, 4) * 255 + coolingYOffset;

      //black is where fire can be
      //white is where the fire is cooled
      
      cooling.pixels[x+y*w] = color(bright);
    }
  }

  cooling.updatePixels();
  ystart += increment;
}

void fire(int rows) {
  buffer1.beginDraw();
  buffer1.loadPixels();
  for (int x = 0; x < w; x++) {
    //buffer1.pixels[x] = color(255);
    for (int j = 0; j < rows; j++) {
      int y = h-(j+1);
      int index = x + y * w;
      buffer1.pixels[index] = color(255,100,0,0);
    }
  }
  buffer1.updatePixels();
  buffer1.endDraw();
}

float[] greyToColour(float greyScale){
  float RGBValues[] = new float[4];
  float scale = greyScale;
  float mapped = map(scale, 0, 255, 0, 200);
  
  float r = 255;
  float g = mapped;
  float b = 0;
  float a = mapped * 1.5;
  
  RGBValues[0] = r;
  RGBValues[1] = g;
  RGBValues[2] = b;
  RGBValues[3] = a;
  
  return RGBValues;
}

void drawfire() {
  fire(1);
  cool();
  buffer2.beginDraw();
  buffer1.loadPixels();
  buffer2.loadPixels();
  for (int x = 1; x < w-1; x++) {
    for (int y = 1; y < h-1; y++) {
      int index0 = (x) + (y) * w;
      int index1 = (x+1) + (y) * w;
      int index2 = (x-1) + (y) * w;
      int index3 = (x) + (y+1) * w;
      int index4 = (x) + (y-1) * w;
      color c1 = buffer1.pixels[index1];
      color c2 = buffer1.pixels[index2];
      color c3 = buffer1.pixels[index3];
      color c4 = buffer1.pixels[index4];

      color c5 = cooling.pixels[index0];
      float newC = brightness(c1) + brightness(c2)+ brightness(c3) + brightness(c4);
      float scl = map(flameScore, 348.0, 0.0, 0.0, 0.45);
      newC = newC * scl - brightness(c5);

      float RGBValues[] = greyToColour(newC);
      buffer2.pixels[index4] = color(RGBValues[0], RGBValues[1], RGBValues[2], RGBValues[3]);
    }
  }
  buffer2.updatePixels();
  buffer2.endDraw();

  // Swap
  PGraphics temp = buffer1;
  buffer1 = buffer2;
  buffer2 = temp;

  pushMatrix();
  scale(1.0, -1.0);
  translate(-1, -h);
  image(buffer2, 0, 0);
  popMatrix();
}
}