Addons:
 - main menu
  - original game
  - fire game mode

I added a main menu where the original game can be found. The main menu also
 has a fire game mode.
The fire game mode uses a system similar to the original scoring system but
 instead uses a graphical simulation of fire to slowly cover the screen and
 represent the players score.
The fire changes colours with the centre of the flame being brightest,
 becoming more red the further the flame travels outwards. The flame also (kinda)
 flicks like a flame but im still only a COMP 115 student, not a graphics designer.


The game resets when the flame covers the screen. The flame grows when the player
 dies or, unlike the original game, a tool reaches the ground to make the game harder.
 The flame can shrink when the player reaches the end of the level (the door). 