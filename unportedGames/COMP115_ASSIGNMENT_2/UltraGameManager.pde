static float GROUND_SCROLL_SPEED = -2.2;
static float RESPAWN_X = 64;
static float RESPAWN_Y = 128;

collisionManager cm;
toolClass t;
ArrayList<toolClass> allTools;

int cameraX = 0;

private button returnToMenuKey;

class ultraGameManager implements gameManager {    
    
  playerClass player;
  
  int stageLength = 0;
  
  int attempts = 0;
  
  int vspeed = 0;
  int hspeed = 0;
  
  int worldSpeed = -1;
  
  ultraGameManager() {
    
    returnToMenuKey = input.defineButton('m');
    
    cm = new collisionManager();
    
    player = new playerClass(RESPAWN_X,RESPAWN_Y);
    //t = new toolClass(100,height+32);
    
    int xx = -256;
    int yy = height-48;
    
    allTools = new ArrayList<toolClass>();
    
    int ww = 512;
    
    for(int i = 0; i < 5; i++) {
     
      yy = height-48-randomInt(-24,24);
      cm.newCollider(xx,yy,ww,128,"World");
      xx += ww;
      
      int gap = randomInt(128,162); 
      
      allTools.add(new toolClass(xx+gap/2,height+24));
      
      xx += gap;
      ww = randomInt(32,256);
      }
      
    stageLength = xx-ww;

    }
  
  void update() {
    
    player.update();
    
    cameraX = max(0,(int)((cameraX*6)+((player.x+(PLAYER_WIDTH/2)-width/2)*2))/8);
    
    for(toolClass t : allTools) {
      t.update();
      }
    
    if (cm.collisionAtPos(player.bbox,"Tools")) {playerDie();}
    if (player.y > height+8) {playerDie();}
    
    };
  void render() {

    background(color(#b9bffb));
    
    noFill();

    stroke(255,0,0);
    
    translate(-cameraX,0);
    
    player.render();
    
    ArrayList<collider> gnd = cm.collidersInLayer("World");
    for(collider c : gnd) {
      /* dont draw platforms that are outside of the screen */
      if (c.x2 < cameraX || c.x > cameraX+width) {continue;}
      else {drawPlatform(c);} 
      }
      
      
    for(toolClass t : allTools) {
      t.render();
      }
      
    
    
    
    
    translate(cameraX,0);
    noStroke();
    fill(color(#849be4));
    
    textAlign(CENTER,CENTER);
    text("ATTEMPTS: " + str(attempts),width/2,16);    
    
    /* draw the progress meter */
    stroke(0);
    noFill();
    line(32,32,width-32,32);
    line(32+((width-64)*((float)player.x/stageLength)) ,16,32+((width-64)*((float)player.x/stageLength)),48);
    noStroke();
    fill(0);
    
    rect(32,28,((width-64)*((float)player.x/stageLength)),9);
    
    if (player.x>stageLength-256) {
      noStroke();
      fill(0);
      text("YOU WIN!",width/2,height/2);
      }
      
    };
    
  void handleInputs() {
    player.handleKeypressActions();
    if (returnToMenuKey.pressed) {changeGameManager(new menuGameManager());return;}
    };
  
  void cleanup() {
    /* i'm trying the GC a bit too much here because i'm too tired to finish */
    cm = null;
    allTools = null;
    player = null;
    };
  
  void playerDie() {
    player.x = RESPAWN_X;
    player.y = RESPAWN_Y;
    
    player.bbox.setPosition(RESPAWN_X,RESPAWN_Y);
    attempts++;
    
    }
}

void drawPlatform(collider c) {
  noStroke();
  fill(color(#423934));
  
  rect(c.x,c.y,c.w,c.h);
  
  for(int i = 0; i <= c.w; i++) {
    float b = sin(c.x+(i));
    stroke(lerpColor(color(#59c135),color(#1a7a3e),(b+1)/2));
    line(c.x+i,c.y,c.x+i,c.y+16+(8*b));
    }
  }

  
  