static boolean collidersIntersect(collider a, collider b) {
  if (a.x2 <= b.x || a.x >= b.x2 || a.y2 <= b.y || a.y >= b.y2) {
    return false;}
  else {return true;}
  }
  


class collisionManager {
  /* colliders are separated into layers so that i can filter out colliders i'm not looking for, e.g. only iterate through ground colliders*/
  private HashMap<String,ArrayList> collisionLayers;
  ArrayList<collider> everythingLayer;
  
  collisionManager() {
    collisionLayers = new HashMap<String,ArrayList>();
    collisionLayers.put("Everything",new ArrayList<collider>()); /*the list of ALL colliders*/
    everythingLayer = collisionLayers.get("Everything"); /* for convenience */
    }
  
  
  /* create a collider and add it to a layer */
  collider newCollider(float x, float y, float w, float h, String layer) {
    collider c = new collider(x,y,w,h);
    everythingLayer.add(c); /* all colliders go in the everything layer*/
    if (layer != "Everything" && layer != "") {
      if (collisionLayers.containsKey(layer)) {
        collisionLayers.get(layer).add(c); /*add it to its respective layer list*/
      } else {
        collisionLayers.put(layer,new ArrayList<collider>()); /* if the layer doesn't exist, make it */
        collisionLayers.get(layer).add(c); 
        }
      }
    return c;
    }
    
  /* for iteration */
  ArrayList collidersInLayer(String layer) {
    if (collisionLayers.containsKey(layer)) {
      return collisionLayers.get(layer);
      }
      
    /* the fallback */
    println("THE LAYER '" + layer + "' DOESN'T EXIST");
    return everythingLayer;
    }
  
  /* checks for any collision with one collider and all other colliders in a layer */
  boolean collisionAtPos(collider c, String layer) {
    if (!collisionLayers.containsKey(layer)) {
      println("THE LAYER '" + layer + "' DOESN'T EXIST");
      return false;
      }
    
    ArrayList<collider> colliders = collidersInLayer(layer);
    for(collider col : colliders) {
      if (collidersIntersect(c, col)) {
        return true;
        }
      }
    return false;
    }
  
  }


class collider {
  public float x, y, w, h;
  public float x2, y2;
  
  collider(float x, float y, float w, float h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.x2 = x+w;
    this.y2 = y+h;
    }
        
  /* relative movement*/
  void move(float relx, float rely) {
    x += relx;
    y += rely;
    x2 = x+w;
    y2 = y+h;
    }
    
  /* absolute movement*/
  void setPosition(float x, float y) {
    this.x = x;
    this.y = y;
    x2 = x+w;
    y2 = y+h;
    }
    
  /* generates a new collider based on the current one at a new location, usefull for predictive collision checking */
  collider atOffset(float ox, float oy) {
    return new collider(x+ox,y+oy,w,h);
    }
    
  void render() {
    rect(x,y,w,h);
    }
  
  }