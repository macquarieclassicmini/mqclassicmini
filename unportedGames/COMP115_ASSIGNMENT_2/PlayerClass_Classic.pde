PShape currentFrame;
PShape leftFrame; 
PShape rightFrame;

class classicPlayerClass {
  
  private int movementStepSize = 60;
  
  public float x,y;
  public int playerSlot;
  private float startX;
  
  classicPlayerClass(float x, float y) {
    this.x = x;
    this.y = y;
    playerSlot = 0;
    startX = x;
    
    
    
    currentFrame = leftFrame;
    }
  
  
  public void moveForward() {
    x += movementStepSize;
    swapFrames();
    playerSlot++;
    }
    
  public void moveBackward() {
    x -= movementStepSize;
    swapFrames();
    playerSlot--;
    }
    
  public void resetPosition() {
    playerSlot = STARTSLOT;
    x = startX;
    }
  
  public void renderPlayer() {
    shape(currentFrame,x,y);
  }
    
  public void swapFrames() {
    if (currentFrame == leftFrame) {currentFrame = rightFrame;}
    else {currentFrame = leftFrame;}
    }
   
  
  }
  
/* I wrote a small program that let me generate PShape vertex code by clicking on points on an image, so i didn't have to write it all by hand, which is why this code looks the way it does */
private void initialiseLeftFrame() {
  leftFrame.beginShape();
  
  leftFrame.vertex(21.0,65.0);
  leftFrame.vertex(31.0,66.0);
  leftFrame.vertex(31.0,64.0);
  leftFrame.vertex(26.0,61.0);
  leftFrame.vertex(26.0,50.0);
  leftFrame.vertex(29.0,47.0);
  leftFrame.vertex(28.0,41.0);
  leftFrame.vertex(32.0,39.0);
  leftFrame.vertex(36.0,41.0);
  leftFrame.vertex(39.0,45.0);
  leftFrame.vertex(44.0,40.0);
  leftFrame.vertex(44.0,37.0);
  leftFrame.vertex(41.0,38.0);
  leftFrame.vertex(40.0,39.0);
  leftFrame.vertex(35.0,35.0);
  leftFrame.vertex(28.0,35.0);
  leftFrame.vertex(26.0,32.0);
  leftFrame.vertex(31.0,31.0);
  leftFrame.vertex(31.0,29.0);
  leftFrame.vertex(29.0,29.0);
  leftFrame.vertex(26.0,26.0);
  leftFrame.vertex(26.0,23.0);
  leftFrame.vertex(29.0,20.0);
  leftFrame.vertex(33.0,20.0);
  leftFrame.vertex(36.0,23.0);
  leftFrame.vertex(38.0,25.0);
  leftFrame.vertex(41.0,20.0);
  leftFrame.vertex(44.0,20.0);
  leftFrame.vertex(44.0,17.0);
  leftFrame.vertex(41.0,14.0);
  leftFrame.vertex(40.0,13.0);
  leftFrame.vertex(44.0,10.0);
  leftFrame.vertex(44.0,7.0);
  leftFrame.vertex(38.0,8.0);
  leftFrame.vertex(36.0,5.0);
  leftFrame.vertex(32.0,1.0);
  leftFrame.vertex(24.0,0.0);
  leftFrame.vertex(19.0,3.0);
  leftFrame.vertex(14.0,8.0);
  leftFrame.vertex(11.0,14.0);
  leftFrame.vertex(13.0,21.0);
  leftFrame.vertex(17.0,26.0);
  leftFrame.vertex(23.0,31.0);
  leftFrame.vertex(20.0,32.0);
  leftFrame.vertex(16.0,32.0);
  leftFrame.vertex(14.0,31.0);
  leftFrame.vertex(10.0,32.0);
  leftFrame.vertex(7.0,31.0);
  leftFrame.vertex(7.0,35.0);
  leftFrame.vertex(10.0,38.0);
  leftFrame.vertex(12.0,35.0);
  leftFrame.vertex(15.0,35.0);
  leftFrame.vertex(18.0,37.0);
  leftFrame.vertex(18.0,40.0);
  leftFrame.vertex(13.0,47.0);
  leftFrame.vertex(10.0,46.0);
  leftFrame.vertex(5.0,48.0);
  leftFrame.vertex(3.0,46.0);
  leftFrame.vertex(1.0,44.0);
  leftFrame.vertex(0.0,48.0);
  leftFrame.vertex(3.0,52.0);
  leftFrame.vertex(6.0,53.0);
  leftFrame.vertex(8.0,51.0);
  leftFrame.vertex(11.0,50.0);
  leftFrame.vertex(14.0,50.0);
  leftFrame.vertex(17.0,51.0);
  leftFrame.vertex(20.0,52.0);
  leftFrame.vertex(23.0,56.0);
  leftFrame.vertex(23.0,59.0);
  leftFrame.vertex(20.0,65.0);
  leftFrame.endShape();
  
  leftFrame.setStroke(false);
  leftFrame.setFill(0);
  }
   
  private void initialiseRightFrame() {
    rightFrame.beginShape();
    rightFrame.vertex(14.0,64.0);
    rightFrame.vertex(21.0,64.0);
    rightFrame.vertex(25.0,63.0);
    rightFrame.vertex(23.0,61.0);
    rightFrame.vertex(23.0,58.0);
    rightFrame.vertex(22.0,54.0);
    rightFrame.vertex(25.0,50.0);
    rightFrame.vertex(32.0,48.0);
    rightFrame.vertex(38.0,50.0);
    rightFrame.vertex(40.0,52.0);
    rightFrame.vertex(45.0,46.0);
    rightFrame.vertex(43.0,43.0);
    rightFrame.vertex(40.0,47.0);
    rightFrame.vertex(37.0,45.0);
    rightFrame.vertex(32.0,45.0);
    rightFrame.vertex(26.0,39.0);
    rightFrame.vertex(26.0,34.0);
    rightFrame.vertex(29.0,34.0);
    rightFrame.vertex(32.0,34.0);
    rightFrame.vertex(36.0,37.0);
    rightFrame.vertex(38.0,31.0);
    rightFrame.vertex(35.0,32.0);
    rightFrame.vertex(32.0,31.0);
    rightFrame.vertex(29.0,31.0);
    rightFrame.vertex(25.0,32.0);
    rightFrame.vertex(23.0,30.0);
    rightFrame.vertex(26.0,29.0);
    rightFrame.vertex(22.0,28.0);
    rightFrame.vertex(20.0,25.0);
    rightFrame.vertex(20.0,22.0);
    rightFrame.vertex(23.0,19.0);
    rightFrame.vertex(26.0,19.0);
    rightFrame.vertex(29.0,21.0);
    rightFrame.vertex(31.0,23.0);
    rightFrame.vertex(35.0,20.0);
    rightFrame.vertex(38.0,18.0);
    rightFrame.vertex(38.0,15.0);
    rightFrame.vertex(35.0,13.0);
    rightFrame.vertex(32.0,12.0);
    rightFrame.vertex(38.0,8.0);
    rightFrame.vertex(38.0,7.0);
    rightFrame.vertex(32.0,7.0);
    rightFrame.vertex(29.0,3.0);
    rightFrame.vertex(22.0,0.0);
    rightFrame.vertex(16.0,0.0);
    rightFrame.vertex(10.0,4.0);
    rightFrame.vertex(7.0,10.0);
    rightFrame.vertex(6.0,16.0);
    rightFrame.vertex(9.0,23.0);
    rightFrame.vertex(14.0,27.0);
    rightFrame.vertex(18.0,31.0);
    rightFrame.vertex(16.0,35.0);
    rightFrame.vertex(13.0,35.0);
    rightFrame.vertex(9.0,37.0);
    rightFrame.vertex(5.0,40.0);
    rightFrame.vertex(2.0,37.0);
    rightFrame.vertex(0.0,40.0);
    rightFrame.vertex(6.0,43.0);
    rightFrame.vertex(8.0,40.0);
    rightFrame.vertex(13.0,38.0);
    rightFrame.vertex(17.0,40.0);
    rightFrame.vertex(17.0,47.0);
    rightFrame.vertex(19.0,51.0);
    rightFrame.vertex(18.0,55.0);
    rightFrame.vertex(20.0,60.0);
    rightFrame.vertex(16.0,60.0);
    rightFrame.vertex(14.0,64.0);
    rightFrame.endShape();
    
    rightFrame.setStroke(false);
    rightFrame.setFill(0);
    }