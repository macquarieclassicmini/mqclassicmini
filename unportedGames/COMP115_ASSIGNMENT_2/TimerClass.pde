/* Helper class to allow me to easily keep second interval Timers */

ArrayList<timer> allTimers = new ArrayList<timer>();

class timer {
  float time; /* frames remaining */
  
  /* empty constructor */
  timer() {
    this.time = 0;
    allTimers.add(this);
    }
  
  /* constructor takes time in SECONDS */
  timer(float time) {
    this.time = time*frameRate; /* convert seconds to frames */
    allTimers.add(this); /* add to the tracker */
    }
    
  /* cause Timer to tick down */
  void update() {if (time>0) {time--;}}
  
  /* set Timer to a new time IN SECONDS */
  void setTimer(float newTime) {this.time = newTime*frameRate;}
  
  boolean complete() {
    if (time<=0) {return true;}
    return false;
    }
    
  void destroy() {
    allTimers.remove(this);
    }
  }
  
/* update all existing Timers at once */
void processTimers() {
  timer t;
  for(int i = 0; i<allTimers.size(); i++) {
    t = allTimers.get(i);
    t.update();
    }
  }