/* A small interface that both the old and new game manager can be used through */
interface gameManager {
      
  void update();
  void render();
  void handleInputs();
  void cleanup();
  }