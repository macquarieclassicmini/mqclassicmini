/* A collection of misc helper functions*/

/* slightly shorter than int(random(a,b+1)), and all inclusive */
int randomInt(int a, int b) {
  return int(random(a,b+1));
  }
  
/* randomly returns one of the provided arguments, very useful for selecting from set options */
<Type> Type choose(Type ...args) {
  return args[randomInt(0,args.length-1)];
  }
  
/* does what it says on the tin :/ */
void gradientCircle(float x, float y, float r, color c) {
  final int divisions = 10;
  final float separation = r/divisions;  
  noStroke();
  for(float i = 0.0; i < divisions; i++) {
    //redefine the input colour with an alpha component for stacking circles
    fill(color(red(c),blue(c),green(c),(255*((1+i)/divisions))));
    ellipse(x,y,r-(separation*i),r-(separation*i));
    }
  }
  
/* trig in degrees because radians are inconvenient */
float dsin(float degrees) {
  return sin(radians(degrees));
  }
  
float dcos(float degrees) {
  return cos(radians(degrees));
  }
  
/* from value, approach target in intervals of step without overshooting */
float approach(float value, float target, float step) {
  if (value > target) {return max(value-step,target);}
  else if (value < target) {return min(value+step,target);}
  else {return target;}
  }
  
int approach(int value, int target, int step) {
  if (value > target) {return max(value-step,target);}
  else if (value < target) {return min(value+step,target);}
  else {return target;}
  }