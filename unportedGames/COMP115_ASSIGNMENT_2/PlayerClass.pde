static float PLAYER_WIDTH = 44;
static float PLAYER_HEIGHT = 65;
static float PLAYER_MAX_SPEED = 5;
static float PLAYER_ACCEL = 0.5;
static float GROUND_FRICTION = 0.3;

class playerClass {
  public float x, y;
  private float subX = 0; /* for storing the subpixel offsets  */
  private float subY = 0;
  public float xSpeed = 0;
  public float ySpeed = 0;
  public collider bbox; /* players bounding box collider */
  
  private PShape currentFrame = leftFrame;
  
  private float maxSwaptimer = 30;
  private float swapTimer = maxSwaptimer;
  
  private boolean onGround = false;
  
  private button leftKey, rightKey, jumpKey;
  
  playerClass(float x, float y) {
    this.x = x;
    this.y = y;
    this.bbox = cm.newCollider(x,y,PLAYER_WIDTH,PLAYER_HEIGHT,"Player");
    
    leftKey = input.defineButton(LEFT);
    rightKey = input.defineButton(RIGHT);
    jumpKey = input.defineButton(UP);
    
    }
    
  void update() {
    
    if (leftKey.held) {xSpeed = approach(xSpeed,-PLAYER_MAX_SPEED,PLAYER_ACCEL);}
    else if (rightKey.held) {xSpeed = approach(xSpeed,PLAYER_MAX_SPEED,PLAYER_ACCEL);}
    else {xSpeed = approach(xSpeed,0,GROUND_FRICTION);};
    
    if (xSpeed != 0) {
      swapTimer -= max(abs(xSpeed),1);
      }
      
    if (swapTimer <= 0) {
      swapTimer = maxSwaptimer;
      if (currentFrame == leftFrame) {currentFrame = rightFrame;}
      else {currentFrame = leftFrame;}
      }
    
    if (cm.collisionAtPos(bbox.atOffset(0,1),"World")) {
      onGround = true;
      } else {
      onGround = false;
      ySpeed += 0.5;
      }
      
    handlePlayerPhysics();
    }
    
  void render() {
    shape(currentFrame,x,y);
    }  
  
  void handleKeypressActions() {
    if (onGround && jumpKey.pressed) {
      ySpeed = -8;
      }
    }
  
  void handlePlayerPhysics() {
    /* calculate temp speeds for movement and update subpixel offsets */
    subX += xSpeed;
    subY += ySpeed;
    int tempXSpeed = round(subX);
    int tempYSpeed = round(subY);
    subX -= tempXSpeed;
    subY -= tempYSpeed;
       
    
       
    /* collision and movement handling on the y axis */
    int sign = Integer.signum(tempYSpeed);
    while(tempYSpeed!=0) {
      if (!cm.collisionAtPos(bbox.atOffset(0,sign),"World")) {
        move(0,sign);
        tempYSpeed -= sign;
        }
      else {
        ySpeed = 0;
        break;
        }
      }
      
    /* collision and movement handling on the x axis */
    sign = Integer.signum(tempXSpeed);
    while(tempXSpeed!=0) {
      if (!cm.collisionAtPos(bbox.atOffset(sign,0),"World")) {
        move(sign,0);
        tempXSpeed -= sign;
        }
      else {
        xSpeed = 0;
        break;
        }
      }

    }
  
  /* moving the player and the bounding box in one action */
  void move(float nx, float ny) {
    x+=nx;
    y+=ny;
    bbox.move(nx,ny);
    }
  
  }