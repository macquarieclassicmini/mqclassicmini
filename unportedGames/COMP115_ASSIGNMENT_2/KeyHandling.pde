/*
  I made my own system for handling keyboard inputs because processing's default system doesn't handle holding keys well and only processes one key at a time;
*/

class inputManager {
  public ArrayList<button> allButtons;
  
  inputManager() {
    allButtons = new ArrayList<button>();
    }
    
  /* overloaded function to automaticially determine the type of button required */
  button defineButton(char c) {
    charButton b = new charButton(c);
    allButtons.add(b);
    return b;
    }
    
   button defineButton(int t) {
    keyButton b = new keyButton(t);
    allButtons.add(b);
    return b;
    }
    
  /* for cleanup */
  void delButton(button b) {
    allButtons.remove(b);
    b = null;
    }
    
  /* update all keys each time the keyPressed and keyReleased events occur */
  void checkKeyPressed() {
    for(button b : allButtons) {b.press();}
    }
    
  void checkKeyReleased() {
    for(button b : allButtons) {b.release();}
    }
    
    
  /* used to make sure that after one frame keys are only HELD, not PRESSED */
  void unpressKeys() {
    for(button b : allButtons) {b.pressed = false;}
    }
  }
  
  
  
/* 
    An abstract button class to contain both key and keyCode based buttons 
*/
abstract class button {
  public boolean pressed = false;
  public boolean held = false;
  
  abstract void press();
  abstract void release();
  }
  
class charButton extends button {
  private char trigger;
  
  charButton(char c) {
    trigger = c;
    }
  
  void press() {
    if (key == trigger) {
      if (!held) {pressed = true;} /* prevent pressed event from triggering twice*/ 
      held = true;
      }
    }
    
  void release() {
    if (key == trigger) {held = false;}
    }
  }
  
class keyButton extends button {
  private int trigger;
  
  keyButton(int k) {
    trigger = k;
    }
  
  void press() {
    if (key==CODED && keyCode == trigger) {
      if (!held) {pressed = true;} /* prevent pressed event from triggering twice in one hold*/ 
      held = true;
      }
    }
    
  void release() {
    if (key==CODED && keyCode == trigger) {held = false;}
    }
  }