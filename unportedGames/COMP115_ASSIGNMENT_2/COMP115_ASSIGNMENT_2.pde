/*
    CHLOE HARRIS - 45417631
    
     COMP115 ASSIGNMENT 2
*/

PFont scoreFont;

inputManager input;
gameManager gm;

/* a global function to allow game managers to swap to eachother*/
void changeGameManager(gameManager newManager) {
  gm.cleanup(); /* make the old game manager clear up its resources before changing */
  gm = newManager;
  }

void setup() { 
  frameRate(60);
  size(512,348);

  /* prime scenery from backgroundRender, this only needs to be done once */
  initialiseScenery();
  leftFrame = createShape();
  initialiseLeftFrame();
  rightFrame = createShape();
  initialiseRightFrame();
  
  input = new inputManager();
  
  gm = new menuGameManager();
  
  }
  
void draw() {
  gm.update();
  gm.render();
  }
    
void keyPressed() {
  input.checkKeyPressed(); /* the inputManager needs to be updated before games process their logic */
  gm.handleInputs();
  input.unpressKeys();
  }
  
void keyReleased() {
  input.checkKeyReleased();
  }
    

  
  

  
  
  