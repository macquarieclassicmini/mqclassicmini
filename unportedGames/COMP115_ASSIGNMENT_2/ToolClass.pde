static int MIN_SPEED = -8;
static int MAX_SPEED = -16;

class toolClass {
  public float x,y;
  private PShape sprite;
  private float ySpeed;
  
  private float rotation; 
  private float rotationDir;
  
  public collider bbox;
  
  toolClass(float x,float y) {
    this.x = x;
    this.y = y;
    
    ySpeed = randomInt(MAX_SPEED,MIN_SPEED);
    
    sprite = createShape(); /* temporary assignment */
    
    /* randomly become one of the 5 tools */
    switch(randomInt(1,5)) {
      case 1: {sprite = createBucketSprite();}break;
      case 2: {sprite = createWrenchSprite();}break;
      case 3: {sprite = createScrewdriverSprite();}break;
      case 4: {sprite = createHammerSprite();}break;
      case 5: {sprite = createPlierSprite();}break;
      }
      
    bbox = cm.newCollider(x-24,y-24,32,32,"Tools");
    }
    
  void update() {
    sprite.rotate(radians(abs(max(2,ySpeed)*2)));
    ySpeed+=0.3;
    y += ySpeed;
    bbox.move(0,ySpeed);
    
    if (y > height+150) {ySpeed = randomInt(MAX_SPEED,MIN_SPEED);y=height+24;bbox.setPosition(x-24,y-24);}
    }
  
  void render() {
    polygonMode();
    shape(sprite,x,y);
    }
  
  }