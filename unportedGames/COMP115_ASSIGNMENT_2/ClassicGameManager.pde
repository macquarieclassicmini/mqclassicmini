/*

  The game manager for the marking criteria version of the game

*/


/* All classic mode constants */
static int STARTSLOT = 0;
static int EXITSLOT = 6;
static int PLAYER_START_X = 58;
static float MIN_TOOL_SPAWN_TIME = 0.5;
static float MAX_TOOL_SPAWN_TIME = 1.0;
static float MIN_DOOR_TIME = 3.0;
static float MAX_DOOR_TIME = 8.0;
static float CIRCLE_SIZE = 58;
static float CIRCLE_COLUMNS = (int)(512/58)+1;
static float CIRCLE_ROWS = (int)348/58;



class classicGameManager implements gameManager {
  public int score = 0;
  public int deaths = 0;
  public classicPlayerClass player;
  private timer toolSpawnTimer; /* seconds till the next tool spawn*/
  private timer doorTimer; /* Timer used to track door opening and closing */
  private boolean doorOpen = false; /* the state of the door */
  
  private ArrayList<classicToolClass> allTools; /* List used to keep track of all the existing tools */
  
  private button returnToMenuKey;
  
  classicGameManager() {
    /* Initialise the game */
    player = new classicPlayerClass(PLAYER_START_X,224);
    allTools = new ArrayList<classicToolClass>();
            
    /* define our two timers */
    doorTimer = new timer(round(random(MIN_DOOR_TIME,MAX_DOOR_TIME)));
    toolSpawnTimer = new timer(random(MIN_TOOL_SPAWN_TIME,MAX_TOOL_SPAWN_TIME));
    
    /* i trust my own code more than processing's */
    returnToMenuKey = input.defineButton('m');
    }
    
  /* ########################
     #   PER FRAME UPDATE   #
     ######################## */
  void update() {
    processTimers();
    if(toolSpawnTimer.complete()){
      spawnTool();
      }
    processTools();
    updateDoor();
    /* If the screen is full of circles */
    if (score+(deaths*CIRCLE_COLUMNS)>(CIRCLE_COLUMNS*CIRCLE_ROWS)) {
      gameReset();
      }
    
    }
    
  /* #######################
     #   RENDER THE GAME   #
     ####################### */
  void render() {
    background(161,171,172);
    stroke(255,255);
    fill(255,255);
    renderBackground(doorOpen);
    player.renderPlayer();
    renderTools();
    drawScoreAndDeathCircles(score,deaths);
    }
  
  /* #####################################
     #                                   #
     #   ONE FUNCTION TO RULE THEM ALL   #
     #    DRAWS CIRCLES TO THE SCREEN    #
     #                                   #
     ##################################### */
  void drawScoreAndDeathCircles(int scr, int dth) {
    /* score and deaths are already accessable in this scope but the marking criteria specifies this function MUST be an INFORMED changer, so here they are as inputs anyway*/
    
    /* Draw the red circles*/
    float redOffset = CIRCLE_SIZE/2;
    for(int i = 0; i<dth; i++) {
      for(int o = 0; o<CIRCLE_COLUMNS; o++) {
          gradientCircle(redOffset+(CIRCLE_SIZE*o),redOffset+(CIRCLE_SIZE*i),CIRCLE_SIZE,color(255,0,0));
        }
      }
      
    /* Draw the black circles*/
    float blackXOffset = CIRCLE_SIZE/2;
    float blackYOffset = (CIRCLE_SIZE/2)+(CIRCLE_SIZE*dth);
    for(int i = 0; i<scr; i++) {
      gradientCircle(blackXOffset+(CIRCLE_SIZE*(i%CIRCLE_COLUMNS)),blackYOffset+(CIRCLE_SIZE*(int)(i/CIRCLE_COLUMNS)),CIRCLE_SIZE,color(0,0,0));
      }
    }  
  
  /* ######################
     #   INPUT HANDLING   #
     ###################### */
  void handleInputs() {
    
    if (returnToMenuKey.pressed) {changeGameManager(new menuGameManager());return;}    
    
    /*all of this code was written before i made inputManager and i don't want to touch this code anymore*/
    if (keyCode==RIGHT) {
      if (doorOpen) {
        if (player.playerSlot<6) {player.moveForward();}
        }
      else if (player.playerSlot<5) {
         player.moveForward();
          }
      }
    else if (keyCode==LEFT) {
      if (player.playerSlot>0) {player.moveBackward();}
      }  
      
    /* press m to return to the main menu */
    
      
    /* quality of life tweak, never make the player wait for the door to close for more than a second */ 
    if (player.playerSlot==EXITSLOT && doorTimer.time>frameRate) {doorTimer.setTimer(1);}
    } 

  /* ##################
     #   UPDATE DOOR  #
     ################## */

  void updateDoor() {
    if(doorTimer.complete()) {
        /* Claim a point and reset if the player was in the door when it closes*/
        if (doorOpen) {
          if (player.playerSlot==EXITSLOT) {
            playerWin();
            }
          }
      doorOpen = !doorOpen;
      doorTimer.setTimer(round(random(MIN_DOOR_TIME,MAX_DOOR_TIME)));
      }
    }
  
  /* ##################
     #   SPAWN TOOLS  #
     ################## */
  void spawnTool() {
    int targetSlot = 1;
    boolean foundSlot = false; /* whether an empty slot has been found */
    boolean clash = false;
 
    while(!foundSlot) {
      clash = false;
      targetSlot = randomInt(1,5);     
      /* iterate through all tools to see if there's one at the top of our target slot*/
      for(classicToolClass tool : allTools) {
        if(tool.slot == targetSlot) {
          if(tool.row == 1) {clash = true; break;}
          }
        }
      /* if no clashes were found, this slot is safe */
      if(!clash) {foundSlot = true;}
      }
    toolSpawnTimer.setTimer(random(MIN_TOOL_SPAWN_TIME,MAX_TOOL_SPAWN_TIME));
    allTools.add(new classicToolClass(targetSlot));
    }
    
  /* ####################
     #   PROCESS TOOLS  #
     #################### */
  void processTools() {
    classicToolClass tool;
    for(int i = allTools.size()-1; i>=0; i--) { /*process tool list backwards*/
      tool = allTools.get(i);   
      tool.updateTool(allTools);
      if (tool.row == 5 && tool.slot == player.playerSlot) { /* confirmed collision, reset game */
        playerDie();
        }
      /* instances cannot null themselves, so this has to be done here */
      if (tool.row > 5) {
        allTimers.remove(tool.dropTimer);
        tool.sprite = null;
        tool = null; allTools.remove(i);
        /* A tool hitting the ground gives us one point */
        score++;
        }
      }
    }
    
  /* ####################
     #   RENDER TOOLS   #
     #################### */
  void renderTools() {
    for(classicToolClass tool : allTools) {tool.renderTool();}
    }  
  
  
  /* ###############################
     #   MISC GAMEPLAY FUNCTIONS   #
     ############################### */
  void playerDie() {
    player.resetPosition();
    deaths++;
    }
    
  void playerWin() {
    player.resetPosition();
    score+=3;
    }
    
  void gameReset() {
    println("Game Over!");
    println("Final score: " + str(score) + " with " + str(deaths) + " deaths!");
    deaths = 0;
    score = 0;
    }

  /* ###############
     #   CLEANUP   #
     ############### */
  void cleanup() {
    /* this MIGHT be entirely pointless because of java's garbage collector, but it doesn't hurt to clean up after youself */
    player = null;
    doorTimer.destroy();
    doorTimer=null;
    toolSpawnTimer.destroy();
    toolSpawnTimer=null;
    
    /* remove all the tool instances*/
    for(int i = allTools.size()-1; i>=0; i--) {
      classicToolClass tool = allTools.get(i);
      tool.destroy();
      tool = null; allTools.remove(i);
      }
    allTools = null;
    
    input.delButton(returnToMenuKey);
    
    }

  }

  









   
   