/*
NOTES:

Tools are 48px wide at maximum

must be drawn at +24 from offset

move downwards in 52px intervals

*/

class classicToolClass {
  final private float TOOl_ROTATIONiNTERVAL = 45.0;
  final private float moveMinTime = 0.3;
  final private float moveMaxTime = 0.8;  
  
  
  public float x, y;  
  public int slot; /* which playerslot this tool is above */
  public timer dropTimer; /* tracks interval for each update */
  private PShape sprite; /* the shape used display the tool */
  
  private float rotation; /* the angle to render the tool at */
  private float rotationDir; /* how the tool will rotate every move */
  
  public int row; /* how many times the tool has gone down*/
  
  classicToolClass(int targetSlot) {
    slot = targetSlot;
    
    /* set x position based on assigned slot*/
    switch(slot) {
      case 1: {x = 116;};break;
      case 2: {x = 176;};break;
      case 3: {x = 236;};break;
      case 4: {x = 296;};break;
      case 5: {x = 356;};break;
      }
    
    y = 4;
    row = 1;
    
    dropTimer = new timer(random(moveMinTime,moveMaxTime));
        
    rotation = choose(0.0, 90.0, 180.0, 270.0);
    rotationDir = choose(TOOl_ROTATIONiNTERVAL, -TOOl_ROTATIONiNTERVAL);
    
    sprite = createShape(); /* temporary assignment */
    
    /* randomly become one of the 5 tools */
    switch(randomInt(1,5)) {
      case 1: {sprite = createBucketSprite();}break;
      case 2: {sprite = createWrenchSprite();}break;
      case 3: {sprite = createScrewdriverSprite();}break;
      case 4: {sprite = createHammerSprite();}break;
      case 5: {sprite = createPlierSprite();}break;
      }
    
    sprite.rotate(rotation);
    sprite.scale(choose(-1,1),choose(-1,1)); /* just so every shape doesn't look EXACTLY the same */
    }
    
  void updateTool(ArrayList<classicToolClass> allTools) {
    if (dropTimer.complete()) {
      
      boolean clash = false; /* whether or not there's another tool in the same slot */
      
      /* iterate through all other tools to check if there's one in this slot and if its below us*/
      for(classicToolClass other : allTools) {
        if(other.slot != this.slot) {continue;} /* ignore tools in other rows */
        if(other.row == this.row+1) {clash = true; break;}
        }
        
      if (!clash) { /* if there's no clash, move*/
        y += 52;
        row++;
        sprite.rotate(radians(rotationDir));
        }
        
      /*regardless of a clash, start the wait timer again*/
      dropTimer.setTimer(random(moveMinTime,moveMaxTime));

      }
    
    }
  
  void renderTool() {
    polygonMode();
    shape(sprite,x+24,y+24);
    }
    
  void destroy() {
    this.sprite = null;
    this.dropTimer.destroy();
    }
  
  }
  
/* ###########################
   All the vertex data below this point was generated using my plotting tool, i've tried to lay it out nicely but its better left ignored...
   ########################### */
  
PShape createBucketSprite() {
  PShape bucketShape = createShape();
  
  bucketShape.beginShape();
  bucketShape.vertex(4.0,-16.0);
  bucketShape.vertex(11.0,-16.0);
  bucketShape.vertex(16.0,-12.0);
  bucketShape.vertex(20.0,-6.0);
  bucketShape.vertex(21.0,0.0);
  bucketShape.vertex(19.0,7.0);
  bucketShape.vertex(15.0,12.0);
  bucketShape.vertex(9.0,12.0);
  bucketShape.vertex(-12.0,11.0);
  bucketShape.vertex(-14.0,-8.0);
  bucketShape.vertex(4.0,-16.0);
  
  bucketShape.beginContour();
  bucketShape.vertex(10.0,-12.0);
  bucketShape.vertex(7.0,-11.0);
  bucketShape.vertex(8.0,9.0);
  bucketShape.vertex(13.0,8.0);
  bucketShape.vertex(14.0,5.0);
  bucketShape.vertex(16.0,-1.0);
  bucketShape.vertex(15.0,-7.0);
  bucketShape.vertex(10.0,-12.0);
  bucketShape.endContour();
  
  bucketShape.endShape();

  
  bucketShape.setStroke(false);
  bucketShape.setFill(0);
  
  return bucketShape;
  
  }
  
PShape createWrenchSprite() {
  PShape wrenchShape = createShape();  

  wrenchShape.beginShape();
  wrenchShape.vertex(-5.0,0.0);
  wrenchShape.vertex(-1.0,-5.0);
  wrenchShape.vertex(-3.0,-9.0);
  wrenchShape.vertex(-5.0,-13.0);
  wrenchShape.vertex(-4.0,-18.0);
  wrenchShape.vertex(1.0,-21.0);
  wrenchShape.vertex(4.0,-20.0);
  wrenchShape.vertex(3.0,-13.0);
  wrenchShape.vertex(5.0,-12.0);
  wrenchShape.vertex(10.0,-16.0);
  wrenchShape.vertex(12.0,-12.0);
  wrenchShape.vertex(12.0,-8.0);
  wrenchShape.vertex(10.0,-3.0);
  wrenchShape.vertex(7.0,-2.0);
  wrenchShape.vertex(4.0,-2.0);
  wrenchShape.vertex(1.0,5.0);
  wrenchShape.vertex(4.0,9.0);
  wrenchShape.vertex(4.0,15.0);
  wrenchShape.vertex(1.0,19.0);
  wrenchShape.vertex(-3.0,19.0);
  wrenchShape.vertex(-6.0,17.0);
  wrenchShape.vertex(-4.0,12.0);
  wrenchShape.vertex(-6.0,9.0);
  wrenchShape.vertex(-9.0,13.0);
  wrenchShape.vertex(-12.0,12.0);
  wrenchShape.vertex(-13.0,6.0);
  wrenchShape.vertex(-11.0,3.0);
  wrenchShape.vertex(-7.0,0.0);
  wrenchShape.vertex(-5.0,0.0);
  wrenchShape.endShape();

  wrenchShape.setStroke(false);
  wrenchShape.setFill(0);
  
  return wrenchShape;
}

PShape createScrewdriverSprite() {
  PShape screwdriverShape = createShape();
  
  screwdriverShape.beginShape();
  screwdriverShape.vertex(4.0,-3.0);
  screwdriverShape.vertex(23.0,2.0);
  screwdriverShape.vertex(24.0,11.0);
  screwdriverShape.vertex(20.0,16.0);
  screwdriverShape.vertex(1.0,6.0);
  screwdriverShape.vertex(-1.0,2.0);
  screwdriverShape.vertex(-13.0,-2.0);
  screwdriverShape.vertex(-16.0,1.0);
  screwdriverShape.vertex(-22.0,-4.0);
  screwdriverShape.vertex(-20.0,-9.0);
  screwdriverShape.vertex(-12.0,-10.0);
  screwdriverShape.vertex(-12.0,-5.0);
  screwdriverShape.vertex(0.0,-2.0);
  screwdriverShape.endShape();
  
  screwdriverShape.setStroke(false);
  screwdriverShape.setFill(0);
  
  return screwdriverShape;
  }
  
PShape createHammerSprite() {
  PShape hammerShape = createShape();
  
  hammerShape.beginShape();
  hammerShape.vertex(15.0,-10.0);
  hammerShape.vertex(-3.0,11.0);
  hammerShape.vertex(-6.0,10.0);
  hammerShape.vertex(-9.0,12.0);
  hammerShape.vertex(-3.0,23.0);
  hammerShape.vertex(-9.0,28.0);
  hammerShape.vertex(-15.0,23.0);
  hammerShape.vertex(-18.0,17.0);
  hammerShape.vertex(-21.0,11.0);
  hammerShape.vertex(-20.0,5.0);
  hammerShape.vertex(-18.0,-1.0);
  hammerShape.vertex(-15.0,-1.0);
  hammerShape.vertex(-15.0,5.0);
  hammerShape.vertex(-14.0,8.0);
  hammerShape.vertex(-10.0,10.0);
  hammerShape.vertex(-8.0,6.0);
  hammerShape.vertex(-8.0,3.0);
  hammerShape.vertex(10.0,-15.0);
  hammerShape.vertex(12.0,-15.0);
  hammerShape.vertex(15.0,-12.0);
  hammerShape.endShape();

  hammerShape.setStroke(false);
  hammerShape.setFill(0);
  
  return hammerShape;
  }
  
PShape createPlierSprite() {
  PShape plierShape = createShape();
  

  plierShape.beginShape();
  plierShape.vertex(-13.0,-16.0);
  plierShape.vertex(-6.0,-16.0);
  plierShape.vertex(-3.0,-12.0);
  plierShape.vertex(4.0,-3.0);
  plierShape.vertex(9.0,-3.0);
  plierShape.vertex(15.0,1.0);
  plierShape.vertex(18.0,5.0);
  plierShape.vertex(17.0,9.0);
  plierShape.vertex(15.0,9.0);
  plierShape.vertex(14.0,13.0);
  plierShape.vertex(11.0,13.0);
  plierShape.vertex(5.0,11.0);
  plierShape.vertex(-1.0,5.0);
  plierShape.vertex(-6.0,6.0);
  plierShape.vertex(-10.0,5.0);
  plierShape.vertex(-16.0,2.0);
  plierShape.vertex(-19.0,-2.0);
  plierShape.vertex(-19.0,-4.0);
  plierShape.vertex(-16.0,-5.0);
  plierShape.vertex(-11.0,0.0);
  plierShape.vertex(-7.0,1.0);
  plierShape.vertex(-2.0,1.0);
  plierShape.vertex(0.0,-1.0);
  plierShape.vertex(-4.0,-7.0);
  plierShape.vertex(-7.0,-10.0);
  plierShape.vertex(-12.0,-11.0);
  plierShape.vertex(-15.0,-13.0);
  plierShape.vertex(-15.0,-15.0);
  plierShape.vertex(-13.0,-16.0);
  plierShape.beginContour();
  plierShape.vertex(9.0,3.0);
  plierShape.vertex(6.0,4.0);
  plierShape.vertex(7.0,8.0);
  plierShape.vertex(9.0,9.0);
  plierShape.vertex(12.0,8.0);
  plierShape.vertex(13.0,5.0);
  plierShape.vertex(11.0,3.0);
  plierShape.vertex(9.0,3.0);
  plierShape.endContour();
  plierShape.endShape();
  
  plierShape.setStroke(false);
  plierShape.setFill(0);
  
  
  
  return plierShape;
  }