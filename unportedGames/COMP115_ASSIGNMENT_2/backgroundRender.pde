/* I chose to move this code to its own tab due to its insane size */

/* Helper functions so code isn't filled with fill and stroke functions */
void polygonMode() {fill(0);noStroke();} /* settings for polygons */
void lineMode() {noFill();stroke(0);strokeWeight(3);} /* settings for lines */

static int ENTRY_DOOR_X = 6;
static int ENTRY_DOOR_Y = 204;

static int EXIT_DOOR_X = 414;
static int DOOR_HEIGHT = 89;

boolean sceneryInitialised = false;

PShape exitDoor, scenery, ground, hotel;

void initialiseScenery() {
  
  exitDoor = createShape(GROUP);
  scenery = createShape(GROUP);
  ground = createShape(GROUP);
  hotel = createShape(GROUP);;
  
  defineScenery();
  defineExitDoor();
  }

void renderBackground(boolean doorState) {
  
  lineMode();
  
  /* starting door */
  rect(ENTRY_DOOR_X,ENTRY_DOOR_Y,48,DOOR_HEIGHT);
     
  /* generic scenery */
  shape(ground,ENTRY_DOOR_X,ENTRY_DOOR_Y+4);
  shape(hotel,ENTRY_DOOR_X,ENTRY_DOOR_Y);
  shape(scenery,414,204);
  
  polygonMode();
  rect(16,219,27,15);
  ellipse(46,254,8,8);
  
  lineMode();
  /* finishing door */
  rect(EXIT_DOOR_X,ENTRY_DOOR_Y,51,DOOR_HEIGHT);
  
  if(doorState) {shape(exitDoor,416,206);}
  else {
    polygonMode();
    rect(424,219,30,15);
    ellipse(458,254,8,8);
    }
  
  polygonMode(); /* reset render mode back to polygons for tool rendering */
  
  }
 
/* ###########################
   All the vertex data below this point was generated using my plotting tool, i've tried to lay it out nicely but its better left ignored...
   ########################### */
 
 
void defineExitDoor() {
    
  /* Door Outline */
  PShape doorOutline = createShape();
  doorOutline.beginShape(LINES);
  doorOutline.strokeWeight(3);
  doorOutline.vertex(60.0,-6.0);
  doorOutline.vertex(78.0,-12.0);
  doorOutline.vertex(78.0,-12.0);
  doorOutline.vertex(77.0,104.0);
  doorOutline.vertex(77.0,104.0);
  doorOutline.vertex(58.0,90.0);
  doorOutline.endShape();
  exitDoor.addChild(doorOutline);
  
  /* Door Window */
  PShape doorWindow = createShape();
  doorWindow.beginShape();
  doorWindow.vertex(55.0,15.0);
  doorWindow.vertex(72.0,9.0);
  doorWindow.vertex(72.0,33.0);
  doorWindow.vertex(55.0,30.0);
  doorWindow.endShape();
  
  doorWindow.setStroke(false);
  doorWindow.setFill(0);
  
  exitDoor.addChild(doorWindow);
  
  /* Door Inner Handle */
  PShape doorInHandle = createShape(ELLIPSE, 70, 50, 8, 8);  
  doorInHandle.setStroke(false);
  doorInHandle.setFill(0);
  
  exitDoor.addChild(doorInHandle);
  
  /* Door Outer Handle */
  PShape doorOutHandle = createShape();
  doorOutHandle.beginShape(LINES);
  doorOutHandle.strokeWeight(3);
  doorOutHandle.vertex(84.0,45.0);
  doorOutHandle.vertex(84.0,52.0);
  doorOutHandle.endShape();
  exitDoor.addChild(doorOutHandle);
  }
  
void defineScenery() {
  
  
  PShape houseRoof = createShape();
  houseRoof.beginShape();
  houseRoof.vertex(102.0,-20.0);
  houseRoof.vertex(67.0,-33.0);
  houseRoof.vertex(-17.0,6.0);
  houseRoof.vertex(-17.0,-6.0);
  houseRoof.vertex(-20.0,-7.0);
  houseRoof.vertex(-19.0,-14.0);
  houseRoof.vertex(67.0,-54.0);
  houseRoof.vertex(102.0,-42.0);
  houseRoof.endShape();
  houseRoof.setStroke(false);
  houseRoof.setFill(0);
  
  
  PShape houseWall = createShape();
  houseWall.beginShape(LINES);
  houseWall.strokeWeight(3);
  houseWall.vertex(-7.0,97.0);
  houseWall.vertex(-7.0,0.0);
  houseWall.endShape();
  
  
  PShape houseTree1 = createShape();
  houseTree1.beginShape();
  houseTree1.vertex(1.0,-23.0);
  houseTree1.vertex(10.0,-32.0);
  houseTree1.vertex(1.0,-31.0);
  houseTree1.vertex(11.0,-44.0);
  houseTree1.vertex(1.0,-45.0);
  houseTree1.vertex(13.0,-56.0);
  houseTree1.vertex(4.0,-56.0);
  houseTree1.vertex(16.0,-71.0);
  houseTree1.vertex(7.0,-70.0);
  houseTree1.vertex(17.0,-84.0);
  houseTree1.vertex(12.0,-86.0);
  houseTree1.vertex(19.0,-96.0);
  houseTree1.vertex(15.0,-95.0);
  houseTree1.vertex(26.0,-110.0);
  houseTree1.vertex(32.0,-100.0);
  houseTree1.vertex(37.0,-95.0);
  houseTree1.vertex(30.0,-96.0);
  houseTree1.vertex(36.0,-88.0);
  houseTree1.vertex(33.0,-86.0);
  houseTree1.vertex(40.0,-73.0);
  houseTree1.vertex(34.0,-75.0);
  houseTree1.vertex(49.0,-61.0);
  houseTree1.vertex(39.0,-59.0);
  houseTree1.vertex(49.0,-47.0);
  houseTree1.vertex(42.0,-48.0);
  houseTree1.vertex(47.0,-43.0);
  houseTree1.vertex(1.0,-20.0);
  houseTree1.endShape();  
  houseTree1.setStroke(false);
  houseTree1.setFill(color(86,130,38));
    
  PShape houseTree2 = createShape();
  houseTree2.beginShape();
  houseTree2.vertex(72.0,-49.0);
  houseTree2.vertex(54.0,-52.0);
  houseTree2.vertex(72.0,-63.0);
  houseTree2.vertex(63.0,-69.0);
  houseTree2.vertex(73.0,-79.0);
  houseTree2.vertex(65.0,-84.0);
  houseTree2.vertex(78.0,-93.0);
  houseTree2.vertex(71.0,-99.0);
  houseTree2.vertex(79.0,-105.0);
  houseTree2.vertex(72.0,-112.0);
  houseTree2.vertex(84.0,-120.0);
  houseTree2.vertex(77.0,-123.0);
  houseTree2.vertex(84.0,-127.0);
  houseTree2.vertex(90.0,-133.0);
  houseTree2.vertex(96.0,-126.0);
  houseTree2.vertex(100.0,-123.0);
  houseTree2.vertex(99.0,-40.0);
  houseTree2.vertex(72.0,-49.0);
  houseTree2.endShape();
  houseTree2.setStroke(false);
  houseTree2.setFill(color(86,130,38));
  
  PShape houseTree3 = createShape();
  houseTree3.beginShape();
  houseTree3.vertex(43.0,-38.0);
  houseTree3.vertex(26.0,-39.0);
  houseTree3.vertex(48.0,-48.0);
  houseTree3.vertex(26.0,-50.0);
  houseTree3.vertex(45.0,-63.0);
  houseTree3.vertex(29.0,-66.0);
  houseTree3.vertex(49.0,-78.0);
  houseTree3.vertex(41.0,-81.0);
  houseTree3.vertex(51.0,-87.0);
  houseTree3.vertex(42.0,-89.0);
  houseTree3.vertex(51.0,-96.0);
  houseTree3.vertex(44.0,-99.0);
  houseTree3.vertex(55.0,-110.0);
  houseTree3.vertex(64.0,-99.0);
  houseTree3.vertex(57.0,-97.0);
  houseTree3.vertex(69.0,-89.0);
  houseTree3.vertex(59.0,-88.0);
  houseTree3.vertex(72.0,-77.0);
  houseTree3.vertex(57.0,-77.0);
  houseTree3.vertex(72.0,-65.0);
  houseTree3.vertex(58.0,-64.0);
  houseTree3.vertex(72.0,-56.0);
  houseTree3.vertex(60.0,-53.0);
  houseTree3.vertex(78.0,-41.0);
  houseTree3.endShape();
  houseTree3.setStroke(false);
  houseTree3.setFill(color(73,110,32));
  
   
  PShape groundLines1 = createShape();
  
  groundLines1.beginShape(LINES);
  groundLines1.strokeWeight(3);
  groundLines1.vertex(36.0,92.0);
  groundLines1.vertex(43.0,90.0);
  groundLines1.vertex(43.0,90.0);
  groundLines1.vertex(52.0,92.0);
  groundLines1.vertex(51.0,92.0);
  groundLines1.vertex(61.0,88.0);
  groundLines1.vertex(60.0,88.0);
  groundLines1.vertex(67.0,91.0);
  groundLines1.vertex(67.0,91.0);
  groundLines1.vertex(76.0,88.0);
  groundLines1.vertex(126.0,90.0);
  groundLines1.vertex(130.0,91.0);
  groundLines1.vertex(130.0,91.0);
  groundLines1.vertex(135.0,88.0);
  groundLines1.vertex(135.0,88.0);
  groundLines1.vertex(141.0,88.0);
  groundLines1.vertex(141.0,88.0);
  groundLines1.vertex(145.0,91.0);
  groundLines1.vertex(145.0,91.0);
  groundLines1.vertex(150.0,88.0);
  groundLines1.vertex(150.0,88.0);
  groundLines1.vertex(157.0,88.0);
  groundLines1.vertex(157.0,88.0);
  groundLines1.vertex(160.0,91.0);
  groundLines1.vertex(160.0,91.0);
  groundLines1.vertex(165.0,91.0);
  groundLines1.vertex(187.0,91.0);
  groundLines1.vertex(195.0,87.0);
  groundLines1.vertex(195.0,87.0);
  groundLines1.vertex(201.0,86.0);
  groundLines1.vertex(211.0,85.0);
  groundLines1.vertex(214.0,89.0);
  groundLines1.vertex(214.0,89.0);
  groundLines1.vertex(219.0,91.0);
  groundLines1.vertex(261.0,88.0);
  groundLines1.vertex(267.0,85.0);
  groundLines1.vertex(267.0,85.0);
  groundLines1.vertex(282.0,85.0);
  groundLines1.vertex(282.0,85.0);
  groundLines1.vertex(288.0,88.0);
  groundLines1.vertex(317.0,88.0);
  groundLines1.vertex(327.0,90.0);
  groundLines1.vertex(327.0,90.0);
  groundLines1.vertex(334.0,92.0);
  groundLines1.vertex(334.0,92.0);
  groundLines1.vertex(342.0,94.0);
  groundLines1.vertex(342.0,94.0);
  groundLines1.vertex(349.0,90.0);
  groundLines1.vertex(349.0,90.0);
  groundLines1.vertex(357.0,90.0);
  groundLines1.vertex(357.0,90.0);
  groundLines1.vertex(364.0,94.0);
  groundLines1.vertex(364.0,94.0);
  groundLines1.vertex(369.0,91.0);
  groundLines1.vertex(369.0,91.0);
  groundLines1.vertex(378.0,90.0);
  groundLines1.vertex(378.0,90.0);
  groundLines1.vertex(388.0,94.0);
  groundLines1.endShape();
  groundLines1.setStroke(color(188,121,107));
  groundLines1.setFill(false);
  
  PShape groundLines2 = createShape();
  groundLines2.beginShape(LINES);
  groundLines2.strokeWeight(3);
  groundLines2.vertex(38.0,99.0);
  groundLines2.vertex(46.0,98.0);
  groundLines2.vertex(46.0,98.0);
  groundLines2.vertex(55.0,102.0);
  groundLines2.vertex(55.0,102.0);
  groundLines2.vertex(62.0,98.0);
  groundLines2.vertex(62.0,98.0);
  groundLines2.vertex(70.0,100.0);
  groundLines2.vertex(70.0,100.0);
  groundLines2.vertex(73.0,96.0);
  groundLines2.vertex(73.0,96.0);
  groundLines2.vertex(79.0,96.0);
  groundLines2.vertex(125.0,96.0);
  groundLines2.vertex(130.0,99.0);
  groundLines2.vertex(130.0,99.0);
  groundLines2.vertex(137.0,96.0);
  groundLines2.vertex(137.0,96.0);
  groundLines2.vertex(145.0,100.0);
  groundLines2.vertex(145.0,100.0);
  groundLines2.vertex(151.0,100.0);
  groundLines2.vertex(151.0,100.0);
  groundLines2.vertex(154.0,98.0);
  groundLines2.vertex(154.0,98.0);
  groundLines2.vertex(160.0,98.0);
  groundLines2.vertex(160.0,98.0);
  groundLines2.vertex(164.0,100.0);
  groundLines2.vertex(164.0,100.0);
  groundLines2.vertex(170.0,100.0);
  groundLines2.vertex(170.0,100.0);
  groundLines2.vertex(173.0,98.0);
  groundLines2.vertex(173.0,98.0);
  groundLines2.vertex(181.0,99.0);
  groundLines2.vertex(133.0,105.0);
  groundLines2.vertex(142.0,106.0);
  groundLines2.vertex(142.0,106.0);
  groundLines2.vertex(144.0,109.0);
  groundLines2.vertex(161.0,107.0);
  groundLines2.vertex(173.0,109.0);
  groundLines2.vertex(173.0,109.0);
  groundLines2.vertex(175.0,105.0);
  groundLines2.vertex(175.0,105.0);
  groundLines2.vertex(179.0,105.0);
  groundLines2.vertex(191.0,98.0);
  groundLines2.vertex(196.0,98.0);
  groundLines2.vertex(196.0,98.0);
  groundLines2.vertex(197.0,95.0);
  groundLines2.vertex(197.0,95.0);
  groundLines2.vertex(203.0,93.0);
  groundLines2.vertex(203.0,93.0);
  groundLines2.vertex(206.0,96.0);
  groundLines2.vertex(206.0,96.0);
  groundLines2.vertex(211.0,93.0);
  groundLines2.vertex(211.0,93.0);
  groundLines2.vertex(215.0,97.0);
  groundLines2.vertex(215.0,98.0);
  groundLines2.vertex(220.0,99.0);
  groundLines2.vertex(220.0,99.0);
  groundLines2.vertex(221.0,96.0);
  groundLines2.vertex(221.0,96.0);
  groundLines2.vertex(230.0,95.0);
  groundLines2.vertex(221.0,105.0);
  groundLines2.vertex(228.0,102.0);
  groundLines2.vertex(263.0,94.0);
  groundLines2.vertex(271.0,93.0);
  groundLines2.vertex(271.0,93.0);
  groundLines2.vertex(277.0,92.0);
  groundLines2.vertex(277.0,92.0);
  groundLines2.vertex(281.0,96.0);
  groundLines2.vertex(281.0,96.0);
  groundLines2.vertex(289.0,96.0);
  groundLines2.vertex(289.0,96.0);
  groundLines2.vertex(290.0,96.0);
  groundLines2.vertex(316.0,99.0);
  groundLines2.vertex(325.0,98.0);
  groundLines2.vertex(325.0,98.0);
  groundLines2.vertex(332.0,102.0);
  groundLines2.vertex(332.0,102.0);
  groundLines2.vertex(345.0,101.0);
  groundLines2.vertex(345.0,101.0);
  groundLines2.vertex(355.0,98.0);
  groundLines2.vertex(355.0,98.0);
  groundLines2.vertex(361.0,99.0);
  groundLines2.vertex(361.0,99.0);
  groundLines2.vertex(367.0,102.0);
  groundLines2.vertex(367.0,102.0);
  groundLines2.vertex(374.0,99.0);
  groundLines2.vertex(374.0,99.0);
  groundLines2.vertex(381.0,98.0);
  groundLines2.vertex(381.0,98.0);
  groundLines2.vertex(388.0,102.0);
  groundLines2.vertex(316.0,107.0);
  groundLines2.vertex(320.0,107.0);
  groundLines2.vertex(320.0,107.0);
  groundLines2.vertex(324.0,110.0);
  groundLines2.vertex(352.0,108.0);
  groundLines2.vertex(358.0,107.0);
  groundLines2.vertex(358.0,107.0);
  groundLines2.vertex(359.0,108.0);
  groundLines2.endShape();
  groundLines2.setStroke(color(152,127,123));
  groundLines2.setFill(false);
  
  
  
  PShape hotel1 = createShape();
  hotel1.beginShape();
  hotel1.vertex(-7.0,-11.0);
  hotel1.vertex(0.0,-10.0);
  hotel1.vertex(0.0,-17.0);
  hotel1.vertex(41.0,-17.0);
  hotel1.vertex(41.0,-24.0);
  hotel1.vertex(2.0,-25.0);
  hotel1.vertex(1.0,-80.0);
  hotel1.vertex(39.0,-79.0);
  hotel1.vertex(39.0,-86.0);
  hotel1.vertex(1.0,-87.0);
  hotel1.vertex(1.0,-135.0);
  hotel1.vertex(40.0,-136.0);
  hotel1.vertex(40.0,-146.0);
  hotel1.vertex(-7.0,-146.0);
  hotel1.endShape();
  hotel1.setStroke(false);
  hotel1.setFill(color(137,66,37));

  PShape hotel2 = createShape();
  hotel2.beginShape();
  hotel2.vertex(-7.0,5.0);
  hotel2.vertex(-1.0,5.0);
  hotel2.vertex(-2.0,-4.0);
  hotel2.vertex(41.0,-4.0);
  hotel2.vertex(40.0,-20.0);
  hotel2.vertex(-1.0,-21.0);
  hotel2.vertex(-2.0,-66.0);
  hotel2.vertex(40.0,-64.0);
  hotel2.vertex(40.0,-82.0);
  hotel2.vertex(-2.0,-81.0);
  hotel2.vertex(-2.0,-129.0);
  hotel2.vertex(38.0,-129.0);
  hotel2.vertex(37.0,-140.0);
  hotel2.vertex(-7.0,-141.0);
  hotel2.endShape();
  hotel2.setStroke(false);
  hotel2.setFill(color(174,140,102));
  
  PShape hotel3 = createShape();
  hotel3.beginShape(LINES);
  hotel3.strokeWeight(3);
  hotel3.vertex(1.0,-46.0);
  hotel3.vertex(35.0,-46.0);
  hotel3.vertex(32.0,-24.0);
  hotel3.vertex(32.0,-45.0);
  hotel3.vertex(25.0,-24.0);
  hotel3.vertex(25.0,-45.0);
  hotel3.vertex(16.0,-23.0);
  hotel3.vertex(16.0,-45.0);
  hotel3.vertex(8.0,-23.0);
  hotel3.vertex(7.0,-45.0);
  hotel3.vertex(0.0,-107.0);
  hotel3.vertex(37.0,-107.0);
  hotel3.vertex(33.0,-84.0);
  hotel3.vertex(33.0,-107.0);
  hotel3.vertex(25.0,-84.0);
  hotel3.vertex(25.0,-108.0);
  hotel3.vertex(16.0,-84.0);
  hotel3.vertex(17.0,-107.0);
  hotel3.vertex(9.0,-84.0);
  hotel3.vertex(9.0,-107.0);
  hotel3.endShape();
  hotel3.setStroke(color(167,126,114));
  hotel3.setFill(false);

  /* grouped in intentional draw order*/
  scenery.addChild(houseTree3);
  scenery.addChild(houseTree1);
  scenery.addChild(houseTree2);
  scenery.addChild(houseWall);  
  scenery.addChild(houseRoof);

  ground.addChild(groundLines1);
  ground.addChild(groundLines2);
  
  hotel.addChild(hotel3);
  hotel.addChild(hotel2);
  hotel.addChild(hotel1);

  }