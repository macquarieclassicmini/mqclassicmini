/*

  The game manager for the game selection menu

*/

class menuGameManager implements gameManager {
  color BG_COLOUR_1 = color(#0D0D09);
  color BG_COLOUR_2 = color(#191919);
  float menuFloatDegrees = 0;
  float menuFloatLength = 16;
  
  float selectionOffset = 0;
  int selection = 0;
  
  int optionDistance = 32;
  int option1Y = height/2+48;
  int option2Y = option1Y+optionDistance;
   
  PFont menuFont;
  PFont subtitleFont;
  PFont optionFont;
  
  button upKey, downKey, enterKey, returnKey;

  menuGameManager() {
    menuFont = loadFont("PixelOperatorMono8-Bold-64.vlw"); //PixelOperator and is a public domain font, which can be found at  https://www.dafont.com/pixel-operator.font
    subtitleFont = loadFont("PixelOperatorMono8-Bold-32.vlw");
    optionFont = loadFont("PixelOperatorMono8-Bold-16.vlw");
    
    upKey = input.defineButton(UP);
    downKey = input.defineButton(DOWN);
    /* Windows uses ENTER but mac uses RETURN, safest to use BOTH*/
    enterKey = input.defineButton(ENTER);
    returnKey = input.defineButton(RETURN);
    
    
    };
  
  void update() {
    menuFloatDegrees+=1;
    if (menuFloatDegrees>360) {menuFloatDegrees = 0;}
   
    
    /* averaging for a smooth transition */
    selectionOffset = ((selectionOffset*3)+selection)/4;
    
    };
  void render() {
    /* fancy rendering of the menu text */
    
    
    textAlign(CENTER,CENTER);
    
    int degreeSpacing = 5;
    int iterations = 15;
    
    background(lerpColor(BG_COLOUR_1,BG_COLOUR_2,(dsin(menuFloatDegrees/2))));

    for(int i = iterations; i>= 0; i--) {
      float a = (255*(i/((float)iterations))); 
      fill(255-(a*1.2),255-a*1.2);
      textFont(menuFont);
      text("HELMET",width/2,64+(dsin(menuFloatDegrees-(degreeSpacing*i))*menuFloatLength)); /* draw the text*/
      textFont(subtitleFont);
      fill(128-(a*1.2),128-a*1.2);
      text("Assignment 2",width/2,112+(dsin(menuFloatDegrees-(degreeSpacing*i))*menuFloatLength)); /* draw the text*/
      }
   
   textFont(optionFont);
   
   fill(255-(128*selectionOffset));
   text("CLASSIC MODE (CRITERIA)",width/2,option1Y-(optionDistance*selectionOffset));
   
   fill(128+(128*selectionOffset));
   text("ULTRA MODE (SHOWING OFF)",width/2,option2Y-(optionDistance*selectionOffset));
   
   
   
   fill(255);
   
   text("PRESS M TO RETURN TO MENU",width/2,height-16);
   
   int leftX = 16;
   int rightX = width-16;
   
   triangle(leftX,option1Y+(16*dsin(menuFloatDegrees*2)),leftX,option1Y-(16*dsin(menuFloatDegrees*2)),leftX+28,option1Y);
   triangle(rightX,option1Y+(16*dsin(menuFloatDegrees*2)),rightX,option1Y-(16*dsin(menuFloatDegrees*2)),rightX-28,option1Y);
   
   textAlign(LEFT,TOP);
   
    };
  void handleInputs() {
    if (upKey.pressed && selection == 0) {selection = 1;}
    if (downKey.pressed && selection == 1) {selection = 0;}
    if (enterKey.pressed || returnKey.pressed) {
      if (selection == 0) {changeGameManager(new classicGameManager());}
       else {changeGameManager(new ultraGameManager());}
      }
    };
    
    
  void cleanup() {
    input.delButton(upKey);
    input.delButton(downKey);
    input.delButton(enterKey);
    input.delButton(returnKey);
    };
  }