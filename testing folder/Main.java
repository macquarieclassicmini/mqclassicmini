import processing.core.*;

public class ColorfulCircles extends PApplet{
	int gwPos = 1;
	int blackScore = 0;
	int redScore = 0;
	int toolCounter = 60;
	int counter = (int)random(180, 480);
	int gapX = 60;
	int[] toolY = {1, 5, 3};
	boolean doorOpen = false;
	boolean gameWon = false;

	public void settings() {
		size(512, 348);
	}  

	public void draw() {
		background(255);
		backdrop();

		//decreases the counter once a frame
		counter = counter - 1;
		toolCounter = toolCounter - 1;

		//calls other functions without altering their variables
		doorState();
		charPos(0);
		toolPos(); 
		displayScore(0, 0);

		//calls gameover function when counter is set high on fail state
		for (int i = counter; i > 480; i--) {
			gameOver();
		}
	}

	//defines the current state of G&W by user input, increases score if goal is met
	public void keyPressed() {
		if (key == CODED) {
			if (keyCode == RIGHT) {
				charPos(1);
			} else if (keyCode == LEFT) {
				charPos(-1);
			}
		}
	}

	void backdrop() {
		PImage backdropLeft;
		PImage backdropBottom;
		PImage backdropRight;
		backdropLeft = loadImage("backdropLeft.png");
		backdropBottom = loadImage("backdropBottom.png");
		backdropRight = loadImage("backdropRight.png");
		image(backdropLeft, 0, 0);
		image(backdropRight, 400, 0);
		image(backdropBottom, 85, 327);
	}

	void charPos(int gwAdd) {
		PImage gw;
		gw = loadImage("gwhelmet.gif");

		//assigns Mr G&W a position on the screen, changes based on user input
		int gwY = 280;
		int gwX = gwPos*gapX+20;
		image(gw, gwX, gwY);  

		//moves gw left and right, or not at all if he is at the bounds of the stage
		//increments score by 3 when goal is met, sets door timer low to prevent player spam scoring
		gwPos = gwPos + gwAdd;

		if (gwPos == 7) {
			if (doorOpen == true) {
				gwPos = 1;
				counter = 20;
				displayScore(3, 0);
			} else {
				gwPos = 6;
			}
		}
		if (gwPos < 1) {
			gwPos = 1;
		}
	}

	void displayScore(int blackAdd, int redAdd) {
		ellipseMode(CENTER);
		int drawnScore = 0;
		int circleNum = 0;
		blackScore = blackAdd + blackScore; //increases positive score
		redScore = redAdd + redScore; // increases negative score

		if (blackScore >= 40) {  //resets game if circles fill screen
			gameWon = true;
			gameOver();
		} else if (blackScore + redScore >= 40) {
			gameWon = false;
			gameOver();
		}

		//negative score
		for (int i=0; i < redScore; i++) {            //makes the circles display in rows
			drawnScore = i*8;
			circleNum = redScore;
			circleNum = circleNum - drawnScore;

			for (int j=0; j<circleNum && j<8; j++) {    //draws the circles in a row

				for (int k = 50; k > 0; k--) {            //makes a gradient circle
					fill(200, 0, 0, 70-k);
					noStroke();
					ellipse((gapX*j+40), (i*70+35), k, k);
				}
			}
		}

		//positive score, functions same as negative
		for (int i=0; i < blackScore; i++) {
			drawnScore = i*8;
			circleNum = blackScore;
			circleNum = circleNum - drawnScore;

			for (int j=0; j<circleNum && j<8; j++) {

				for (int k = 50; k > 0; k--) {
					fill(k*3, 90-k);
					noStroke();
					ellipse((gapX*j+40), (i*70+35+redScore/8*70), k, k);  //makes black circles appear below red circles
				}
			}
		}
	}

	void doorState() {
		PImage doorClosed;
		PImage doorClear;
		doorClear = loadImage("doorClear.png");
		doorClosed = loadImage("doorClosed.png");

		//defines state of door and displays appropriate image
		if (counter <=0) {
			if (doorOpen == false) {
				doorOpen = true;
				counter = (int)random(180, 480);
			} else {
				doorOpen = false;
				counter = (int)random(180, 480);
			}
		}

		if (doorOpen == false) {
			image(doorClosed, 450, 225);
		} else if (doorOpen == true) {
			image(doorClear, 450, 225);
		}
	}

	void gameOver() {
		//resets all relevant variables to original state, displays appropriate game over screen
		//set counter high to tell draw loop to recall this function for an additional few seconds
		if (counter < 480) {
			gwPos = 1;
			doorOpen = false;
			counter = 600;
			toolY[0] = 9;
			toolY[1] = 1;
			toolY[2] = 11;
		}
		// resetting score to 0 here prevents player scoring during game over screen
		if (counter > 480) {
			if (gameWon == false) {
				background(0);
				fill(255);
				textSize(40);
				text("GAME OVER", width/4, height/2);
				blackScore = 0;
				redScore = 0;
				return;
			} else {
				background(0);
				fill(255);
				textSize(40);
				text("YOU WIN", width/3, height/2);
				blackScore = 0;
				redScore = 0;
				return;
			}
		}
	}

	void toolPos() {
		PImage[] toolType = new PImage[3];
		toolType[0] = loadImage("hammer.png");
		toolType[1] = loadImage("bucket.png");
		toolType[2] = loadImage("wrench.png");

		//defines tool position once a second
		//tools in array jump til off screen, then reset
		int gapY = 30;
		int[] toolX = {2, 3, 5};
		for (int i = 2; i > -1; i--) {
			image(toolType[i], toolX[i]*gapX+20, toolY[i]*gapY+20);
			if (toolCounter == 0) {
				toolY[i] = toolY[i] + 2;
			} else if (toolY[i]  >= 12) {
				toolY[i] = 1;
				displayScore(1, 0);
			}
		}

		if (toolCounter == 0) {
			toolCounter = 60;
		}

		//resets if gw and the tool are touching, adds negative score
		for (int i = 2; i > -1; i--)
			if (toolY[i] == 9 && gwPos == toolX[i]) {
				gwPos = 1;
				toolY[i] = 1;
				displayScore(0, 8);
			}
	}

}