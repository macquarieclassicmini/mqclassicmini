import processing.core.*; 
import java.util.HashMap; 
import java.util.ArrayList; 
import cart.Cart;

public class COMP115_ASSIGNMENT_2 extends Cart {

/*
    CHLOE HARRIS - 45417631
    
     COMP115 ASSIGNMENT 2
*/

PFont scoreFont;

inputManager input;
gameManager gm;

/* a global function to allow game managers to swap to eachother*/
public void changeGameManager(gameManager newManager) {
  gm.cleanup(); /* make the old game manager clear up its resources before changing */
  gm = newManager;
  }

public void setup() { 
  frameRate(30);
  

  /* prime scenery from backgroundRender, this only needs to be done once */
  initialiseScenery();
  leftFrame = createShape();
  initialiseLeftFrame();
  rightFrame = createShape();
  initialiseRightFrame();
  
  input = new inputManager();
  
  gm = new menuGameManager();
  
  }
  
public void draw() {
  gm.update();
  gm.render();
  }
    
public void keyPressed() {
  if (key == ESC) {
	  eject();
	  key = 0;
	  }
  
  input.checkKeyPressed(); /* the inputManager needs to be updated before games process their logic */
  gm.handleInputs();
  input.unpressKeys();
  }
  
public void keyReleased() {
  input.checkKeyReleased();
  }
    

  
/*
I made my own system for handling keyboard inputs because processing's default system doesn't handle holding keys well and only processes one key at a time;
*/

class inputManager {
public ArrayList<button> allButtons;

inputManager() {
  allButtons = new ArrayList<button>();
  }
  
/* overloaded function to automaticially determine the type of button required */
button defineButton(char c) {
  charButton b = new charButton(c);
  allButtons.add(b);
  return b;
  }
  
 button defineButton(int t) {
  keyButton b = new keyButton(t);
  allButtons.add(b);
  return b;
  }
  
/* for cleanup */
void delButton(button b) {
  allButtons.remove(b);
  b = null;
  }
  
/* update all keys each time the keyPressed and keyReleased events occur */
void checkKeyPressed() {
  for(button b : allButtons) {b.press();}
  }
  
void checkKeyReleased() {
  for(button b : allButtons) {b.release();}
  }
  
  
/* used to make sure that after one frame keys are only HELD, not PRESSED */
void unpressKeys() {
  for(button b : allButtons) {b.pressed = false;}
  }
}



/* 
  An abstract button class to contain both key and keyCode based buttons 
*/
abstract class button {
public boolean pressed = false;
public boolean held = false;

abstract void press();
abstract void release();
}

class charButton extends button {
private char trigger;

charButton(char c) {
  trigger = c;
  }

void press() {
  if (key == trigger) {
    if (!held) {pressed = true;} /* prevent pressed event from triggering twice*/ 
    held = true;
    }
  }
  
void release() {
  if (key == trigger) {held = false;}
  }
}

class keyButton extends button {
private int trigger;

keyButton(int k) {
  trigger = k;
  }

void press() {
  if (key==CODED && keyCode == trigger) {
    if (!held) {pressed = true;} /* prevent pressed event from triggering twice in one hold*/ 
    held = true;
    }
  }
  
void release() {
  if (key==CODED && keyCode == trigger) {held = false;}
  }
}

  
  
  
/*

  The game manager for the marking criteria version of the game

*/


/* All classic mode constants */
static int STARTSLOT = 0;
static int EXITSLOT = 6;
static int PLAYER_START_X = 58;
static float MIN_TOOL_SPAWN_TIME = 0.5f;
static float MAX_TOOL_SPAWN_TIME = 1.0f;
static float MIN_DOOR_TIME = 3.0f;
static float MAX_DOOR_TIME = 8.0f;
static float CIRCLE_SIZE = 58;
static float CIRCLE_COLUMNS = (int)(512/58)+1;
static float CIRCLE_ROWS = (int)348/58;



class classicGameManager implements gameManager {
  public int score = 0;
  public int deaths = 0;
  public classicPlayerClass player;
  private timer toolSpawnTimer; /* seconds till the next tool spawn*/
  private timer doorTimer; /* Timer used to track door opening and closing */
  private boolean doorOpen = false; /* the state of the door */
  
  private ArrayList<classicToolClass> allTools; /* List used to keep track of all the existing tools */
  
  private button returnToMenuKey;
  
  classicGameManager() {
    /* Initialise the game */
    player = new classicPlayerClass(PLAYER_START_X,224);
    allTools = new ArrayList<classicToolClass>();
            
    /* define our two timers */
    doorTimer = new timer(round(random(MIN_DOOR_TIME,MAX_DOOR_TIME)));
    toolSpawnTimer = new timer(random(MIN_TOOL_SPAWN_TIME,MAX_TOOL_SPAWN_TIME));
    
    /* i trust my own code more than processing's */
    returnToMenuKey = input.defineButton('m');
    }
    
  /* ########################
     #   PER FRAME UPDATE   #
     ######################## */
  public void update() {
    processTimers();
    if(toolSpawnTimer.complete()){
      spawnTool();
      }
    processTools();
    updateDoor();
    /* If the screen is full of circles */
    if (score+(deaths*CIRCLE_COLUMNS)>(CIRCLE_COLUMNS*CIRCLE_ROWS)) {
      gameReset();
      }
    
    }
    
  /* #######################
     #   RENDER THE GAME   #
     ####################### */
  public void render() {
    background(161,171,172);
    stroke(255,255);
    fill(255,255);
    renderBackground(doorOpen);
    player.renderPlayer();
    renderTools();
    drawScoreAndDeathCircles(score,deaths);
    }
  
  /* #####################################
     #                                   #
     #   ONE FUNCTION TO RULE THEM ALL   #
     #    DRAWS CIRCLES TO THE SCREEN    #
     #                                   #
     ##################################### */
  public void drawScoreAndDeathCircles(int scr, int dth) {
    /* score and deaths are already accessable in this scope but the marking criteria specifies this function MUST be an INFORMED changer, so here they are as inputs anyway*/
    
    /* Draw the red circles*/
    float redOffset = CIRCLE_SIZE/2;
    for(int i = 0; i<dth; i++) {
      for(int o = 0; o<CIRCLE_COLUMNS; o++) {
          gradientCircle(redOffset+(CIRCLE_SIZE*o),redOffset+(CIRCLE_SIZE*i),CIRCLE_SIZE,color(255,0,0));
        }
      }
      
    /* Draw the black circles*/
    float blackXOffset = CIRCLE_SIZE/2;
    float blackYOffset = (CIRCLE_SIZE/2)+(CIRCLE_SIZE*dth);
    for(int i = 0; i<scr; i++) {
      gradientCircle(blackXOffset+(CIRCLE_SIZE*(i%CIRCLE_COLUMNS)),blackYOffset+(CIRCLE_SIZE*(int)(i/CIRCLE_COLUMNS)),CIRCLE_SIZE,color(0,0,0));
      }
    }  
  
  /* ######################
     #   INPUT HANDLING   #
     ###################### */
  public void handleInputs() {
    
    if (returnToMenuKey.pressed) {changeGameManager(new menuGameManager());return;}    
    
    /*all of this code was written before i made inputManager and i don't want to touch this code anymore*/
    if (keyCode==RIGHT) {
      if (doorOpen) {
        if (player.playerSlot<6) {player.moveForward();}
        }
      else if (player.playerSlot<5) {
         player.moveForward();
          }
      }
    else if (keyCode==LEFT) {
      if (player.playerSlot>0) {player.moveBackward();}
      }  
      
    /* press m to return to the main menu */
    
      
    /* quality of life tweak, never make the player wait for the door to close for more than a second */ 
    if (player.playerSlot==EXITSLOT && doorTimer.time>frameRate) {doorTimer.setTimer(1);}
    } 

  /* ##################
     #   UPDATE DOOR  #
     ################## */

  public void updateDoor() {
    if(doorTimer.complete()) {
        /* Claim a point and reset if the player was in the door when it closes*/
        if (doorOpen) {
          if (player.playerSlot==EXITSLOT) {
            playerWin();
            }
          }
      doorOpen = !doorOpen;
      doorTimer.setTimer(round(random(MIN_DOOR_TIME,MAX_DOOR_TIME)));
      }
    }
  
  /* ##################
     #   SPAWN TOOLS  #
     ################## */
  public void spawnTool() {
    int targetSlot = 1;
    boolean foundSlot = false; /* whether an empty slot has been found */
    boolean clash = false;
 
    while(!foundSlot) {
      clash = false;
      targetSlot = randomInt(1,5);     
      /* iterate through all tools to see if there's one at the top of our target slot*/
      for(classicToolClass tool : allTools) {
        if(tool.slot == targetSlot) {
          if(tool.row == 1) {clash = true; break;}
          }
        }
      /* if no clashes were found, this slot is safe */
      if(!clash) {foundSlot = true;}
      }
    toolSpawnTimer.setTimer(random(MIN_TOOL_SPAWN_TIME,MAX_TOOL_SPAWN_TIME));
    allTools.add(new classicToolClass(targetSlot));
    }
    
  /* ####################
     #   PROCESS TOOLS  #
     #################### */
  public void processTools() {
    classicToolClass tool;
    for(int i = allTools.size()-1; i>=0; i--) { /*process tool list backwards*/
      tool = allTools.get(i);   
      tool.updateTool(allTools);
      if (tool.row == 5 && tool.slot == player.playerSlot) { /* confirmed collision, reset game */
        playerDie();
        }
      /* instances cannot null themselves, so this has to be done here */
      if (tool.row > 5) {
        allTimers.remove(tool.dropTimer);
        tool.sprite = null;
        tool = null; allTools.remove(i);
        /* A tool hitting the ground gives us one point */
        score++;
        }
      }
    }
    
  /* ####################
     #   RENDER TOOLS   #
     #################### */
  public void renderTools() {
    for(classicToolClass tool : allTools) {tool.renderTool();}
    }  
  
  
  /* ###############################
     #   MISC GAMEPLAY FUNCTIONS   #
     ############################### */
  public void playerDie() {
    player.resetPosition();
    deaths++;
    }
    
  public void playerWin() {
    player.resetPosition();
    score+=3;
    }
    
  public void gameReset() {
    println("Game Over!");
    println("Final score: " + str(score) + " with " + str(deaths) + " deaths!");
    deaths = 0;
    score = 0;
    }

  /* ###############
     #   CLEANUP   #
     ############### */
  public void cleanup() {
    /* this MIGHT be entirely pointless because of java's garbage collector, but it doesn't hurt to clean up after youself */
    player = null;
    doorTimer.destroy();
    doorTimer=null;
    toolSpawnTimer.destroy();
    toolSpawnTimer=null;
    
    /* remove all the tool instances*/
    for(int i = allTools.size()-1; i>=0; i--) {
      classicToolClass tool = allTools.get(i);
      tool.destroy();
      tool = null; allTools.remove(i);
      }
    allTools = null;
    
    input.delButton(returnToMenuKey);
    
    }

  }

/* Helper class to allow me to easily keep second interval Timers */

ArrayList<timer> allTimers = new ArrayList<timer>();

class timer {
  float time; /* frames remaining */
  
  /* empty constructor */
  timer() {
    this.time = 0;
    allTimers.add(this);
    }
  
  /* constructor takes time in SECONDS */
  timer(float time) {
    this.time = time*frameRate; /* convert seconds to frames */
    allTimers.add(this); /* add to the tracker */
    }
    
  /* cause Timer to tick down */
  public void update() {if (time>0) {time--;}}
  
  /* set Timer to a new time IN SECONDS */
  public void setTimer(float newTime) {this.time = newTime*frameRate;}
  
  public boolean complete() {
    if (time<=0) {return true;}
    return false;
    }
    
  public void destroy() {
    allTimers.remove(this);
    }
  }
  
/* update all existing Timers at once */
public void processTimers() {
  timer t;
  for(int i = 0; i<allTimers.size(); i++) {
    t = allTimers.get(i);
    t.update();
    }
  }
   
public static boolean collidersIntersect(collider a, collider b) {
  if (a.x2 <= b.x || a.x >= b.x2 || a.y2 <= b.y || a.y >= b.y2) {
    return false;}
  else {return true;}
  }
  


class collisionManager {
  /* colliders are separated into layers so that i can filter out colliders i'm not looking for, e.g. only iterate through ground colliders*/
  private HashMap<String,ArrayList> collisionLayers;
  ArrayList<collider> everythingLayer;
  
  collisionManager() {
    collisionLayers = new HashMap<String,ArrayList>();
    collisionLayers.put("Everything",new ArrayList<collider>()); /*the list of ALL colliders*/
    everythingLayer = collisionLayers.get("Everything"); /* for convenience */
    }
  
  
  /* create a collider and add it to a layer */
  public collider newCollider(float x, float y, float w, float h, String layer) {
    collider c = new collider(x,y,w,h);
    everythingLayer.add(c); /* all colliders go in the everything layer*/
    if (layer != "Everything" && layer != "") {
      if (collisionLayers.containsKey(layer)) {
        collisionLayers.get(layer).add(c); /*add it to its respective layer list*/
      } else {
        collisionLayers.put(layer,new ArrayList<collider>()); /* if the layer doesn't exist, make it */
        collisionLayers.get(layer).add(c); 
        }
      }
    return c;
    }
    
  /* for iteration */
  public ArrayList collidersInLayer(String layer) {
    if (collisionLayers.containsKey(layer)) {
      return collisionLayers.get(layer);
      }
      
    /* the fallback */
    println("THE LAYER '" + layer + "' DOESN'T EXIST");
    return everythingLayer;
    }
  
  /* checks for any collision with one collider and all other colliders in a layer */
  public boolean collisionAtPos(collider c, String layer) {
    if (!collisionLayers.containsKey(layer)) {
      println("THE LAYER '" + layer + "' DOESN'T EXIST");
      return false;
      }
    
    ArrayList<collider> colliders = collidersInLayer(layer);
    for(collider col : colliders) {
      if (collidersIntersect(c, col)) {
        return true;
        }
      }
    return false;
    }
  
  }


class collider {
  public float x, y, w, h;
  public float x2, y2;
  
  collider(float x, float y, float w, float h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.x2 = x+w;
    this.y2 = y+h;
    }
        
  /* relative movement*/
  public void move(float relx, float rely) {
    x += relx;
    y += rely;
    x2 = x+w;
    y2 = y+h;
    }
    
  /* absolute movement*/
  public void setPosition(float x, float y) {
    this.x = x;
    this.y = y;
    x2 = x+w;
    y2 = y+h;
    }
    
  /* generates a new collider based on the current one at a new location, usefull for predictive collision checking */
  public collider atOffset(float ox, float oy) {
    return new collider(x+ox,y+oy,w,h);
    }
    
  public void render() {
    rect(x,y,w,h);
    }
  
  }
/* A small interface that both the old and new game manager can be used through */
interface gameManager {
      
  public void update();
  public void render();
  public void handleInputs();
  public void cleanup();
  }
/*
  I made my own system for handling keyboard inputs because processing's default system doesn't handle holding keys well and only processes one key at a time;
*/


/*

  The game manager for the game selection menu

*/

class menuGameManager implements gameManager {
  int BG_COLOUR_1 = color(0xff0D0D09);
  int BG_COLOUR_2 = color(0xff191919);
  float menuFloatDegrees = 0;
  float menuFloatLength = 16;
  
  float selectionOffset = 0;
  int selection = 0;
  
  int optionDistance = 32;
  int option1Y = height/2+48;
  int option2Y = option1Y+optionDistance;
   
  PFont menuFont;
  PFont subtitleFont;
  PFont optionFont;
  
  button upKey, downKey, enterKey, returnKey;

  menuGameManager() {
    menuFont = loadFont("PixelOperatorMono8-Bold-64.vlw"); //PixelOperator and is a public domain font, which can be found at  https://www.dafont.com/pixel-operator.font
    subtitleFont = loadFont("PixelOperatorMono8-Bold-32.vlw");
    optionFont = loadFont("PixelOperatorMono8-Bold-16.vlw");
    
    upKey = input.defineButton(UP);
    downKey = input.defineButton(DOWN);
    /* Windows uses ENTER but mac uses RETURN, safest to use BOTH*/
    enterKey = input.defineButton(ENTER);
    returnKey = input.defineButton(RETURN);
    
    
    };
  
  public void update() {
    menuFloatDegrees+=1;
    if (menuFloatDegrees>360) {menuFloatDegrees = 0;}
   
    
    /* averaging for a smooth transition */
    selectionOffset = ((selectionOffset*3)+selection)/4;
    
    };
  public void render() {
    /* fancy rendering of the menu text */
    
    
    textAlign(CENTER,CENTER);
    
    int degreeSpacing = 5;
    int iterations = 15;
    
    background(lerpColor(BG_COLOUR_1,BG_COLOUR_2,(dsin(menuFloatDegrees/2))));

    for(int i = iterations; i>= 0; i--) {
      float a = (255*(i/((float)iterations))); 
      fill(255-(a*1.2f),255-a*1.2f);
      textFont(menuFont);
      text("HELMET",width/2,64+(dsin(menuFloatDegrees-(degreeSpacing*i))*menuFloatLength)); /* draw the text*/
      textFont(subtitleFont);
      fill(128-(a*1.2f),128-a*1.2f);
      text("Assignment 2",width/2,112+(dsin(menuFloatDegrees-(degreeSpacing*i))*menuFloatLength)); /* draw the text*/
      }
   
   textFont(optionFont);
   
   fill(255-(128*selectionOffset));
   text("CLASSIC MODE (CRITERIA)",width/2,option1Y-(optionDistance*selectionOffset));
   
   fill(128+(128*selectionOffset));
   text("ULTRA MODE (SHOWING OFF)",width/2,option2Y-(optionDistance*selectionOffset));
   
   
   
   fill(255);
   
   text("PRESS M TO RETURN TO MENU",width/2,height-16);
   
   int leftX = 16;
   int rightX = width-16;
   
   triangle(leftX,option1Y+(16*dsin(menuFloatDegrees*2)),leftX,option1Y-(16*dsin(menuFloatDegrees*2)),leftX+28,option1Y);
   triangle(rightX,option1Y+(16*dsin(menuFloatDegrees*2)),rightX,option1Y-(16*dsin(menuFloatDegrees*2)),rightX-28,option1Y);
   
   textAlign(LEFT,TOP);
   
    };
  public void handleInputs() {
    if (upKey.pressed && selection == 0) {selection = 1;}
    if (downKey.pressed && selection == 1) {selection = 0;}
    if (enterKey.pressed || returnKey.pressed) {
      if (selection == 0) {changeGameManager(new classicGameManager());}
       else {changeGameManager(new ultraGameManager());}
      }
    };
    
    
  public void cleanup() {
    input.delButton(upKey);
    input.delButton(downKey);
    input.delButton(enterKey);
    input.delButton(returnKey);
    };
  }
static float PLAYER_WIDTH = 44;
static float PLAYER_HEIGHT = 65;
/*
static float PLAYER_MAX_SPEED = 5;
static float PLAYER_ACCEL = 0.5;
static float GROUND_FRICTION = 0.3;
static float GRAVITY_FORCE = 0.5;
static float JUMP_STRENGTH = -8;
*/
static float PLAYER_MAX_SPEED = 7;
static float PLAYER_ACCEL = 1;
static float GROUND_FRICTION = 0.6f;
static float GRAVITY_FORCE = 0.98f;
static float JUMP_STRENGTH = -16;

class playerClass {
  public float x, y;
  private float subX = 0; /* for storing the subpixel offsets  */
  private float subY = 0;
  public float xSpeed = 0;
  public float ySpeed = 0;
  public collider bbox; /* players bounding box collider */
  
  private PShape currentFrame = leftFrame;
  
  private float maxSwaptimer = 45; /*frames between image swaps*/
  private float swapTimer = maxSwaptimer;
  
  private boolean onGround = false;
  
  private button leftKey, rightKey, jumpKey;
  
  playerClass(float x, float y) {
    this.x = x;
    this.y = y;
    this.bbox = cm.newCollider(x,y,PLAYER_WIDTH,PLAYER_HEIGHT,"Player");
    
    leftKey = input.defineButton(LEFT);
    rightKey = input.defineButton(RIGHT);
    jumpKey = input.defineButton(UP);
    
    }
    
  public void update() {
    
    if (leftKey.held) {xSpeed = approach(xSpeed,-PLAYER_MAX_SPEED,PLAYER_ACCEL);}
    else if (rightKey.held) {xSpeed = approach(xSpeed,PLAYER_MAX_SPEED,PLAYER_ACCEL);}
    else {xSpeed = approach(xSpeed,0,GROUND_FRICTION);};
    
    if (xSpeed != 0) {
      swapTimer -= max(abs(xSpeed),1);
      }
      
    if (swapTimer <= 0) {
      swapTimer = maxSwaptimer;
      if (currentFrame == leftFrame) {currentFrame = rightFrame;}
      else {currentFrame = leftFrame;}
      }
    
    if (cm.collisionAtPos(bbox.atOffset(0,1),"World")) {
      onGround = true;
      } else {
      onGround = false;
      ySpeed += GRAVITY_FORCE;
      }
      
    handlePlayerPhysics();
    }
    
  public void render() {
    shape(currentFrame,x,y);
    }  
  
  public void handleKeypressActions() {
    if (onGround && jumpKey.pressed) {
      ySpeed = JUMP_STRENGTH;
      }
    }
  
  public void handlePlayerPhysics() {
    /* calculate temp speeds for movement and update subpixel offsets */
    subX += xSpeed;
    subY += ySpeed;
    int tempXSpeed = round(subX);
    int tempYSpeed = round(subY);
    subX -= tempXSpeed;
    subY -= tempYSpeed;
       
    
       
    /* collision and movement handling on the y axis */
    int sign = Integer.signum(tempYSpeed);
    while(tempYSpeed!=0) {
      if (!cm.collisionAtPos(bbox.atOffset(0,sign),"World")) {
        move(0,sign);
        tempYSpeed -= sign;
        }
      else {
        ySpeed = 0;
        break;
        }
      }
      
    /* collision and movement handling on the x axis */
    sign = Integer.signum(tempXSpeed);
    while(tempXSpeed!=0) {
      if (!cm.collisionAtPos(bbox.atOffset(sign,0),"World")) {
        move(sign,0);
        tempXSpeed -= sign;
        }
      else {
        xSpeed = 0;
        break;
        }
      }

    }
  
  /* moving the player and the bounding box in one action */
  public void move(float nx, float ny) {
    x+=nx;
    y+=ny;
    bbox.move(nx,ny);
    }
  
  }
PShape currentFrame;
PShape leftFrame; 
PShape rightFrame;

class classicPlayerClass {
  
  private int movementStepSize = 60;
  
  public float x,y;
  public int playerSlot;
  private float startX;
  
  classicPlayerClass(float x, float y) {
    this.x = x;
    this.y = y;
    playerSlot = 0;
    startX = x;
    
    
    
    currentFrame = leftFrame;
    }
  
  
  public void moveForward() {
    x += movementStepSize;
    swapFrames();
    playerSlot++;
    }
    
  public void moveBackward() {
    x -= movementStepSize;
    swapFrames();
    playerSlot--;
    }
    
  public void resetPosition() {
    playerSlot = STARTSLOT;
    x = startX;
    }
  
  public void renderPlayer() {
    shape(currentFrame,x,y);
  }
    
  public void swapFrames() {
    if (currentFrame == leftFrame) {currentFrame = rightFrame;}
    else {currentFrame = leftFrame;}
    }
   
  
  }
  
/* I wrote a small program that let me generate PShape vertex code by clicking on points on an image, so i didn't have to write it all by hand, which is why this code looks the way it does */
private void initialiseLeftFrame() {
  leftFrame.beginShape();
  
  leftFrame.vertex(21.0f,65.0f);
  leftFrame.vertex(31.0f,66.0f);
  leftFrame.vertex(31.0f,64.0f);
  leftFrame.vertex(26.0f,61.0f);
  leftFrame.vertex(26.0f,50.0f);
  leftFrame.vertex(29.0f,47.0f);
  leftFrame.vertex(28.0f,41.0f);
  leftFrame.vertex(32.0f,39.0f);
  leftFrame.vertex(36.0f,41.0f);
  leftFrame.vertex(39.0f,45.0f);
  leftFrame.vertex(44.0f,40.0f);
  leftFrame.vertex(44.0f,37.0f);
  leftFrame.vertex(41.0f,38.0f);
  leftFrame.vertex(40.0f,39.0f);
  leftFrame.vertex(35.0f,35.0f);
  leftFrame.vertex(28.0f,35.0f);
  leftFrame.vertex(26.0f,32.0f);
  leftFrame.vertex(31.0f,31.0f);
  leftFrame.vertex(31.0f,29.0f);
  leftFrame.vertex(29.0f,29.0f);
  leftFrame.vertex(26.0f,26.0f);
  leftFrame.vertex(26.0f,23.0f);
  leftFrame.vertex(29.0f,20.0f);
  leftFrame.vertex(33.0f,20.0f);
  leftFrame.vertex(36.0f,23.0f);
  leftFrame.vertex(38.0f,25.0f);
  leftFrame.vertex(41.0f,20.0f);
  leftFrame.vertex(44.0f,20.0f);
  leftFrame.vertex(44.0f,17.0f);
  leftFrame.vertex(41.0f,14.0f);
  leftFrame.vertex(40.0f,13.0f);
  leftFrame.vertex(44.0f,10.0f);
  leftFrame.vertex(44.0f,7.0f);
  leftFrame.vertex(38.0f,8.0f);
  leftFrame.vertex(36.0f,5.0f);
  leftFrame.vertex(32.0f,1.0f);
  leftFrame.vertex(24.0f,0.0f);
  leftFrame.vertex(19.0f,3.0f);
  leftFrame.vertex(14.0f,8.0f);
  leftFrame.vertex(11.0f,14.0f);
  leftFrame.vertex(13.0f,21.0f);
  leftFrame.vertex(17.0f,26.0f);
  leftFrame.vertex(23.0f,31.0f);
  leftFrame.vertex(20.0f,32.0f);
  leftFrame.vertex(16.0f,32.0f);
  leftFrame.vertex(14.0f,31.0f);
  leftFrame.vertex(10.0f,32.0f);
  leftFrame.vertex(7.0f,31.0f);
  leftFrame.vertex(7.0f,35.0f);
  leftFrame.vertex(10.0f,38.0f);
  leftFrame.vertex(12.0f,35.0f);
  leftFrame.vertex(15.0f,35.0f);
  leftFrame.vertex(18.0f,37.0f);
  leftFrame.vertex(18.0f,40.0f);
  leftFrame.vertex(13.0f,47.0f);
  leftFrame.vertex(10.0f,46.0f);
  leftFrame.vertex(5.0f,48.0f);
  leftFrame.vertex(3.0f,46.0f);
  leftFrame.vertex(1.0f,44.0f);
  leftFrame.vertex(0.0f,48.0f);
  leftFrame.vertex(3.0f,52.0f);
  leftFrame.vertex(6.0f,53.0f);
  leftFrame.vertex(8.0f,51.0f);
  leftFrame.vertex(11.0f,50.0f);
  leftFrame.vertex(14.0f,50.0f);
  leftFrame.vertex(17.0f,51.0f);
  leftFrame.vertex(20.0f,52.0f);
  leftFrame.vertex(23.0f,56.0f);
  leftFrame.vertex(23.0f,59.0f);
  leftFrame.vertex(20.0f,65.0f);
  leftFrame.endShape();
  
  leftFrame.setStroke(false);
  leftFrame.setFill(0);
  }
   
  private void initialiseRightFrame() {
    rightFrame.beginShape();
    rightFrame.vertex(14.0f,64.0f);
    rightFrame.vertex(21.0f,64.0f);
    rightFrame.vertex(25.0f,63.0f);
    rightFrame.vertex(23.0f,61.0f);
    rightFrame.vertex(23.0f,58.0f);
    rightFrame.vertex(22.0f,54.0f);
    rightFrame.vertex(25.0f,50.0f);
    rightFrame.vertex(32.0f,48.0f);
    rightFrame.vertex(38.0f,50.0f);
    rightFrame.vertex(40.0f,52.0f);
    rightFrame.vertex(45.0f,46.0f);
    rightFrame.vertex(43.0f,43.0f);
    rightFrame.vertex(40.0f,47.0f);
    rightFrame.vertex(37.0f,45.0f);
    rightFrame.vertex(32.0f,45.0f);
    rightFrame.vertex(26.0f,39.0f);
    rightFrame.vertex(26.0f,34.0f);
    rightFrame.vertex(29.0f,34.0f);
    rightFrame.vertex(32.0f,34.0f);
    rightFrame.vertex(36.0f,37.0f);
    rightFrame.vertex(38.0f,31.0f);
    rightFrame.vertex(35.0f,32.0f);
    rightFrame.vertex(32.0f,31.0f);
    rightFrame.vertex(29.0f,31.0f);
    rightFrame.vertex(25.0f,32.0f);
    rightFrame.vertex(23.0f,30.0f);
    rightFrame.vertex(26.0f,29.0f);
    rightFrame.vertex(22.0f,28.0f);
    rightFrame.vertex(20.0f,25.0f);
    rightFrame.vertex(20.0f,22.0f);
    rightFrame.vertex(23.0f,19.0f);
    rightFrame.vertex(26.0f,19.0f);
    rightFrame.vertex(29.0f,21.0f);
    rightFrame.vertex(31.0f,23.0f);
    rightFrame.vertex(35.0f,20.0f);
    rightFrame.vertex(38.0f,18.0f);
    rightFrame.vertex(38.0f,15.0f);
    rightFrame.vertex(35.0f,13.0f);
    rightFrame.vertex(32.0f,12.0f);
    rightFrame.vertex(38.0f,8.0f);
    rightFrame.vertex(38.0f,7.0f);
    rightFrame.vertex(32.0f,7.0f);
    rightFrame.vertex(29.0f,3.0f);
    rightFrame.vertex(22.0f,0.0f);
    rightFrame.vertex(16.0f,0.0f);
    rightFrame.vertex(10.0f,4.0f);
    rightFrame.vertex(7.0f,10.0f);
    rightFrame.vertex(6.0f,16.0f);
    rightFrame.vertex(9.0f,23.0f);
    rightFrame.vertex(14.0f,27.0f);
    rightFrame.vertex(18.0f,31.0f);
    rightFrame.vertex(16.0f,35.0f);
    rightFrame.vertex(13.0f,35.0f);
    rightFrame.vertex(9.0f,37.0f);
    rightFrame.vertex(5.0f,40.0f);
    rightFrame.vertex(2.0f,37.0f);
    rightFrame.vertex(0.0f,40.0f);
    rightFrame.vertex(6.0f,43.0f);
    rightFrame.vertex(8.0f,40.0f);
    rightFrame.vertex(13.0f,38.0f);
    rightFrame.vertex(17.0f,40.0f);
    rightFrame.vertex(17.0f,47.0f);
    rightFrame.vertex(19.0f,51.0f);
    rightFrame.vertex(18.0f,55.0f);
    rightFrame.vertex(20.0f,60.0f);
    rightFrame.vertex(16.0f,60.0f);
    rightFrame.vertex(14.0f,64.0f);
    rightFrame.endShape();
    
    rightFrame.setStroke(false);
    rightFrame.setFill(0);
    }

/*
static int MIN_SPEED = -8;
static int MAX_SPEED = -16;
*/
static int MIN_SPEED = -12;
static int MAX_SPEED = -24;
static float TOOL_GRAVITY = 0.6f;

class toolClass {
  public float x,y;
  private PShape sprite;
  private float ySpeed;
  
  private float rotation; 
  private float rotationDir;
  
  public collider bbox;
  
  toolClass(float x,float y) {
    this.x = x;
    this.y = y;
    
    ySpeed = randomInt(MAX_SPEED,MIN_SPEED);
    
    sprite = createShape(); /* temporary assignment */
    
    /* randomly become one of the 5 tools */
    switch(randomInt(1,5)) {
      case 1: {sprite = createBucketSprite();}break;
      case 2: {sprite = createWrenchSprite();}break;
      case 3: {sprite = createScrewdriverSprite();}break;
      case 4: {sprite = createHammerSprite();}break;
      case 5: {sprite = createPlierSprite();}break;
      }
      
    bbox = cm.newCollider(x-24,y-24,32,32,"Tools");
    }
    
  public void update() {
    sprite.rotate(radians(abs(max(2,ySpeed)*2)));
    ySpeed+=TOOL_GRAVITY;
    y += ySpeed;
    bbox.move(0,ySpeed);
    
    if (y > height+150) {ySpeed = randomInt(MAX_SPEED,MIN_SPEED);y=height+24;bbox.setPosition(x-24,y-24);}
    }
  
  public void render() {
    polygonMode();
    shape(sprite,x,y);
    }
  
  }
/*
NOTES:

Tools are 48px wide at maximum

must be drawn at +24 from offset

move downwards in 52px intervals

*/

class classicToolClass {
  final private float TOOl_ROTATIONiNTERVAL = 45.0f;
  final private float moveMinTime = 0.3f;
  final private float moveMaxTime = 0.8f;  
  
  
  public float x, y;  
  public int slot; /* which playerslot this tool is above */
  public timer dropTimer; /* tracks interval for each update */
  private PShape sprite; /* the shape used display the tool */
  
  private float rotation; /* the angle to render the tool at */
  private float rotationDir; /* how the tool will rotate every move */
  
  public int row; /* how many times the tool has gone down*/
  
  classicToolClass(int targetSlot) {
    slot = targetSlot;
    
    /* set x position based on assigned slot*/
    switch(slot) {
      case 1: {x = 116;};break;
      case 2: {x = 176;};break;
      case 3: {x = 236;};break;
      case 4: {x = 296;};break;
      case 5: {x = 356;};break;
      }
    
    y = 4;
    row = 1;
    
    dropTimer = new timer(random(moveMinTime,moveMaxTime));
        
    rotation = choose(0.0f, 90.0f, 180.0f, 270.0f);
    rotationDir = choose(TOOl_ROTATIONiNTERVAL, -TOOl_ROTATIONiNTERVAL);
    
    sprite = createShape(); /* temporary assignment */
    
    /* randomly become one of the 5 tools */
    switch(randomInt(1,5)) {
      case 1: {sprite = createBucketSprite();}break;
      case 2: {sprite = createWrenchSprite();}break;
      case 3: {sprite = createScrewdriverSprite();}break;
      case 4: {sprite = createHammerSprite();}break;
      case 5: {sprite = createPlierSprite();}break;
      }
    
    sprite.rotate(rotation);
    sprite.scale(choose(-1,1),choose(-1,1)); /* just so every shape doesn't look EXACTLY the same */
    }
    
  public void updateTool(ArrayList<classicToolClass> allTools) {
    if (dropTimer.complete()) {
      
      boolean clash = false; /* whether or not there's another tool in the same slot */
      
      /* iterate through all other tools to check if there's one in this slot and if its below us*/
      for(classicToolClass other : allTools) {
        if(other.slot != this.slot) {continue;} /* ignore tools in other rows */
        if(other.row == this.row+1) {clash = true; break;}
        }
        
      if (!clash) { /* if there's no clash, move*/
        y += 52;
        row++;
        sprite.rotate(radians(rotationDir));
        }
        
      /*regardless of a clash, start the wait timer again*/
      dropTimer.setTimer(random(moveMinTime,moveMaxTime));

      }
    
    }
  
  public void renderTool() {
    polygonMode();
    shape(sprite,x+24,y+24);
    }
    
  public void destroy() {
    this.sprite = null;
    this.dropTimer.destroy();
    }
  
  }
  
/* ###########################
   All the vertex data below this point was generated using my plotting tool, i've tried to lay it out nicely but its better left ignored...
   ########################### */
  
public PShape createBucketSprite() {
  PShape bucketShape = createShape();
  
  bucketShape.beginShape();
  bucketShape.vertex(4.0f,-16.0f);
  bucketShape.vertex(11.0f,-16.0f);
  bucketShape.vertex(16.0f,-12.0f);
  bucketShape.vertex(20.0f,-6.0f);
  bucketShape.vertex(21.0f,0.0f);
  bucketShape.vertex(19.0f,7.0f);
  bucketShape.vertex(15.0f,12.0f);
  bucketShape.vertex(9.0f,12.0f);
  bucketShape.vertex(-12.0f,11.0f);
  bucketShape.vertex(-14.0f,-8.0f);
  bucketShape.vertex(4.0f,-16.0f);
  
  bucketShape.beginContour();
  bucketShape.vertex(10.0f,-12.0f);
  bucketShape.vertex(7.0f,-11.0f);
  bucketShape.vertex(8.0f,9.0f);
  bucketShape.vertex(13.0f,8.0f);
  bucketShape.vertex(14.0f,5.0f);
  bucketShape.vertex(16.0f,-1.0f);
  bucketShape.vertex(15.0f,-7.0f);
  bucketShape.vertex(10.0f,-12.0f);
  bucketShape.endContour();
  
  bucketShape.endShape();

  
  bucketShape.setStroke(false);
  bucketShape.setFill(0);
  
  return bucketShape;
  
  }
  
public PShape createWrenchSprite() {
  PShape wrenchShape = createShape();  

  wrenchShape.beginShape();
  wrenchShape.vertex(-5.0f,0.0f);
  wrenchShape.vertex(-1.0f,-5.0f);
  wrenchShape.vertex(-3.0f,-9.0f);
  wrenchShape.vertex(-5.0f,-13.0f);
  wrenchShape.vertex(-4.0f,-18.0f);
  wrenchShape.vertex(1.0f,-21.0f);
  wrenchShape.vertex(4.0f,-20.0f);
  wrenchShape.vertex(3.0f,-13.0f);
  wrenchShape.vertex(5.0f,-12.0f);
  wrenchShape.vertex(10.0f,-16.0f);
  wrenchShape.vertex(12.0f,-12.0f);
  wrenchShape.vertex(12.0f,-8.0f);
  wrenchShape.vertex(10.0f,-3.0f);
  wrenchShape.vertex(7.0f,-2.0f);
  wrenchShape.vertex(4.0f,-2.0f);
  wrenchShape.vertex(1.0f,5.0f);
  wrenchShape.vertex(4.0f,9.0f);
  wrenchShape.vertex(4.0f,15.0f);
  wrenchShape.vertex(1.0f,19.0f);
  wrenchShape.vertex(-3.0f,19.0f);
  wrenchShape.vertex(-6.0f,17.0f);
  wrenchShape.vertex(-4.0f,12.0f);
  wrenchShape.vertex(-6.0f,9.0f);
  wrenchShape.vertex(-9.0f,13.0f);
  wrenchShape.vertex(-12.0f,12.0f);
  wrenchShape.vertex(-13.0f,6.0f);
  wrenchShape.vertex(-11.0f,3.0f);
  wrenchShape.vertex(-7.0f,0.0f);
  wrenchShape.vertex(-5.0f,0.0f);
  wrenchShape.endShape();

  wrenchShape.setStroke(false);
  wrenchShape.setFill(0);
  
  return wrenchShape;
}

public PShape createScrewdriverSprite() {
  PShape screwdriverShape = createShape();
  
  screwdriverShape.beginShape();
  screwdriverShape.vertex(4.0f,-3.0f);
  screwdriverShape.vertex(23.0f,2.0f);
  screwdriverShape.vertex(24.0f,11.0f);
  screwdriverShape.vertex(20.0f,16.0f);
  screwdriverShape.vertex(1.0f,6.0f);
  screwdriverShape.vertex(-1.0f,2.0f);
  screwdriverShape.vertex(-13.0f,-2.0f);
  screwdriverShape.vertex(-16.0f,1.0f);
  screwdriverShape.vertex(-22.0f,-4.0f);
  screwdriverShape.vertex(-20.0f,-9.0f);
  screwdriverShape.vertex(-12.0f,-10.0f);
  screwdriverShape.vertex(-12.0f,-5.0f);
  screwdriverShape.vertex(0.0f,-2.0f);
  screwdriverShape.endShape();
  
  screwdriverShape.setStroke(false);
  screwdriverShape.setFill(0);
  
  return screwdriverShape;
  }
  
public PShape createHammerSprite() {
  PShape hammerShape = createShape();
  
  hammerShape.beginShape();
  hammerShape.vertex(15.0f,-10.0f);
  hammerShape.vertex(-3.0f,11.0f);
  hammerShape.vertex(-6.0f,10.0f);
  hammerShape.vertex(-9.0f,12.0f);
  hammerShape.vertex(-3.0f,23.0f);
  hammerShape.vertex(-9.0f,28.0f);
  hammerShape.vertex(-15.0f,23.0f);
  hammerShape.vertex(-18.0f,17.0f);
  hammerShape.vertex(-21.0f,11.0f);
  hammerShape.vertex(-20.0f,5.0f);
  hammerShape.vertex(-18.0f,-1.0f);
  hammerShape.vertex(-15.0f,-1.0f);
  hammerShape.vertex(-15.0f,5.0f);
  hammerShape.vertex(-14.0f,8.0f);
  hammerShape.vertex(-10.0f,10.0f);
  hammerShape.vertex(-8.0f,6.0f);
  hammerShape.vertex(-8.0f,3.0f);
  hammerShape.vertex(10.0f,-15.0f);
  hammerShape.vertex(12.0f,-15.0f);
  hammerShape.vertex(15.0f,-12.0f);
  hammerShape.endShape();

  hammerShape.setStroke(false);
  hammerShape.setFill(0);
  
  return hammerShape;
  }
  
public PShape createPlierSprite() {
  PShape plierShape = createShape();
  

  plierShape.beginShape();
  plierShape.vertex(-13.0f,-16.0f);
  plierShape.vertex(-6.0f,-16.0f);
  plierShape.vertex(-3.0f,-12.0f);
  plierShape.vertex(4.0f,-3.0f);
  plierShape.vertex(9.0f,-3.0f);
  plierShape.vertex(15.0f,1.0f);
  plierShape.vertex(18.0f,5.0f);
  plierShape.vertex(17.0f,9.0f);
  plierShape.vertex(15.0f,9.0f);
  plierShape.vertex(14.0f,13.0f);
  plierShape.vertex(11.0f,13.0f);
  plierShape.vertex(5.0f,11.0f);
  plierShape.vertex(-1.0f,5.0f);
  plierShape.vertex(-6.0f,6.0f);
  plierShape.vertex(-10.0f,5.0f);
  plierShape.vertex(-16.0f,2.0f);
  plierShape.vertex(-19.0f,-2.0f);
  plierShape.vertex(-19.0f,-4.0f);
  plierShape.vertex(-16.0f,-5.0f);
  plierShape.vertex(-11.0f,0.0f);
  plierShape.vertex(-7.0f,1.0f);
  plierShape.vertex(-2.0f,1.0f);
  plierShape.vertex(0.0f,-1.0f);
  plierShape.vertex(-4.0f,-7.0f);
  plierShape.vertex(-7.0f,-10.0f);
  plierShape.vertex(-12.0f,-11.0f);
  plierShape.vertex(-15.0f,-13.0f);
  plierShape.vertex(-15.0f,-15.0f);
  plierShape.vertex(-13.0f,-16.0f);
  plierShape.beginContour();
  plierShape.vertex(9.0f,3.0f);
  plierShape.vertex(6.0f,4.0f);
  plierShape.vertex(7.0f,8.0f);
  plierShape.vertex(9.0f,9.0f);
  plierShape.vertex(12.0f,8.0f);
  plierShape.vertex(13.0f,5.0f);
  plierShape.vertex(11.0f,3.0f);
  plierShape.vertex(9.0f,3.0f);
  plierShape.endContour();
  plierShape.endShape();
  
  plierShape.setStroke(false);
  plierShape.setFill(0);
  
  
  
  return plierShape;
  }
static float GROUND_SCROLL_SPEED = -2.2f;
static float RESPAWN_X = 64;
static float RESPAWN_Y = 128;

collisionManager cm;
toolClass t;
ArrayList<toolClass> allTools;

int cameraX = 0;

private button returnToMenuKey;

class ultraGameManager implements gameManager {    
    
  playerClass player;
  
  int stageLength = 0;
  
  int attempts = 0;
  
  int vspeed = 0;
  int hspeed = 0;
  
  int worldSpeed = -1;
  
  ultraGameManager() {
    
    returnToMenuKey = input.defineButton('m');
    
    cm = new collisionManager();
    
    player = new playerClass(RESPAWN_X,RESPAWN_Y);
    //t = new toolClass(100,height+32);
    
    int xx = -256;
    int yy = height-48;
    
    allTools = new ArrayList<toolClass>();
    
    int ww = 512;
    
    /* level generation code, generate 20 platforms */
    for(int i = 0; i < 20; i++) {
     
      yy = height-48-randomInt(-24,24);
      cm.newCollider(xx,yy,ww,128,"World");
      xx += ww;
      
      int gap = randomInt(128,162); 
      
      allTools.add(new toolClass(xx+gap/2,height+24));
      
      xx += gap;
      ww = randomInt(32,256);
      }
      
    stageLength = xx-ww;

    }
  
  public void update() {
    
    player.update();
    
    cameraX = max(0,(int)((cameraX*6)+((player.x+(PLAYER_WIDTH/2)-width/2)*2))/8);
    
    for(toolClass t : allTools) {
      t.update();
      }
    
    if (cm.collisionAtPos(player.bbox,"Tools")) {playerDie();}
    if (player.y > height+8) {playerDie();}
    
    };
  public void render() {

    background(color(0xffb9bffb));
    
    noFill();

    stroke(255,0,0);
    
    translate(-cameraX,0);
    
    player.render();
    
    ArrayList<collider> gnd = cm.collidersInLayer("World");
    for(collider c : gnd) {
      /* dont draw platforms that are outside of the screen */
      if (c.x2 < cameraX || c.x > cameraX+width) {continue;}
      else {drawPlatform(c);} 
      }
      
      
    for(toolClass t : allTools) {
      t.render();
      }
      
 
    
    translate(cameraX,0);
    noStroke();
    fill(color(0xff849be4));
    
    textAlign(CENTER,CENTER);
    text("ATTEMPTS: " + str(attempts),width/2,16);    
    
    /* draw the progress meter */
    stroke(0);
    noFill();
    line(32,32,width-32,32);
    line(32+constrain(((width-64)*((float)player.x/stageLength)),0,width-64) ,16,32+constrain(((width-64)*((float)player.x/stageLength)),0,width-64),48);
    noStroke();
    fill(0);
    
    rect(32,28,constrain(((width-64)*((float)player.x/stageLength)),0,width-64),9);
    
    if (player.x>stageLength-256) {
      noStroke();
      fill(0);
      text("YOU WIN!",width/2,height/2);
      }
      
    };
    
  public void handleInputs() {
    player.handleKeypressActions();
    if (returnToMenuKey.pressed) {changeGameManager(new menuGameManager());return;}
    };
  
  public void cleanup() {
    /* i'm trying the GC a bit too much here because i'm too tired to finish */
    cm = null;
    allTools = null;
    player = null;
    };
  
  public void playerDie() {
    player.x = RESPAWN_X;
    player.y = RESPAWN_Y;
    
    player.bbox.setPosition(RESPAWN_X,RESPAWN_Y);
    attempts++;
    
    }
}

public void drawPlatform(collider c) {
  noStroke();
  fill(color(0xff423934));
  
  rect(c.x,c.y,c.w,c.h);
  
  for(int i = 0; i <= c.w; i++) {
    float b = sin(c.x+(i));
    stroke(lerpColor(color(0xff59c135),color(0xff1a7a3e),(b+1)/2));
    line(c.x+i,c.y,c.x+i,c.y+16+(8*b));
    }
  }

  
  
/* I chose to move this code to its own tab due to its insane size */

/* Helper functions so code isn't filled with fill and stroke functions */
public void polygonMode() {fill(0);noStroke();} /* settings for polygons */
public void lineMode() {noFill();stroke(0);strokeWeight(3);} /* settings for lines */

static int ENTRY_DOOR_X = 6;
static int ENTRY_DOOR_Y = 204;

static int EXIT_DOOR_X = 414;
static int DOOR_HEIGHT = 89;

boolean sceneryInitialised = false;

PShape exitDoor, scenery, ground, hotel;

public void initialiseScenery() {
  
  exitDoor = createShape(GROUP);
  scenery = createShape(GROUP);
  ground = createShape(GROUP);
  hotel = createShape(GROUP);;
  
  defineScenery();
  defineExitDoor();
  }

public void renderBackground(boolean doorState) {
  
  lineMode();
  
  /* starting door */
  rect(ENTRY_DOOR_X,ENTRY_DOOR_Y,48,DOOR_HEIGHT);
     
  /* generic scenery */
  shape(ground,ENTRY_DOOR_X,ENTRY_DOOR_Y+4);
  shape(hotel,ENTRY_DOOR_X,ENTRY_DOOR_Y);
  shape(scenery,414,204);
  
  polygonMode();
  rect(16,219,27,15);
  ellipse(46,254,8,8);
  
  lineMode();
  /* finishing door */
  rect(EXIT_DOOR_X,ENTRY_DOOR_Y,51,DOOR_HEIGHT);
  
  if(doorState) {shape(exitDoor,416,206);}
  else {
    polygonMode();
    rect(424,219,30,15);
    ellipse(458,254,8,8);
    }
  
  polygonMode(); /* reset render mode back to polygons for tool rendering */
  
  }
 
/* ###########################
   All the vertex data below this point was generated using my plotting tool, i've tried to lay it out nicely but its better left ignored...
   ########################### */
 
 
public void defineExitDoor() {
    
  /* Door Outline */
  PShape doorOutline = createShape();
  doorOutline.beginShape(LINES);
  doorOutline.strokeWeight(3);
  doorOutline.vertex(60.0f,-6.0f);
  doorOutline.vertex(78.0f,-12.0f);
  doorOutline.vertex(78.0f,-12.0f);
  doorOutline.vertex(77.0f,104.0f);
  doorOutline.vertex(77.0f,104.0f);
  doorOutline.vertex(58.0f,90.0f);
  doorOutline.endShape();
  exitDoor.addChild(doorOutline);
  
  /* Door Window */
  PShape doorWindow = createShape();
  doorWindow.beginShape();
  doorWindow.vertex(55.0f,15.0f);
  doorWindow.vertex(72.0f,9.0f);
  doorWindow.vertex(72.0f,33.0f);
  doorWindow.vertex(55.0f,30.0f);
  doorWindow.endShape();
  
  doorWindow.setStroke(false);
  doorWindow.setFill(0);
  
  exitDoor.addChild(doorWindow);
  
  /* Door Inner Handle */
  PShape doorInHandle = createShape(ELLIPSE, 70, 50, 8, 8);  
  doorInHandle.setStroke(false);
  doorInHandle.setFill(0);
  
  exitDoor.addChild(doorInHandle);
  
  /* Door Outer Handle */
  PShape doorOutHandle = createShape();
  doorOutHandle.beginShape(LINES);
  doorOutHandle.strokeWeight(3);
  doorOutHandle.vertex(84.0f,45.0f);
  doorOutHandle.vertex(84.0f,52.0f);
  doorOutHandle.endShape();
  exitDoor.addChild(doorOutHandle);
  }
  
public void defineScenery() {
  
  
  PShape houseRoof = createShape();
  houseRoof.beginShape();
  houseRoof.vertex(102.0f,-20.0f);
  houseRoof.vertex(67.0f,-33.0f);
  houseRoof.vertex(-17.0f,6.0f);
  houseRoof.vertex(-17.0f,-6.0f);
  houseRoof.vertex(-20.0f,-7.0f);
  houseRoof.vertex(-19.0f,-14.0f);
  houseRoof.vertex(67.0f,-54.0f);
  houseRoof.vertex(102.0f,-42.0f);
  houseRoof.endShape();
  houseRoof.setStroke(false);
  houseRoof.setFill(0);
  
  
  PShape houseWall = createShape();
  houseWall.beginShape(LINES);
  houseWall.strokeWeight(3);
  houseWall.vertex(-7.0f,97.0f);
  houseWall.vertex(-7.0f,0.0f);
  houseWall.endShape();
  
  
  PShape houseTree1 = createShape();
  houseTree1.beginShape();
  houseTree1.vertex(1.0f,-23.0f);
  houseTree1.vertex(10.0f,-32.0f);
  houseTree1.vertex(1.0f,-31.0f);
  houseTree1.vertex(11.0f,-44.0f);
  houseTree1.vertex(1.0f,-45.0f);
  houseTree1.vertex(13.0f,-56.0f);
  houseTree1.vertex(4.0f,-56.0f);
  houseTree1.vertex(16.0f,-71.0f);
  houseTree1.vertex(7.0f,-70.0f);
  houseTree1.vertex(17.0f,-84.0f);
  houseTree1.vertex(12.0f,-86.0f);
  houseTree1.vertex(19.0f,-96.0f);
  houseTree1.vertex(15.0f,-95.0f);
  houseTree1.vertex(26.0f,-110.0f);
  houseTree1.vertex(32.0f,-100.0f);
  houseTree1.vertex(37.0f,-95.0f);
  houseTree1.vertex(30.0f,-96.0f);
  houseTree1.vertex(36.0f,-88.0f);
  houseTree1.vertex(33.0f,-86.0f);
  houseTree1.vertex(40.0f,-73.0f);
  houseTree1.vertex(34.0f,-75.0f);
  houseTree1.vertex(49.0f,-61.0f);
  houseTree1.vertex(39.0f,-59.0f);
  houseTree1.vertex(49.0f,-47.0f);
  houseTree1.vertex(42.0f,-48.0f);
  houseTree1.vertex(47.0f,-43.0f);
  houseTree1.vertex(1.0f,-20.0f);
  houseTree1.endShape();  
  houseTree1.setStroke(false);
  houseTree1.setFill(color(86,130,38));
    
  PShape houseTree2 = createShape();
  houseTree2.beginShape();
  houseTree2.vertex(72.0f,-49.0f);
  houseTree2.vertex(54.0f,-52.0f);
  houseTree2.vertex(72.0f,-63.0f);
  houseTree2.vertex(63.0f,-69.0f);
  houseTree2.vertex(73.0f,-79.0f);
  houseTree2.vertex(65.0f,-84.0f);
  houseTree2.vertex(78.0f,-93.0f);
  houseTree2.vertex(71.0f,-99.0f);
  houseTree2.vertex(79.0f,-105.0f);
  houseTree2.vertex(72.0f,-112.0f);
  houseTree2.vertex(84.0f,-120.0f);
  houseTree2.vertex(77.0f,-123.0f);
  houseTree2.vertex(84.0f,-127.0f);
  houseTree2.vertex(90.0f,-133.0f);
  houseTree2.vertex(96.0f,-126.0f);
  houseTree2.vertex(100.0f,-123.0f);
  houseTree2.vertex(99.0f,-40.0f);
  houseTree2.vertex(72.0f,-49.0f);
  houseTree2.endShape();
  houseTree2.setStroke(false);
  houseTree2.setFill(color(86,130,38));
  
  PShape houseTree3 = createShape();
  houseTree3.beginShape();
  houseTree3.vertex(43.0f,-38.0f);
  houseTree3.vertex(26.0f,-39.0f);
  houseTree3.vertex(48.0f,-48.0f);
  houseTree3.vertex(26.0f,-50.0f);
  houseTree3.vertex(45.0f,-63.0f);
  houseTree3.vertex(29.0f,-66.0f);
  houseTree3.vertex(49.0f,-78.0f);
  houseTree3.vertex(41.0f,-81.0f);
  houseTree3.vertex(51.0f,-87.0f);
  houseTree3.vertex(42.0f,-89.0f);
  houseTree3.vertex(51.0f,-96.0f);
  houseTree3.vertex(44.0f,-99.0f);
  houseTree3.vertex(55.0f,-110.0f);
  houseTree3.vertex(64.0f,-99.0f);
  houseTree3.vertex(57.0f,-97.0f);
  houseTree3.vertex(69.0f,-89.0f);
  houseTree3.vertex(59.0f,-88.0f);
  houseTree3.vertex(72.0f,-77.0f);
  houseTree3.vertex(57.0f,-77.0f);
  houseTree3.vertex(72.0f,-65.0f);
  houseTree3.vertex(58.0f,-64.0f);
  houseTree3.vertex(72.0f,-56.0f);
  houseTree3.vertex(60.0f,-53.0f);
  houseTree3.vertex(78.0f,-41.0f);
  houseTree3.endShape();
  houseTree3.setStroke(false);
  houseTree3.setFill(color(73,110,32));
  
   
  PShape groundLines1 = createShape();
  
  groundLines1.beginShape(LINES);
  groundLines1.strokeWeight(3);
  groundLines1.vertex(36.0f,92.0f);
  groundLines1.vertex(43.0f,90.0f);
  groundLines1.vertex(43.0f,90.0f);
  groundLines1.vertex(52.0f,92.0f);
  groundLines1.vertex(51.0f,92.0f);
  groundLines1.vertex(61.0f,88.0f);
  groundLines1.vertex(60.0f,88.0f);
  groundLines1.vertex(67.0f,91.0f);
  groundLines1.vertex(67.0f,91.0f);
  groundLines1.vertex(76.0f,88.0f);
  groundLines1.vertex(126.0f,90.0f);
  groundLines1.vertex(130.0f,91.0f);
  groundLines1.vertex(130.0f,91.0f);
  groundLines1.vertex(135.0f,88.0f);
  groundLines1.vertex(135.0f,88.0f);
  groundLines1.vertex(141.0f,88.0f);
  groundLines1.vertex(141.0f,88.0f);
  groundLines1.vertex(145.0f,91.0f);
  groundLines1.vertex(145.0f,91.0f);
  groundLines1.vertex(150.0f,88.0f);
  groundLines1.vertex(150.0f,88.0f);
  groundLines1.vertex(157.0f,88.0f);
  groundLines1.vertex(157.0f,88.0f);
  groundLines1.vertex(160.0f,91.0f);
  groundLines1.vertex(160.0f,91.0f);
  groundLines1.vertex(165.0f,91.0f);
  groundLines1.vertex(187.0f,91.0f);
  groundLines1.vertex(195.0f,87.0f);
  groundLines1.vertex(195.0f,87.0f);
  groundLines1.vertex(201.0f,86.0f);
  groundLines1.vertex(211.0f,85.0f);
  groundLines1.vertex(214.0f,89.0f);
  groundLines1.vertex(214.0f,89.0f);
  groundLines1.vertex(219.0f,91.0f);
  groundLines1.vertex(261.0f,88.0f);
  groundLines1.vertex(267.0f,85.0f);
  groundLines1.vertex(267.0f,85.0f);
  groundLines1.vertex(282.0f,85.0f);
  groundLines1.vertex(282.0f,85.0f);
  groundLines1.vertex(288.0f,88.0f);
  groundLines1.vertex(317.0f,88.0f);
  groundLines1.vertex(327.0f,90.0f);
  groundLines1.vertex(327.0f,90.0f);
  groundLines1.vertex(334.0f,92.0f);
  groundLines1.vertex(334.0f,92.0f);
  groundLines1.vertex(342.0f,94.0f);
  groundLines1.vertex(342.0f,94.0f);
  groundLines1.vertex(349.0f,90.0f);
  groundLines1.vertex(349.0f,90.0f);
  groundLines1.vertex(357.0f,90.0f);
  groundLines1.vertex(357.0f,90.0f);
  groundLines1.vertex(364.0f,94.0f);
  groundLines1.vertex(364.0f,94.0f);
  groundLines1.vertex(369.0f,91.0f);
  groundLines1.vertex(369.0f,91.0f);
  groundLines1.vertex(378.0f,90.0f);
  groundLines1.vertex(378.0f,90.0f);
  groundLines1.vertex(388.0f,94.0f);
  groundLines1.endShape();
  groundLines1.setStroke(color(188,121,107));
  groundLines1.setFill(false);
  
  PShape groundLines2 = createShape();
  groundLines2.beginShape(LINES);
  groundLines2.strokeWeight(3);
  groundLines2.vertex(38.0f,99.0f);
  groundLines2.vertex(46.0f,98.0f);
  groundLines2.vertex(46.0f,98.0f);
  groundLines2.vertex(55.0f,102.0f);
  groundLines2.vertex(55.0f,102.0f);
  groundLines2.vertex(62.0f,98.0f);
  groundLines2.vertex(62.0f,98.0f);
  groundLines2.vertex(70.0f,100.0f);
  groundLines2.vertex(70.0f,100.0f);
  groundLines2.vertex(73.0f,96.0f);
  groundLines2.vertex(73.0f,96.0f);
  groundLines2.vertex(79.0f,96.0f);
  groundLines2.vertex(125.0f,96.0f);
  groundLines2.vertex(130.0f,99.0f);
  groundLines2.vertex(130.0f,99.0f);
  groundLines2.vertex(137.0f,96.0f);
  groundLines2.vertex(137.0f,96.0f);
  groundLines2.vertex(145.0f,100.0f);
  groundLines2.vertex(145.0f,100.0f);
  groundLines2.vertex(151.0f,100.0f);
  groundLines2.vertex(151.0f,100.0f);
  groundLines2.vertex(154.0f,98.0f);
  groundLines2.vertex(154.0f,98.0f);
  groundLines2.vertex(160.0f,98.0f);
  groundLines2.vertex(160.0f,98.0f);
  groundLines2.vertex(164.0f,100.0f);
  groundLines2.vertex(164.0f,100.0f);
  groundLines2.vertex(170.0f,100.0f);
  groundLines2.vertex(170.0f,100.0f);
  groundLines2.vertex(173.0f,98.0f);
  groundLines2.vertex(173.0f,98.0f);
  groundLines2.vertex(181.0f,99.0f);
  groundLines2.vertex(133.0f,105.0f);
  groundLines2.vertex(142.0f,106.0f);
  groundLines2.vertex(142.0f,106.0f);
  groundLines2.vertex(144.0f,109.0f);
  groundLines2.vertex(161.0f,107.0f);
  groundLines2.vertex(173.0f,109.0f);
  groundLines2.vertex(173.0f,109.0f);
  groundLines2.vertex(175.0f,105.0f);
  groundLines2.vertex(175.0f,105.0f);
  groundLines2.vertex(179.0f,105.0f);
  groundLines2.vertex(191.0f,98.0f);
  groundLines2.vertex(196.0f,98.0f);
  groundLines2.vertex(196.0f,98.0f);
  groundLines2.vertex(197.0f,95.0f);
  groundLines2.vertex(197.0f,95.0f);
  groundLines2.vertex(203.0f,93.0f);
  groundLines2.vertex(203.0f,93.0f);
  groundLines2.vertex(206.0f,96.0f);
  groundLines2.vertex(206.0f,96.0f);
  groundLines2.vertex(211.0f,93.0f);
  groundLines2.vertex(211.0f,93.0f);
  groundLines2.vertex(215.0f,97.0f);
  groundLines2.vertex(215.0f,98.0f);
  groundLines2.vertex(220.0f,99.0f);
  groundLines2.vertex(220.0f,99.0f);
  groundLines2.vertex(221.0f,96.0f);
  groundLines2.vertex(221.0f,96.0f);
  groundLines2.vertex(230.0f,95.0f);
  groundLines2.vertex(221.0f,105.0f);
  groundLines2.vertex(228.0f,102.0f);
  groundLines2.vertex(263.0f,94.0f);
  groundLines2.vertex(271.0f,93.0f);
  groundLines2.vertex(271.0f,93.0f);
  groundLines2.vertex(277.0f,92.0f);
  groundLines2.vertex(277.0f,92.0f);
  groundLines2.vertex(281.0f,96.0f);
  groundLines2.vertex(281.0f,96.0f);
  groundLines2.vertex(289.0f,96.0f);
  groundLines2.vertex(289.0f,96.0f);
  groundLines2.vertex(290.0f,96.0f);
  groundLines2.vertex(316.0f,99.0f);
  groundLines2.vertex(325.0f,98.0f);
  groundLines2.vertex(325.0f,98.0f);
  groundLines2.vertex(332.0f,102.0f);
  groundLines2.vertex(332.0f,102.0f);
  groundLines2.vertex(345.0f,101.0f);
  groundLines2.vertex(345.0f,101.0f);
  groundLines2.vertex(355.0f,98.0f);
  groundLines2.vertex(355.0f,98.0f);
  groundLines2.vertex(361.0f,99.0f);
  groundLines2.vertex(361.0f,99.0f);
  groundLines2.vertex(367.0f,102.0f);
  groundLines2.vertex(367.0f,102.0f);
  groundLines2.vertex(374.0f,99.0f);
  groundLines2.vertex(374.0f,99.0f);
  groundLines2.vertex(381.0f,98.0f);
  groundLines2.vertex(381.0f,98.0f);
  groundLines2.vertex(388.0f,102.0f);
  groundLines2.vertex(316.0f,107.0f);
  groundLines2.vertex(320.0f,107.0f);
  groundLines2.vertex(320.0f,107.0f);
  groundLines2.vertex(324.0f,110.0f);
  groundLines2.vertex(352.0f,108.0f);
  groundLines2.vertex(358.0f,107.0f);
  groundLines2.vertex(358.0f,107.0f);
  groundLines2.vertex(359.0f,108.0f);
  groundLines2.endShape();
  groundLines2.setStroke(color(152,127,123));
  groundLines2.setFill(false);
  
  
  
  PShape hotel1 = createShape();
  hotel1.beginShape();
  hotel1.vertex(-7.0f,-11.0f);
  hotel1.vertex(0.0f,-10.0f);
  hotel1.vertex(0.0f,-17.0f);
  hotel1.vertex(41.0f,-17.0f);
  hotel1.vertex(41.0f,-24.0f);
  hotel1.vertex(2.0f,-25.0f);
  hotel1.vertex(1.0f,-80.0f);
  hotel1.vertex(39.0f,-79.0f);
  hotel1.vertex(39.0f,-86.0f);
  hotel1.vertex(1.0f,-87.0f);
  hotel1.vertex(1.0f,-135.0f);
  hotel1.vertex(40.0f,-136.0f);
  hotel1.vertex(40.0f,-146.0f);
  hotel1.vertex(-7.0f,-146.0f);
  hotel1.endShape();
  hotel1.setStroke(false);
  hotel1.setFill(color(137,66,37));

  PShape hotel2 = createShape();
  hotel2.beginShape();
  hotel2.vertex(-7.0f,5.0f);
  hotel2.vertex(-1.0f,5.0f);
  hotel2.vertex(-2.0f,-4.0f);
  hotel2.vertex(41.0f,-4.0f);
  hotel2.vertex(40.0f,-20.0f);
  hotel2.vertex(-1.0f,-21.0f);
  hotel2.vertex(-2.0f,-66.0f);
  hotel2.vertex(40.0f,-64.0f);
  hotel2.vertex(40.0f,-82.0f);
  hotel2.vertex(-2.0f,-81.0f);
  hotel2.vertex(-2.0f,-129.0f);
  hotel2.vertex(38.0f,-129.0f);
  hotel2.vertex(37.0f,-140.0f);
  hotel2.vertex(-7.0f,-141.0f);
  hotel2.endShape();
  hotel2.setStroke(false);
  hotel2.setFill(color(174,140,102));
  
  PShape hotel3 = createShape();
  hotel3.beginShape(LINES);
  hotel3.strokeWeight(3);
  hotel3.vertex(1.0f,-46.0f);
  hotel3.vertex(35.0f,-46.0f);
  hotel3.vertex(32.0f,-24.0f);
  hotel3.vertex(32.0f,-45.0f);
  hotel3.vertex(25.0f,-24.0f);
  hotel3.vertex(25.0f,-45.0f);
  hotel3.vertex(16.0f,-23.0f);
  hotel3.vertex(16.0f,-45.0f);
  hotel3.vertex(8.0f,-23.0f);
  hotel3.vertex(7.0f,-45.0f);
  hotel3.vertex(0.0f,-107.0f);
  hotel3.vertex(37.0f,-107.0f);
  hotel3.vertex(33.0f,-84.0f);
  hotel3.vertex(33.0f,-107.0f);
  hotel3.vertex(25.0f,-84.0f);
  hotel3.vertex(25.0f,-108.0f);
  hotel3.vertex(16.0f,-84.0f);
  hotel3.vertex(17.0f,-107.0f);
  hotel3.vertex(9.0f,-84.0f);
  hotel3.vertex(9.0f,-107.0f);
  hotel3.endShape();
  hotel3.setStroke(color(167,126,114));
  hotel3.setFill(false);

  /* grouped in intentional draw order*/
  scenery.addChild(houseTree3);
  scenery.addChild(houseTree1);
  scenery.addChild(houseTree2);
  scenery.addChild(houseWall);  
  scenery.addChild(houseRoof);

  ground.addChild(groundLines1);
  ground.addChild(groundLines2);
  
  hotel.addChild(hotel3);
  hotel.addChild(hotel2);
  hotel.addChild(hotel1);

  }
/* A collection of misc helper functions*/

/* slightly shorter than int(random(a,b+1)), and all inclusive */
public int randomInt(int a, int b) {
  return PApplet.parseInt(random(a,b+1));
  }
  
/* randomly returns one of the provided arguments, very useful for selecting from set options */
<Type> Type choose(Type ...args) {
  return args[randomInt(0,args.length-1)];
  }
  
/* does what it says on the tin :/ */
public void gradientCircle(float x, float y, float r, int c) {
  final int divisions = 10;
  final float separation = r/divisions;  
  noStroke();
  for(float i = 0.0f; i < divisions; i++) {
    //redefine the input colour with an alpha component for stacking circles
    fill(color(red(c),blue(c),green(c),(255*((1+i)/divisions))));
    ellipse(x,y,r-(separation*i),r-(separation*i));
    }
  }
  
/* trig in degrees because radians are inconvenient */
public float dsin(float degrees) {
  return sin(radians(degrees));
  }
  
public float dcos(float degrees) {
  return cos(radians(degrees));
  }
  
/* from value, approach target in intervals of step without overshooting */
public float approach(float value, float target, float step) {
  if (value > target) {return max(value-step,target);}
  else if (value < target) {return min(value+step,target);}
  else {return target;}
  }
  
public int approach(int value, int target, int step) {
  if (value > target) {return max(value-step,target);}
  else if (value < target) {return min(value+step,target);}
  else {return target;}
  }
  public void settings() {  size(512,348); }
  
}
