import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 
import cart.Cart;

public class KatyBreakwellAssignmentTwo extends Cart {

//General variables.
PImage backImage;
int colWidth;
int points = 0;
int livesLost = 0;
//Variables for door.
boolean door = true;      
PImage doorImage;
long doorLast;  
long doorTimer = PApplet.parseInt(random(3000,8000));
//Variables for obstacles (tools).
Obstacle mushroom;
PImage mushroomImage;
int mushroomX;
int mushroomY = 0; 
int mushroomPending = 0;
int mushroomVel = 1;
Obstacle spider;
PImage spiderImage;
int spiderX;
int spiderY = 0;
int spiderPending = 0;
int spiderVel = 2;
int jump = 60;
//Variables for mrGW.
PImage earImage;        
int mrGWCol = 1;
int mrGWX;


public void setup() {
  
  ellipseMode(CENTER);
  rectMode(CENTER);
  backImage = loadImage("backImage.png");
  doorImage = loadImage("Portal.png");
  mushroomImage = loadImage("Mushroom.png");
  spiderImage = loadImage("Spider.png");
  earImage = loadImage("earImage.png");
  colWidth = width/8;
  mushroomX = 3 * colWidth;
  spiderX = 5 * colWidth;
  mushroom = new Obstacle("Mushroom", mushroomX, mushroomY, mushroomPending, mushroomVel);
  spider = new Obstacle("Spider", spiderX, spiderY, spiderPending, spiderVel);
}

public void draw() {
  mrGWX = mrGWCol * colWidth;
  background(backImage);
  
  //Calls the function displayDoor() which opens and closes the portal in the tree.
  displayDoor();
  
  //Calls the function displayMrGW() which draws mrGW on the screen.
  displayMrGW(mrGWX);
  
  /*Calls the display() and move() function from the clas Obstacle which draws the obstacles on the screen 
  and moves them down the screen in jumps at regular intervals. It resets mrGW if he is 'hit' by the obstacle,
  and resets the obstacle to the top of the screen if it 'hits' mrGW or reaches the bottom of the window.
  It also keeps track of the number of times mrGW was 'hit'and the number of times the obstacle reached the 
  bottom of the window unimpeded.*/
  mushroom.display();
  spider.display();
  mushroom.move();
  spider.move();
  
  //Calls the function displayScore() which will draw red circles for lost lives and black circles for points gained.
  displayScore(livesLost, points);
}


/*Moves mrGW across the screen when the arrow keys are pressed, and resets mrGW to starting position when he moves 
through the portal. Increases score by 3 points every time mrGW makes it through the portal.*/
public void keyPressed() {
	if (key == ESC) {key = 0; eject();};
  if (keyCode == RIGHT) {
    mrGWCol = constrain(mrGWCol + 1, 1, door ? 8 : 6);
  }
  if (keyCode == LEFT) {
    mrGWCol = constrain(mrGWCol - 1, 1, door ? 8 : 6);
  }
  if (keyCode == RIGHT && mrGWCol == 8) {
    mrGWCol = 1;
    points += 3;
  }
}
/* The code used for creating and moving obstacles is based on ideas and code by Matthew Roberts.
The original code can be found at http://ilearn.mq.edu.au/mod/resource/view.php?id=4591562.
I found it very difficult to avoid repeating code for the obstacles using only things we've learned in lectures and pracs,
so I created the class Obstacle. I have included two functions within this class, the function display() which draws an image
of the obstacle, and the function move() which contains all of the instructions for moving the obstacles. The movement of
the obstacles is based on Matt's code (see above), but I added a conditional to reset the obstacle if it reached the bottom of 
the window unimpeded, and increase the variable points by 1.*/

class Obstacle {
  PImage image;
  int obX;
  int obY;
  int obPend;
  int obVel;
  
  Obstacle(String tempImageName, int tempObX, int tempObY, int tempObPend, int tempObVel) {
    String tempImage = tempImageName + ".png";
    image = loadImage(tempImage);
    obX = tempObX;
    obY = tempObY;
    obPend = tempObPend;
    obVel = tempObVel;
  }
  
  public void display() {
    imageMode(CENTER);
    image(image,obX,obY);
    imageMode(CORNER);
  }
  
  public void move() {
    obPend = obPend + obVel;
    if (obPend > jump) {
      obY = obY + obPend;
      obPend = 0;
    }
    if (mrGWX == obX && obY >= 240 && obY <=320) {
      mrGWCol = 1;
      obY = 0;
      livesLost += 1;
    }
        if (obY > 360) {
      obY = 0;
      obPend = 0;
      points += 1;
    }
  }
}
//Will open or close the portal in the tree every 3-8 seconds.

public void displayDoor() {
  int doorX = 413;
  int doorY = 179;
  int now = millis();
  long elapsed = now - doorLast;
  if (door) {
    image(doorImage,doorX,doorY);
  }
  if (elapsed > doorTimer && mrGWCol != 7) {
    door = !door;
    doorLast = now;
    doorTimer = PApplet.parseInt(random(3000,8000));
  }
}
//Draws mrGW on the screen.

public void displayMrGW(int pMrGWX) {
  int mrGWY = 250;
  noStroke();
  fill(0);
  ellipse(pMrGWX,mrGWY-23,35,35); //head
  ellipse(pMrGWX+18,mrGWY-28,12,12); //nose
  rect(pMrGWX,mrGWY-10,5,15); //neck
  image(earImage,pMrGWX-38,mrGWY-45);
  ellipse(pMrGWX,mrGWY+10,22,30); //upper body
  ellipse(pMrGWX,mrGWY+23,30,30); //lower body
  quad(pMrGWX-8,mrGWY,pMrGWX-25,mrGWY+10,pMrGWX-25,mrGWY+15,pMrGWX-9,mrGWY+5); //left arm
  quad(pMrGWX+8,mrGWY,pMrGWX+25,mrGWY+10,pMrGWX+25,mrGWY+15,pMrGWX+9,mrGWY+5); //right arm
  ellipse(pMrGWX-27,mrGWY+13,12,8); //left hand
  ellipse(pMrGWX+27,mrGWY+13,12,8); //right hand
  quad(pMrGWX-10,mrGWY+25,pMrGWX-15,mrGWY+55,pMrGWX-11,mrGWY+55,pMrGWX-6,mrGWY+30); //left leg
  quad(pMrGWX+10,mrGWY+25,pMrGWX+15,mrGWY+55,pMrGWX+11,mrGWY+55,pMrGWX+6,mrGWY+30); //right leg
  ellipse(pMrGWX-18,mrGWY+58,18,12); //left foot
  ellipse(pMrGWX+18,mrGWY+58,18,12); //right foot
}
/*Uses the variables livesLost and score to draw circles on the screen. The circles are displayed in a 6 by 4 grid, so a maximum of 24 circles
can be displayed on the screen. When a life is lost (mrGW is 'hit'by an obstacle), 6 red gradient circles appear at the top of the screen. When an
obstacle reaches the bottom of the screen (1 point), or mrGW moves through the portal in the tree (3 points), black circles will be drawn for each point 
gained. The black circles will be drawn left to right, top to bottom on the screen, and will be drawn underneath any red circle rows. If the total number 
of circles to be drawn is above 24, the game will call the function reset(). */

public void displayScore(int pLivesLost, int pPoints) {
  int gridWidth = width/6;
  int gridHeight = height/4;
  int circleOffsetX = width/12;
  int circleOffsetY = height/8;
  for(int a = 0; a < pLivesLost; a += 1) {
    for(int b = circleOffsetX; b < width; b += gridWidth) {
      gradientCircle(b, circleOffsetY + a * gridHeight, color(255,0,0), 50);
    }
  }
  for(int c = 0; c < pPoints; c += 1) {
    gradientCircle(circleOffsetX + (c%6 * gridWidth), circleOffsetY + (pLivesLost * gridHeight) + (PApplet.parseInt(c/6) * gridHeight), color(0,0,0), 50);
  }
  if (pLivesLost*6 + pPoints > 24) {
  reset();
  }
}
/*Draws 25 concentric ellipses, with each ellipse measuring 5 pixels less in diameter.
The parameters required are the X coordinate and Y coordinate of the ellipse, the colour,
and the opacity. The function displayScore() calls gradientCircle() twice, with an opacity
of 50. As the concentric circles are drawn, the centre of the circles seems to be entirely opaque,
while the outside of the circles remains slightly transperent giving the impression of a single, 
gradient circle.*/

public void gradientCircle(int circleX, int circleY, int colour, int opacity) {
  fill(colour, opacity);
  int diameter = width/7;
  for(int i = 0; i < 25; i += 1) {
    ellipse(circleX,circleY,diameter,diameter);
    diameter = diameter - 5;
  }
}
//resets all necessary variables to restart the game.

public void reset() {
  livesLost = 0;
  points = 0;
  mrGWCol = 1;
  mushroomPending = 0;
  mushroomY = 0;
  spiderPending = 0;
  spiderY = 0;
}
  public void settings() {  size(512,348); }
  
}
