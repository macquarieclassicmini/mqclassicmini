package cart;

import java.lang.reflect.Method;
import processing.core.PApplet;

public class Cart extends PApplet{
	
	Object parentManager;
	PApplet cartSketch;
	
	public Cart() {};
	
	public Cart(PApplet sketch) {
		cartSketch = sketch;
	}
	
	public void boot(String[] args, Object manager) {
		parentManager = manager;
		PApplet.runSketch(args, this);
	};
	
	public Cart getCart() {
		return (Cart) this;
	}
	
	public PApplet getSketch() {
		return cartSketch;
	};
		
	
	public void eject() {
		Method ejectMethod = null;
		try {
			ejectMethod = parentManager.getClass().getMethod("onEject", (Class<?>[])null);
			ejectMethod.invoke(parentManager);
		} catch (Exception e) {e.printStackTrace();}
				
		this.surface.setVisible(false);
		this.frame.setEnabled(false);
		this.frame.dispose();
		this.stop();
	};
}

