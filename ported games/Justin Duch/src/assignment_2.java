import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 
import cart.Cart;
public class assignment_2 extends Cart {

// notes:

// to-do: fix everything that is broken

// number of times i've type ":w" or ":wq" - too many
// WAIT YOU CAN OPEN PDE FILES WITH VIM WHAT WHY DID I NOT TRY THIS EARLIER HOW DO I RUN IT NOW?
// number of times this program has been refactored - 4 (assingment 1) : 2 (assignment 2)

// this is why you plan your projects and look at more than just example code for studying 
// i should quit web dev and become a chef with this amount of spaghetti

/***************** 
  GLOBAL OBJECTS  
*****************/

Config CONF     = new Config();      // instantiate the config boi
Environment ENV = new Environment(); // instantiate the env BOI
Door door       = new Door();        // instantiate the door BOIII
Berd player     = new Berd();        // instantiate the player BOIIIIIIIIIIIIIIIIIIIII
Birb buddy      = new Birb();        // INSTANTIATE PLAYER 2 BOIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
Cate enemy      = new Cate();        // evil player cate hate berds

/***********************
  PROCESSING FUNCTIONS
  built-in functions
***********************/

public void setup() {
  frameRate(30);
      
  // Create columns
  int cWidth = width/ ENV.columns.length; // columns based on screen width
  for (int i = 0; i <= ENV.columns.length - 1; i++) {
    ENV.columns[i] = cWidth * (i + 1);
  }
  // Create rows
  int rHeight = height / ENV.rows.length;
  for (int i = 0; i <= ENV.rows.length - 1; i++) {
    ENV.rows[i] = rHeight * (i + 1);
  }
  
  // load sprites
  ENV.loadSprites();
}

public void draw() {
  // get time stuff
  int runtime    = millis() / 1000; // Get current second of program runtime
  boolean update = false;
  
  if (runtime != ENV.runcount) {
    // program is updated every second
    update       = true;
    ENV.runcount = runtime;
  }
  
  // render game
  ENV.render(update, runtime);
}

public void keyPressed() {
	if (key == ESC) {key = 0; eject();};
  if (ENV.gameRunning) {
    player.move(key, keyCode);
    if (Config.gameType == "standard-coop") { buddy.move(key, keyCode); }
    if (Config.gameType == "standard-pvp")  { enemy.createTool(key, keyCode); }
    // exit game
    if (keyCode == BACKSPACE) { exitGame(); }
  } else {
    // run
    if ((keyCode == ENTER) || (keyCode == RETURN)) { runGame(); }
    // toggle debug
    if (key == '0') { Config.toggleDebug(); }
    // toggle game type
    if (key == '2') { Config.cycleGame(); }
    // cycle backgrounds
    if (key == '1') { Config.cycleBackground(); }
  }
}
// YEAH THATS RIGHT! EVEN THE SETTINGS ARE PUT INTO A CLASS
// MY ADDICTION TO OO KNOWS NO BOUNDS
// PLEASE SEND HELP

static class Config {
  static String background; // background
  static boolean debug;     // debug
  static String gameType;   // helpful comment
  
  Config() {
    background = "city";
    debug      = false;
    gameType   = "circles";
  }
  
  public static void toggleDebug() {
    debug = !debug;
  }
  
  public static void cycleGame() {
    switch (gameType) {
      case "circles":       gameType = "standard"; break;
      case "standard":      gameType = "standard-coop"; break;
      case "standard-coop": gameType = "standard-pvp"; break;
      case "standard-pvp":  gameType = "circles"; break;
      default: break;
    }
  }
  
  public static void cycleBackground() {
    switch (background) {
      case "city":   background = "desert"; break;
      case "desert": background = "plain"; break;
      case "plain":  background = "city"; break;
      default: break;
    }
  }
}
// why is the door in a class? WHO KNOWS
// DONT ASK ME, ASK MY THERAPIST

class Door {
  PImage door;
  boolean open;    // door status
  boolean indoors; // check if player has been in door GET IT? ITS INDOORS LIKE IF YOU'RE INSIDE A HOUSE! GET IT! AHAHAHAAHAHAHAH
  int interval;    // the time that the door will change
  
  Door() {
    open     = false;
    indoors  = false;
    interval = PApplet.parseInt(random(3,9));
  }
  
  public void rndOpen(int runtime) {
    // door logic 
    if (runtime >= interval) {
      // create a new random time from 3 to 8 secs
      interval = PApplet.parseInt(random(3, 9)) + runtime;
      // toggle door
      open = !open;
    }
  }
  
  public void processDoor() {
    // check if player in door
    if (open && !indoors && (player.column == 6 || buddy.column == 6)) { player.score += 3; indoors = true; }
    // change rightwall column
    if (open) { 
      ENV.rightWall = ENV.columns.length - 2;
    } else {
      indoors       = false; 
      ENV.rightWall = ENV.columns.length - 3;
    }
  }

  public void render(int runtime) {  
    rndOpen(runtime);
    processDoor();
    pushMatrix();
    imageMode(CENTER);
    translate(ENV.columns[ENV.rightWall+1], ENV.rows[5]);
    image(door, 0, 0);
    popMatrix();
  }
}
// Environment of the program
// stuff to do with the position of stuff and general game objects stuff

class Environment {  
  PImage city, desert;
  PImage hammer, sickle, popsicle;
  boolean gameRunning; // to check game run
  boolean finish;      // check if game has ended
  ArrayList tools;     // list of falling tool objects
  int[] columns;       // x-pos of every (8) column
  int[] rows;          // y-pos of every(8) row
  int newToolInterval; // the time that a tool will be created
  int ctrlInterval;    // interval the show controls text is removed
  int runcount;        // helper for program timers
  int rightWall;       // index of right wall (changes if door is open or closed)
  
  Environment() {
    gameRunning     = false;
    finish          = false;
    tools           = new ArrayList(0);
    columns         = new int[8];
    rows            = new int[8];
    newToolInterval = PApplet.parseInt(random(1,5));
    ctrlInterval    = 0;
    runcount        = 0;
    rightWall       = columns.length - 2;
  }
  
  public void loadSprites() {
    city          = loadImage("city.png");
    desert        = loadImage("desert.png");
    hammer        = loadImage("hammer.png");
    sickle        = loadImage("sickle.png");
    popsicle      = loadImage("popsicle.png");
    door.door     = loadImage("door.png");
    player.sprite = loadImage("berd.png");
    buddy.sprite  = loadImage("birb.png");
    player.hit    = loadImage("berd_hit.png");
    buddy.hit     = loadImage("berd_hit.png");
  }

  public void render(boolean update, int runtime) {
    background(0xffffffff);
    // draw backgrounds
    pushMatrix();
    imageMode(CORNER);
    translate(width/2, height, 2);
    if (Config.background == "city")   { image(city, 0, 0); }
    if (Config.background == "desert") { image(desert, 0, 0); }
    popMatrix();
    
    if (gameRunning) {
      runGame(update, runtime);
    } else {
      runMenu();
    }
    
    if (Config.debug) {
      // no bugs, only features
      debugLines();
      debugSettings();
    }
  }
}

// player 1
class Berd {
  PImage sprite, hit;
  String direction; // direction player is facing
  int column;       // index of column
  int hits;         // times player has been hit
  int score;        // score
  int oldHit;       // old hit count used for determining if player hit
  int aTime;        // animation time in frames
  int aLeft;        // animation frames left
  
  Berd() {
    direction = "right";
    column    = 0;
    hits      = 0;
    score     = 0;
    oldHit    = 0;
    aTime     = 100;
    aLeft     = aTime;
  }
  
  private void move(int key, int keycode) {
    if ((keycode == LEFT) && (column > 0) && (column-1 != buddy.column)) { 
      column--; 
      direction = "left"; 
    }
    if ((keycode == RIGHT) && (column < ENV.rightWall) && (column+1 != buddy.column)) { 
      column++; 
      direction = "right"; 
    }
  }
  
  public boolean animateHit() {
    // i really wanted to call this ishit
    if ((hits != oldHit) && (aLeft-- >= 0)) { return true; }
    oldHit = hits; 
    aLeft  = aTime;
    return false;
  }
  
  public void render() {
    pushMatrix();
    imageMode(CENTER);
    translate(ENV.columns[column], ENV.rows[5]);
    if (animateHit())         { image(hit, 0, 0); } // draw hit marker
    if (direction == "right") { scale(-1, 1); }     // flip direction
    image(sprite, 0, 0);
    popMatrix();
  }
}

// player 2
class Birb extends Berd {
  
  Birb() {
    column = Config.gameType == "standard-coop" ? 0 : 7; // keep birb out of way when not being used
  }
  
  private void move (int key, int keycode) {
    if ((key == 'a') && (column > 0) && (column-1 != player.column)) {
      column--; 
      direction = "left"; 
    }
    if ((key == 'd') && (column < ENV.rightWall) && (column+1 != player.column)) { 
      column++;
      direction = "right"; 
    }
  }
}

// cate hate berd
class Cate {
  int toolsAvaliable; // tools cate can spawn
  int maxTools;       // max number of tools cate can hold
  
  Cate() {
    toolsAvaliable = 0;
    maxTools       = 4;
  }
  
  public void addTool() {
    if (toolsAvaliable <= maxTools) { toolsAvaliable++; }
  }
  
  public void createTool(int key, int keycode) {
    int column = -1;
    switch (key) {
      case '1': column = 0; break;
      case '2': column = 1; break;
      case '3': column = 2; break;
      case '4': column = 3; break;
      case '5': column = 4; break;
      case '6': column = 5; break;
      case '7': column = 6; break;
      default: break;
    }
    
    if (toolsAvaliable <= 0) { return; }
    if (column >= 0) {
      Tool newTool = new Tool(PApplet.parseInt(random(1,4)), column, random(0,5));
      ENV.tools.add(newTool);
      toolsAvaliable--;
    }
  }
}

class Tool {
  int toolID;      // the identifier of the tool - support for random generation
  int column, row; // the column, row index
  float rotation;  // rotation of the tool
  
  Tool(int whoami, int whereami, float rotatepotate) {
    toolID   = whoami;
    column   = whereami;
    row      = 0; // always spawn at row 0 
    rotation = rotatepotate;
  }
  
  public void render() {
    pushMatrix();
    imageMode(CENTER);
    translate(ENV.columns[column], ENV.rows[row]);
    rotate(PI*rotation);
    if (toolID == 1) { image(ENV.hammer, 0, 0); }
    if (toolID == 2) { image(ENV.sickle, 0, 0); }
    if (toolID == 3) { image(ENV.popsicle, 0, 0); }
    popMatrix();
  }
}
/********
  GAME
  game?
********/

public void runGame(boolean update, int runtime) {
  processTools(update);
  rndTool(runtime);
  
  door.render(runtime);
  player.render();
  
  if (Config.gameType == "circles")       { drawCircles(); }
  if (Config.gameType == "standard")      { drawScore(); }
  if (Config.gameType == "standard-coop") { drawScore(); buddy.render(); }
  if (Config.gameType == "standard-pvp")  { drawScore(); columnMarkers(); }
  
  if (runtime <= ENV.ctrlInterval) { showControls(); }
  
  // Quit game if player loses or wins in circles game
  if (player.hits+buddy.hits >= 4 || (Config.gameType == "circles" && player.score >= 16)) { exitGame(); }
}

public void processTools(boolean update) {
  // tool logic
  for (int i = 0; i < ENV.tools.size(); i++) {
    Tool t = (Tool) ENV.tools.get(i); // get tool
    if (update) { t.row++; }          // move tool every second
    
    // grid based system removes need for distance based collision detection
    if (t.column == player.column && t.row >= 5) {
      ENV.tools.remove(t);
      player.column = 0; // reset player
      player.hits++;     // player hit boo hoo
      continue;          // skip to next tool
    }
    // buddy detection
    if (t.column == buddy.column && t.row >= 5) {
      ENV.tools.remove(t);
      buddy.column = 0; 
      buddy.hits++; 
      continue;
    }
    
    if (t.row >= 7) { 
      ENV.tools.remove(t); // tool reach end
      player.score++;      // player score yoo hoo
      if (Config.gameType == "standard-coop") { buddy.score++; }
      continue;            // skip to next tool
    } 
    
    t.render(); // render
  }
}

public void createTool() {
  // create a tool in a random column and add it to the list
  Tool newTool = new Tool(PApplet.parseInt(random(1,4)), PApplet.parseInt(random(0,7)), random(0,5));
  ENV.tools.add(newTool);
}

public void rndTool(int runtime) {
  // tool creation
  if (runtime >= ENV.newToolInterval) {
    // create a new random time from 2 to 5 secs
    ENV.newToolInterval = PApplet.parseInt(random(2, 6)) + runtime;
    if (Config.gameType == "standard-pvp") {
      enemy.addTool();
    } else {
      createTool();
    }
  }
}

public void runGame() {
  // reset everything
  player           = new Berd();
  buddy            = new Birb();
  enemy            = new Cate();
  ENV.ctrlInterval = ENV.runcount + 3;
  ENV.gameRunning  = true;
  ENV.loadSprites();
}

public void exitGame() {
  ENV.tools       = new ArrayList(0);
  ENV.finish      = true;
  ENV.gameRunning = false;
}
/***********************************
  GAME DISPLAY
  everything shown on the game hud
***********************************/

public void drawCircles() {
  int score = player.score;
  int hits  = player.hits;
  
  noStroke();
  
  // do point circles
  for (int y = 0; y < 8; y+=2) {
    for (int x = 0; x < 8; x+=2){
      if (score-- <= 0) { break; }
      drawCircle("black", x, y);
    }
  }
  
  // do hit circles
  for (int y = 0; y < 8; y+=2) {
    if (hits-- <= 0) { break; }
    for (int x = 0; x < 8; x+=2){
      drawCircle("red", x, y);
    }
  }
  
  stroke(1);
}

public void drawCircle(String colour, int column, int row) {
  for (int i = 1; i < 50; i++) {
    if (colour == "black") { fill(255-(PApplet.parseFloat(i)*8)); }
    if (colour == "red")   { fill(255, 0, 0, (PApplet.parseFloat(i)*8)); }
    ellipse(ENV.columns[column], ENV.rows[row], 80-(2*i), 80-(2*i));
  }
}

public void columnMarkers() {
  for (int i = 0; i < ENV.columns.length - 1; i++) {
    pushMatrix();
    textSize(10);
    fill(1);
    translate(ENV.columns[i], 20);
    text("["+str(i+1)+"]", 0,0);
    popMatrix();
  }
}

public void drawScore() {
  fill(0xffff0000);
  textSize(20);
  textAlign(CENTER, CENTER);
  text("Score: "+str(player.score+buddy.score)+" Lives: "+str(player.hits+buddy.hits)+"/4", width/2, 50);
  if (Config.gameType == "standard-pvp") { text("Cate tools avaliable: "+str(enemy.toolsAvaliable)+"/4", width/2, 70); }
}
/********
  MENU
  menu!
********/

public void runMenu() {
  // title screen
  pushMatrix();
  
  // rect
  rectMode(RADIUS);
  fill(255);
  rect(width/2, height/2+5, 90, 70);
  
  fill(50);
  textAlign(CENTER, CENTER);
  
  titleText();
  
  textSize(32);
  text("Game", width/2, height/2-20);
  textSize(12);
  text("Press <enter> to start", width/2, height/2+15);
  if (ENV.finish) { text("Previous score: "+str(player.score+buddy.score), width/2, height/2+30); }
  text("Game type: "+Config.gameType, width/2, height/2+45);
  
  menuControls();
  popMatrix();
}

public void titleText() {
  // Check if player is back from the game ending
  if (player.hits+buddy.hits >= 4) { 
    // laugh at the player for losing
    textSize(12);
    text("You lose the", width/2, height/2-40);
  } else if (player.score >= 16) {
    // reward the player for winning
    textSize(12);
    text("You win the", width/2, height/2-40);
  } else if (ENV.finish) {
    // shame the player for quitting
    textSize(12);
    text("You chicken'd out of the", width/2, height/2-40);
  }
  // you can't win the standard mode
}

public void menuControls() {
  //controls
  rectMode(CORNER);
  fill(255);
  rect(5, height-70, 155, 65);
  
  fill(50);
  textAlign(LEFT, CENTER);
  textSize(10);
  text("Options (press number key):", 10, height-60);
  text("[1]: Change background", 10, height-45);
  text("[2]: Change game mode", 10, height-30);
  text("[0]: Toggle debug mode", 10, height-15);
}
/********************
  SUBROUTINES
  misc subroutines
********************/

public void debugLines() {
  // show grid
  stroke(1);
  textSize(11);
  for (int i = 0; i < ENV.columns.length; i++) {
    pushMatrix();
    translate(ENV.columns[i]-10, 100);
    rotate(PI*1.5f);
    text("Column["+str(i)+"] "+str(ENV.columns[i])+"px", 0,0);
    popMatrix();
    line(ENV.columns[i], 0, ENV.columns[i], height);
  }
  for (int i = 0; i < ENV.rows.length; i++) {
    line(0, ENV.rows[i], width, ENV.rows[i]);
  }
  
  // show right wall
  stroke(255, 0, 0, 255);
  line(ENV.columns[ENV.rightWall], 0, ENV.columns[ENV.rightWall], height);
  stroke(1);
}

public void debugSettings() {
  // show current settings
  // gibe white background so you can see with circles
  fill(255);
  rect(5, 10, 150, 90);
  textAlign(LEFT, CENTER);
  
  // text
  fill(50);
  textSize(10);
  text("Background: "+Config.background, 10, 15);
  text("Game type: "+Config.gameType, 10, 30);
  text("Runcount: "+str(ENV.runcount), 10, 45);
  text("New tool interval: "+str(ENV.newToolInterval), 10, 60);
  text("Door toggle interval: "+str(door.interval), 10, 75);
  text("Show controls interval: "+str(ENV.ctrlInterval), 10, 90);
}

public void showControls() {
  // rect
  stroke(1);
  fill(255);
  rectMode(RADIUS);
  rect(width/2, height/2, 160, 60);
  
  // text
  textAlign(CENTER, CENTER);
  fill(50);
  textSize(25);
  if (Config.gameType == "circles")       { text("Get to 16 points to win", width/2, height/2-40); }
  if (Config.gameType == "standard")      { text("Survive", width/2, height/2-40); } 
  if (Config.gameType == "standard-coop") { text("Survive together", width/2, height/2-40); } 
  if (Config.gameType == "standard-pvp")  { text("Survive the attack", width/2, height/2-40); }
  textSize(15);
  text("Press <backspace> to return to menu", width/2, height/2-10);
  text("Move berd with <left><right> arrow keys", width/2, height/2+10);
  if (Config.gameType == "standard-coop") { text("Move birb with <a><d> keys", width/2, height/2+30); }
  if (Config.gameType == "standard-pvp")  { text("Create tools with number row keys (1-7)", width/2, height/2+30); }
}
  public void settings() {  size(512, 348); }
  
}
