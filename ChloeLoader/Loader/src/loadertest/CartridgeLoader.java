package loadertest;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import processing.core.PApplet;
import cart.Cart;

public class CartridgeLoader extends ClassLoader {

	private URLClassLoader loader;
	private Class<?> cartClass;
	private Cart cartInstance;
	
	private String libraryDirectory;
	private String currentBox = libraryDirectory;
	
	
	public CartridgeLoader(String libdir) {
		libraryDirectory = libdir;
		
		URL path = null;
		
		try {path = new URL(libraryDirectory);} 
		catch (MalformedURLException e) {
			System.out.println("invalid directory");
			e.printStackTrace();}
		
		URL[] paths = { path }; 
		loader = new URLClassLoader(paths); 
		
	}
	
	
	
	public void changeBox(String boxName)   {
		try {
			loader.close();
			loader = null;
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		URL newpath = null;
		currentBox = libraryDirectory + boxName;
		try {newpath = new URL(libraryDirectory + boxName + "/");}
		catch (MalformedURLException e) {
			e.printStackTrace();}
		
		URL[] patharray = {newpath};
		loader = new URLClassLoader(patharray);
				
	}
	
	
	/* "move" the classloader to a new folder */
	public void insertCart(String cartName) {
		try {cartClass = loader.loadClass(cartName); } 
		catch (ClassNotFoundException e) {
			System.out.println("Class does not exist at " + currentBox);
			e.printStackTrace();}
		
		PApplet classInstance = null;
		System.out.println(cartClass.getSuperclass().getName());
		
		try {
			classInstance = (PApplet) cartClass.getDeclaredConstructor((Class<?>[])null).newInstance();
			cartInstance = (Cart) new Cart(classInstance).getSketch();
			//Method cartMethod = cartClass.getMethod("getCart", (Class<?>[])null);
			//cartInstance = (Cart) cartMethod.invoke(classInstance);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e2) {
			e2.printStackTrace();
		}
		
		
	}	
	
	
	public void ejectCart() {
		cartClass = null;
		cartInstance = null;
		}
	
	public Cart getCart() {
		return cartInstance;
		}
	
	public String getCartName() {
		return cartClass.getName();
		
	}
}
