package loadertest;

import java.nio.file.Path;

import javax.json.*;

public class CartInfo {
	public String author;
	public String folder;
	public Path path;
	private JsonObject map;
	
	public CartInfo(String auth,String fold, Path p, JsonObject m) {
		this.author = auth;
		this.folder = fold;
		this.map = m;
		this.path = p;
		
	}
	
	public String getString(String key) {
		return map.getString(key);
	}
	
	public int getInt(String key) {
		return map.getInt(key);
	}
}
