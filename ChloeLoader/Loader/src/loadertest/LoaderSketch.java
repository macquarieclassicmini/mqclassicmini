package loadertest;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import processing.core.*;

public class LoaderSketch extends PApplet{
	
	boolean thumbnailsLoaded = false;
	
	PImage backgroundImage;
	PImage widgetSelectedImage;
	PImage widgetUnselectedImage;
	PImage missingThumbnailImage;
	PImage unselectedMissingThumbnailImage;
	
	HashMap<String,PImage> selectedThumbnails = new HashMap<String,PImage>();
	HashMap<String,PImage> unselectedThumbnails = new HashMap<String,PImage>();
	
	Path libPath;
	HashMap<String,CartInfo> gamesList;
	ArrayList<String> Authors = new ArrayList<String>();
	CartManager parentManager;
	
	int selector = 1;
	
	int widgetStartX = 61;
	int widgetStartY = 109;
	int widgetGapX = 176;
	int widgetGapY = 143;

	int scrollOffset = 0;
	int totalGames = 0;
	
	PFont font;
	
	public void settings() {
		size(640, 480);
	}
	
	public void setup() {
		backgroundImage = loadImage("mqminibackground.png");
		widgetSelectedImage = loadImage("selectedWidget.png");
		widgetUnselectedImage = loadImage("unselectedWidget.png");
		font = loadFont("Roboto-Regular-18.vlw");
		textFont(font,16);
	}
	
	public void getManagerInfo(CartManager manager) {
		this.libPath = manager.libPath;
		this.gamesList = manager.Games;
		this.Authors = manager.Authors;
		parentManager = manager;
		
		totalGames = gamesList.size();
		
		for(String auth : Authors) {System.out.println(auth);}
	}
	
	public void loadThumbnails() {
		
		for(String auth : Authors) {
			CartInfo info = gamesList.get(auth);
			File thumbnail = info.path.resolve("thumbnail.png").toFile();
			if(thumbnail.exists()) {
				PImage thumb = loadImage(thumbnail.toString());
				try {Thread.sleep(10);} catch (InterruptedException e) {e.printStackTrace();} /*allow a brief moment to load image completely*/
				selectedThumbnails.put(auth, thumb);
				PImage unthumb = thumb.get();
				unthumb.filter(GRAY);
				unselectedThumbnails.put(auth, unthumb);
			}
		}
		
		PImage thumb = loadImage("missingThumbnail.png");
		try {Thread.sleep(100);} catch (InterruptedException e) {e.printStackTrace();} /*allow a brief moment to load image completely*/
		missingThumbnailImage = thumb;
		PImage unthumb = thumb.get();
		unthumb.filter(GRAY);
		unselectedMissingThumbnailImage = unthumb;
		
		thumbnailsLoaded = true;
		
		//System.out.println(missingThumbnailImage.toString());
		//System.out.println(unselectedMissingThumbnailImage.toString());
		
		
	}
	
	public void keyPressed() {
		if (key == CODED) {
			switch(keyCode) {
			case RIGHT: {selector = constrain(selector+1,0,totalGames-1);};break;
			case LEFT: {selector = constrain(selector-1,0,totalGames-1);};break;
			case UP: {selector = constrain(selector-3,0,totalGames-1);};break;
			case DOWN: {selector = constrain(selector+3,0,totalGames-1);};break;
			}
		  }
		if (key == ENTER || key == RETURN) {parentManager.requestStart(Authors.get(selector));};
	}
	
	public void focus() {
		frame.toFront();
	    frame.requestFocus();
	}
	
	public void draw() {
		image(backgroundImage, 0, 0);
		
		scrollOffset = selector/6;
		
		if (thumbnailsLoaded) {
			
			int xx = widgetStartX;
			int yy = widgetStartY;
			int i = 0;
			for(int o = scrollOffset*6; o < min((scrollOffset*6)+6,totalGames); o++) {
				String auth = Authors.get(o);
				drawWidget(xx,yy,auth);
				i+=1;
				xx += widgetGapX;
				if (i == 3) {xx = widgetStartX; yy += widgetGapY;}
			}
			
			
			
			
			}
		
		
		
	}
		
	public void drawWidget(int x, int y, String author) {
		boolean selected = false;
		CartInfo i = gamesList.get(author);
		
		if (Authors.get(selector).equals(author)) {selected = true;}
		
		
		PImage thumbnail = getThumbnail(author, selected);
		textAlign(CENTER);
		int starty = y+100;
		if (selected) {
			image(widgetSelectedImage,x,y);
			image(thumbnail,x+3,y+3);
			fill(255);
			text(author,x+83,starty);
			fill(150);
			text(i.getInt("year"),x+83,starty+18);
		}
		else {
			image(widgetUnselectedImage,x,y);
			image(thumbnail,x+3,y+3);
			fill(150);
			text(author,x+83,starty);
			fill(80);
			text(i.getInt("year"),x+83,starty+18);
		}
		
		
		
		
		
	}
	
	public PImage getThumbnail(String auth, boolean selected) {
		if (selectedThumbnails.containsKey(auth)) {
			if (selected) {return selectedThumbnails.get(auth);}
			else {return unselectedThumbnails.get(auth);}
		} else {
			if (selected) {return missingThumbnailImage;}
			else {return unselectedMissingThumbnailImage;}
		}
	}
	
	  
}
