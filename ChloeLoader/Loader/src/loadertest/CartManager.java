package loadertest;

import processing.core.PApplet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import cart.Cart;

public class CartManager {
	
	String rootDir = ""; //used for spoofing the run directory for debug purposes
	Path libPath;
	
	String libraryDirectory;
	
	
	LoaderSketch loaderSketch; /* the sketch running the loader */
	Cart currentGameCart; /* the sketch for the loaded game */
	CartridgeLoader cl;
	boolean cartFlag = false;
	
	HashMap<String,CartInfo> Games= new HashMap<String,CartInfo>();
	
	ArrayList<String> Authors = new ArrayList<String>();

	
	public CartManager() {
		if (rootDir == "") {
			rootDir = System.getProperty("user.dir");
			libPath = Paths.get(rootDir).resolve("library");
			} //if rootdir is empty we want to run from the current location
		else {
			libPath = Paths.get(rootDir);
			}
		
		
		libraryDirectory = libPath.toUri().toString();
		
		cl = new CartridgeLoader(libraryDirectory);
			
	}
	
	public void onEject() {
		System.gc();
		loaderSketch.getSurface().setVisible(true);
		loaderSketch.loop();
		loaderSketch.focus();
		}
	
	public void requestStart(String auth) {
		loaderSketch.noLoop();
		loaderSketch.getSurface().setVisible(false);
		startGame(auth);
	}
	
	public void startGame(String auth) {
		CartInfo info = null;
		info = Games.get(auth);
		
		
		String folder = info.folder;
		cl.changeBox(folder);
		cl.insertCart(info.getString("classname"));
		currentGameCart = cl.getCart();
		currentGameCart.boot(new String[] {cl.getCartName()}, this);
	}
	
	public void parseLibrary() {
		System.out.println(libPath.toString());
		File[] directories = libPath.toFile().listFiles(File::isDirectory);
		
		JsonReader jr = null;
		int loadedGames = 0;
	
		
		for(File f : directories) {
			File infoJson = f.toPath().resolve("info.json").toFile();
			if (infoJson.exists()) {
				try {jr = Json.createReader(new FileInputStream(infoJson));} catch (FileNotFoundException e) {e.printStackTrace();}
				JsonObject jobj = jr.readObject();
				jr.close();
				
				String auth = jobj.getString("author");
				String fold = infoJson.getParentFile().toPath().getFileName().toString();

				
				loadedGames+=1;
				Games.put(auth, new CartInfo(auth,fold,infoJson.getParentFile().toPath(),jobj));
				Authors.add(auth);

				
				}
			}
		Collections.sort(this.Authors,String.CASE_INSENSITIVE_ORDER);
	
		System.out.println("Loaded " + String.valueOf(loadedGames) + " game(s)");


	}

	public void startLoader() {
		loaderSketch = new LoaderSketch();
		loaderSketch.getManagerInfo(this);
		
		PApplet.runSketch(new String[] {"loadertest.LoaderSketch"}, loaderSketch);
		
		loaderSketch.loadThumbnails();
		
	}
}

